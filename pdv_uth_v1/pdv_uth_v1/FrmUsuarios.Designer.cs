﻿namespace pdv_uth_v1
{
    partial class FrmUsuarios
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmUsuarios));
            this.panel1 = new System.Windows.Forms.Panel();
            this.iconBtnHome = new FontAwesome.Sharp.IconButton();
            this.btnCerrar = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.iconBtnIngresarAUCliente = new FontAwesome.Sharp.IconButton();
            this.pictureBoxNext = new System.Windows.Forms.PictureBox();
            this.btnCertificadoEstudiosCargar = new System.Windows.Forms.Button();
            this.pictureBoxCertificadoEstudios = new System.Windows.Forms.PictureBox();
            this.lblImagenCertificadoEstudios = new System.Windows.Forms.Label();
            this.lblCertificadoEstudiosGuardada = new System.Windows.Forms.Label();
            this.btnActaNacimientoGuardar = new System.Windows.Forms.Button();
            this.pictureBoxActaNacimiento = new System.Windows.Forms.PictureBox();
            this.lblImagenActaNacimiento = new System.Windows.Forms.Label();
            this.lblImagenActaNacGuardada = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.lblImagenComproINE = new System.Windows.Forms.Label();
            this.lblImagenComprobanteCURP = new System.Windows.Forms.Label();
            this.lblImagenComproINEGuardada = new System.Windows.Forms.Label();
            this.btnComproINEGuardada = new System.Windows.Forms.Button();
            this.pictureBoxComproINE = new System.Windows.Forms.PictureBox();
            this.btnComproCURPGuardada = new System.Windows.Forms.Button();
            this.pictureBoxComproCURP = new System.Windows.Forms.PictureBox();
            this.lblImagendeComproCURPGuardada = new System.Windows.Forms.Label();
            this.lblImagendelComproGuardada = new System.Windows.Forms.Label();
            this.lblImagenComproDom = new System.Windows.Forms.Label();
            this.btnCargarImgDomicilio = new System.Windows.Forms.Button();
            this.pictureBoxDomicilio = new System.Windows.Forms.PictureBox();
            this.btnEditar = new System.Windows.Forms.Button();
            this.btnEliminar = new System.Windows.Forms.Button();
            this.btnGuardarUsuario = new System.Windows.Forms.Button();
            this.btnLimpiar = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.comboBoxTipoUsuario = new System.Windows.Forms.ComboBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.txtNumSeguroSocial = new System.Windows.Forms.TextBox();
            this.txtContraseña = new System.Windows.Forms.TextBox();
            this.txtCURP = new System.Windows.Forms.TextBox();
            this.txtCorreo = new System.Windows.Forms.TextBox();
            this.txtCalle = new System.Windows.Forms.TextBox();
            this.txtLocalidad = new System.Windows.Forms.TextBox();
            this.txtMunicipio = new System.Windows.Forms.TextBox();
            this.txtTelefono = new System.Windows.Forms.TextBox();
            this.txtCelular = new System.Windows.Forms.TextBox();
            this.txtColonia = new System.Windows.Forms.TextBox();
            this.txtCP = new System.Windows.Forms.TextBox();
            this.txtFraccionamiento = new System.Windows.Forms.TextBox();
            this.txtApellidoMaterno = new System.Windows.Forms.TextBox();
            this.txtApellidoPaterno = new System.Windows.Forms.TextBox();
            this.txtNumCasa = new System.Windows.Forms.TextBox();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.lblComproDomicilio = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.dtpFechaDeNacimiento = new System.Windows.Forms.DateTimePicker();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.errorProviderForm = new System.Windows.Forms.ErrorProvider(this.components);
            this.panelDataGrid = new System.Windows.Forms.Panel();
            this.dgUsuarios = new System.Windows.Forms.DataGridView();
            this.openFileDialogImagenACargar = new System.Windows.Forms.OpenFileDialog();
            this.toolTipUsers = new System.Windows.Forms.ToolTip(this.components);
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxNext)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCertificadoEstudios)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxActaNacimiento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxComproINE)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxComproCURP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxDomicilio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderForm)).BeginInit();
            this.panelDataGrid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgUsuarios)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(189)))), ((int)(((byte)(84)))));
            this.panel1.Controls.Add(this.iconBtnHome);
            this.panel1.Controls.Add(this.btnCerrar);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1366, 49);
            this.panel1.TabIndex = 0;
            // 
            // iconBtnHome
            // 
            this.iconBtnHome.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(176)))), ((int)(((byte)(105)))));
            this.iconBtnHome.Dock = System.Windows.Forms.DockStyle.Right;
            this.iconBtnHome.FlatAppearance.BorderSize = 0;
            this.iconBtnHome.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.iconBtnHome.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.iconBtnHome.IconChar = FontAwesome.Sharp.IconChar.Home;
            this.iconBtnHome.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.iconBtnHome.IconSize = 55;
            this.iconBtnHome.Location = new System.Drawing.Point(1237, 0);
            this.iconBtnHome.Name = "iconBtnHome";
            this.iconBtnHome.Rotation = 0D;
            this.iconBtnHome.Size = new System.Drawing.Size(76, 49);
            this.iconBtnHome.TabIndex = 2;
            this.iconBtnHome.UseVisualStyleBackColor = false;
            this.iconBtnHome.Click += new System.EventHandler(this.iconBtnHome_Click);
            // 
            // btnCerrar
            // 
            this.btnCerrar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(124)))), ((int)(((byte)(124)))));
            this.btnCerrar.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnCerrar.Image = global::pdv_uth_v1.Properties.Resources.exit_closethesession_close_63171;
            this.btnCerrar.Location = new System.Drawing.Point(1313, 0);
            this.btnCerrar.Name = "btnCerrar";
            this.btnCerrar.Size = new System.Drawing.Size(53, 49);
            this.btnCerrar.TabIndex = 1;
            this.btnCerrar.UseVisualStyleBackColor = false;
            this.btnCerrar.Click += new System.EventHandler(this.btnCerrar_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe Print", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.label1.Location = new System.Drawing.Point(2, 2);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(377, 42);
            this.label1.TabIndex = 0;
            this.label1.Text = "FORMULARIO DE USUARIOS";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(176)))), ((int)(((byte)(105)))));
            this.panel2.Controls.Add(this.iconBtnIngresarAUCliente);
            this.panel2.Controls.Add(this.pictureBoxNext);
            this.panel2.Controls.Add(this.btnCertificadoEstudiosCargar);
            this.panel2.Controls.Add(this.pictureBoxCertificadoEstudios);
            this.panel2.Controls.Add(this.lblImagenCertificadoEstudios);
            this.panel2.Controls.Add(this.lblCertificadoEstudiosGuardada);
            this.panel2.Controls.Add(this.btnActaNacimientoGuardar);
            this.panel2.Controls.Add(this.pictureBoxActaNacimiento);
            this.panel2.Controls.Add(this.lblImagenActaNacimiento);
            this.panel2.Controls.Add(this.lblImagenActaNacGuardada);
            this.panel2.Controls.Add(this.label26);
            this.panel2.Controls.Add(this.lblImagenComproINE);
            this.panel2.Controls.Add(this.lblImagenComprobanteCURP);
            this.panel2.Controls.Add(this.lblImagenComproINEGuardada);
            this.panel2.Controls.Add(this.btnComproINEGuardada);
            this.panel2.Controls.Add(this.pictureBoxComproINE);
            this.panel2.Controls.Add(this.btnComproCURPGuardada);
            this.panel2.Controls.Add(this.pictureBoxComproCURP);
            this.panel2.Controls.Add(this.lblImagendeComproCURPGuardada);
            this.panel2.Controls.Add(this.lblImagendelComproGuardada);
            this.panel2.Controls.Add(this.lblImagenComproDom);
            this.panel2.Controls.Add(this.btnCargarImgDomicilio);
            this.panel2.Controls.Add(this.pictureBoxDomicilio);
            this.panel2.Controls.Add(this.btnEditar);
            this.panel2.Controls.Add(this.btnEliminar);
            this.panel2.Controls.Add(this.btnGuardarUsuario);
            this.panel2.Controls.Add(this.btnLimpiar);
            this.panel2.Controls.Add(this.pictureBox1);
            this.panel2.Controls.Add(this.comboBoxTipoUsuario);
            this.panel2.Controls.Add(this.label24);
            this.panel2.Controls.Add(this.label23);
            this.panel2.Controls.Add(this.label22);
            this.panel2.Controls.Add(this.label21);
            this.panel2.Controls.Add(this.txtNumSeguroSocial);
            this.panel2.Controls.Add(this.txtContraseña);
            this.panel2.Controls.Add(this.txtCURP);
            this.panel2.Controls.Add(this.txtCorreo);
            this.panel2.Controls.Add(this.txtCalle);
            this.panel2.Controls.Add(this.txtLocalidad);
            this.panel2.Controls.Add(this.txtMunicipio);
            this.panel2.Controls.Add(this.txtTelefono);
            this.panel2.Controls.Add(this.txtCelular);
            this.panel2.Controls.Add(this.txtColonia);
            this.panel2.Controls.Add(this.txtCP);
            this.panel2.Controls.Add(this.txtFraccionamiento);
            this.panel2.Controls.Add(this.txtApellidoMaterno);
            this.panel2.Controls.Add(this.txtApellidoPaterno);
            this.panel2.Controls.Add(this.txtNumCasa);
            this.panel2.Controls.Add(this.txtNombre);
            this.panel2.Controls.Add(this.lblComproDomicilio);
            this.panel2.Controls.Add(this.label18);
            this.panel2.Controls.Add(this.label17);
            this.panel2.Controls.Add(this.label16);
            this.panel2.Controls.Add(this.dtpFechaDeNacimiento);
            this.panel2.Controls.Add(this.label15);
            this.panel2.Controls.Add(this.label14);
            this.panel2.Controls.Add(this.label13);
            this.panel2.Controls.Add(this.label12);
            this.panel2.Controls.Add(this.label11);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Font = new System.Drawing.Font("Segoe Print", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.panel2.Location = new System.Drawing.Point(0, 49);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1366, 371);
            this.panel2.TabIndex = 2;
            // 
            // iconBtnIngresarAUCliente
            // 
            this.iconBtnIngresarAUCliente.FlatAppearance.BorderSize = 0;
            this.iconBtnIngresarAUCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.iconBtnIngresarAUCliente.Flip = FontAwesome.Sharp.FlipOrientation.Horizontal;
            this.iconBtnIngresarAUCliente.IconChar = FontAwesome.Sharp.IconChar.Share;
            this.iconBtnIngresarAUCliente.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(156)))), ((int)(((byte)(0)))));
            this.iconBtnIngresarAUCliente.IconSize = 45;
            this.iconBtnIngresarAUCliente.Location = new System.Drawing.Point(1252, 77);
            this.iconBtnIngresarAUCliente.Name = "iconBtnIngresarAUCliente";
            this.iconBtnIngresarAUCliente.Rotation = 0D;
            this.iconBtnIngresarAUCliente.Size = new System.Drawing.Size(44, 38);
            this.iconBtnIngresarAUCliente.TabIndex = 78;
            this.iconBtnIngresarAUCliente.UseVisualStyleBackColor = true;
            this.iconBtnIngresarAUCliente.Click += new System.EventHandler(this.iconBtnIngresarAUCliente_Click);
            // 
            // pictureBoxNext
            // 
            this.pictureBoxNext.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(156)))), ((int)(((byte)(0)))));
            this.pictureBoxNext.Image = global::pdv_uth_v1.Properties.Resources.Next_arrow_1559;
            this.pictureBoxNext.Location = new System.Drawing.Point(1311, 77);
            this.pictureBoxNext.Name = "pictureBoxNext";
            this.pictureBoxNext.Size = new System.Drawing.Size(47, 36);
            this.pictureBoxNext.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBoxNext.TabIndex = 77;
            this.pictureBoxNext.TabStop = false;
            this.pictureBoxNext.Click += new System.EventHandler(this.pictureBoxNext_Click);
            // 
            // btnCertificadoEstudiosCargar
            // 
            this.btnCertificadoEstudiosCargar.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btnCertificadoEstudiosCargar.FlatAppearance.BorderSize = 30;
            this.btnCertificadoEstudiosCargar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnCertificadoEstudiosCargar.Image = global::pdv_uth_v1.Properties.Resources._1486505253_folder_folder_up_folder_upload_update_folder_upload_81427__1_;
            this.btnCertificadoEstudiosCargar.Location = new System.Drawing.Point(1080, 94);
            this.btnCertificadoEstudiosCargar.Name = "btnCertificadoEstudiosCargar";
            this.btnCertificadoEstudiosCargar.Size = new System.Drawing.Size(39, 38);
            this.btnCertificadoEstudiosCargar.TabIndex = 76;
            this.btnCertificadoEstudiosCargar.UseVisualStyleBackColor = true;
            this.btnCertificadoEstudiosCargar.Click += new System.EventHandler(this.btnCertificadoEstudiosCargar_Click);
            // 
            // pictureBoxCertificadoEstudios
            // 
            this.pictureBoxCertificadoEstudios.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(156)))), ((int)(((byte)(0)))));
            this.pictureBoxCertificadoEstudios.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBoxCertificadoEstudios.Location = new System.Drawing.Point(875, 3);
            this.pictureBoxCertificadoEstudios.Name = "pictureBoxCertificadoEstudios";
            this.pictureBoxCertificadoEstudios.Size = new System.Drawing.Size(199, 129);
            this.pictureBoxCertificadoEstudios.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxCertificadoEstudios.TabIndex = 75;
            this.pictureBoxCertificadoEstudios.TabStop = false;
            // 
            // lblImagenCertificadoEstudios
            // 
            this.lblImagenCertificadoEstudios.AutoSize = true;
            this.lblImagenCertificadoEstudios.Font = new System.Drawing.Font("Segoe Print", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblImagenCertificadoEstudios.Location = new System.Drawing.Point(748, 32);
            this.lblImagenCertificadoEstudios.Name = "lblImagenCertificadoEstudios";
            this.lblImagenCertificadoEstudios.Size = new System.Drawing.Size(97, 12);
            this.lblImagenCertificadoEstudios.TabIndex = 74;
            this.lblImagenCertificadoEstudios.Text = "Imagen del Comprobante";
            this.lblImagenCertificadoEstudios.Visible = false;
            // 
            // lblCertificadoEstudiosGuardada
            // 
            this.lblCertificadoEstudiosGuardada.AutoSize = true;
            this.lblCertificadoEstudiosGuardada.Font = new System.Drawing.Font("Segoe Print", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCertificadoEstudiosGuardada.Location = new System.Drawing.Point(742, 6);
            this.lblCertificadoEstudiosGuardada.Name = "lblCertificadoEstudiosGuardada";
            this.lblCertificadoEstudiosGuardada.Size = new System.Drawing.Size(97, 12);
            this.lblCertificadoEstudiosGuardada.TabIndex = 73;
            this.lblCertificadoEstudiosGuardada.Text = "Imagen del Comprobante";
            this.lblCertificadoEstudiosGuardada.Visible = false;
            // 
            // btnActaNacimientoGuardar
            // 
            this.btnActaNacimientoGuardar.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btnActaNacimientoGuardar.FlatAppearance.BorderSize = 30;
            this.btnActaNacimientoGuardar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnActaNacimientoGuardar.Image = global::pdv_uth_v1.Properties.Resources.upload_information_16316__1_;
            this.btnActaNacimientoGuardar.Location = new System.Drawing.Point(666, 313);
            this.btnActaNacimientoGuardar.Name = "btnActaNacimientoGuardar";
            this.btnActaNacimientoGuardar.Size = new System.Drawing.Size(42, 38);
            this.btnActaNacimientoGuardar.TabIndex = 71;
            this.btnActaNacimientoGuardar.UseVisualStyleBackColor = true;
            this.btnActaNacimientoGuardar.Click += new System.EventHandler(this.btnActaNacimientoGuardar_Click);
            // 
            // pictureBoxActaNacimiento
            // 
            this.pictureBoxActaNacimiento.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(141)))), ((int)(((byte)(84)))));
            this.pictureBoxActaNacimiento.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBoxActaNacimiento.Location = new System.Drawing.Point(453, 275);
            this.pictureBoxActaNacimiento.Name = "pictureBoxActaNacimiento";
            this.pictureBoxActaNacimiento.Size = new System.Drawing.Size(204, 90);
            this.pictureBoxActaNacimiento.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxActaNacimiento.TabIndex = 70;
            this.pictureBoxActaNacimiento.TabStop = false;
            // 
            // lblImagenActaNacimiento
            // 
            this.lblImagenActaNacimiento.AutoSize = true;
            this.lblImagenActaNacimiento.Font = new System.Drawing.Font("Segoe Print", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblImagenActaNacimiento.Location = new System.Drawing.Point(269, 313);
            this.lblImagenActaNacimiento.Name = "lblImagenActaNacimiento";
            this.lblImagenActaNacimiento.Size = new System.Drawing.Size(97, 12);
            this.lblImagenActaNacimiento.TabIndex = 69;
            this.lblImagenActaNacimiento.Text = "Imagen del Comprobante";
            this.lblImagenActaNacimiento.Visible = false;
            // 
            // lblImagenActaNacGuardada
            // 
            this.lblImagenActaNacGuardada.AutoSize = true;
            this.lblImagenActaNacGuardada.Font = new System.Drawing.Font("Segoe Print", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblImagenActaNacGuardada.Location = new System.Drawing.Point(263, 287);
            this.lblImagenActaNacGuardada.Name = "lblImagenActaNacGuardada";
            this.lblImagenActaNacGuardada.Size = new System.Drawing.Size(97, 12);
            this.lblImagenActaNacGuardada.TabIndex = 68;
            this.lblImagenActaNacGuardada.Text = "Imagen del Comprobante";
            this.lblImagenActaNacGuardada.Visible = false;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Segoe Print", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.label26.Location = new System.Drawing.Point(248, 290);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(141, 23);
            this.label26.TabIndex = 67;
            this.label26.Text = "Acta de nacimiento";
            // 
            // lblImagenComproINE
            // 
            this.lblImagenComproINE.AutoSize = true;
            this.lblImagenComproINE.Font = new System.Drawing.Font("Segoe Print", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblImagenComproINE.Location = new System.Drawing.Point(269, 204);
            this.lblImagenComproINE.Name = "lblImagenComproINE";
            this.lblImagenComproINE.Size = new System.Drawing.Size(97, 12);
            this.lblImagenComproINE.TabIndex = 66;
            this.lblImagenComproINE.Text = "Imagen del Comprobante";
            this.lblImagenComproINE.Visible = false;
            // 
            // lblImagenComprobanteCURP
            // 
            this.lblImagenComprobanteCURP.AutoSize = true;
            this.lblImagenComprobanteCURP.Font = new System.Drawing.Font("Segoe Print", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblImagenComprobanteCURP.Location = new System.Drawing.Point(765, 199);
            this.lblImagenComprobanteCURP.Name = "lblImagenComprobanteCURP";
            this.lblImagenComprobanteCURP.Size = new System.Drawing.Size(97, 12);
            this.lblImagenComprobanteCURP.TabIndex = 65;
            this.lblImagenComprobanteCURP.Text = "Imagen del Comprobante";
            this.lblImagenComprobanteCURP.Visible = false;
            // 
            // lblImagenComproINEGuardada
            // 
            this.lblImagenComproINEGuardada.AutoSize = true;
            this.lblImagenComproINEGuardada.Font = new System.Drawing.Font("Segoe Print", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblImagenComproINEGuardada.Location = new System.Drawing.Point(269, 181);
            this.lblImagenComproINEGuardada.Name = "lblImagenComproINEGuardada";
            this.lblImagenComproINEGuardada.Size = new System.Drawing.Size(97, 12);
            this.lblImagenComproINEGuardada.TabIndex = 64;
            this.lblImagenComproINEGuardada.Text = "Imagen del Comprobante";
            this.lblImagenComproINEGuardada.Visible = false;
            // 
            // btnComproINEGuardada
            // 
            this.btnComproINEGuardada.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btnComproINEGuardada.FlatAppearance.BorderSize = 30;
            this.btnComproINEGuardada.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnComproINEGuardada.Image = global::pdv_uth_v1.Properties.Resources._1486505253_folder_folder_up_folder_upload_update_folder_upload_81427__1_;
            this.btnComproINEGuardada.Location = new System.Drawing.Point(666, 218);
            this.btnComproINEGuardada.Name = "btnComproINEGuardada";
            this.btnComproINEGuardada.Size = new System.Drawing.Size(42, 38);
            this.btnComproINEGuardada.TabIndex = 63;
            this.btnComproINEGuardada.UseVisualStyleBackColor = true;
            this.btnComproINEGuardada.Click += new System.EventHandler(this.btnComproINEGuardada_Click_1);
            // 
            // pictureBoxComproINE
            // 
            this.pictureBoxComproINE.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(156)))), ((int)(((byte)(0)))));
            this.pictureBoxComproINE.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBoxComproINE.Location = new System.Drawing.Point(453, 181);
            this.pictureBoxComproINE.Name = "pictureBoxComproINE";
            this.pictureBoxComproINE.Size = new System.Drawing.Size(204, 88);
            this.pictureBoxComproINE.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxComproINE.TabIndex = 62;
            this.pictureBoxComproINE.TabStop = false;
            // 
            // btnComproCURPGuardada
            // 
            this.btnComproCURPGuardada.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btnComproCURPGuardada.FlatAppearance.BorderSize = 30;
            this.btnComproCURPGuardada.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnComproCURPGuardada.Image = global::pdv_uth_v1.Properties.Resources.upload_information_16316__1_;
            this.btnComproCURPGuardada.Location = new System.Drawing.Point(1080, 199);
            this.btnComproCURPGuardada.Name = "btnComproCURPGuardada";
            this.btnComproCURPGuardada.Size = new System.Drawing.Size(39, 39);
            this.btnComproCURPGuardada.TabIndex = 61;
            this.btnComproCURPGuardada.UseVisualStyleBackColor = true;
            this.btnComproCURPGuardada.Click += new System.EventHandler(this.btnComproINEGuardada_Click);
            // 
            // pictureBoxComproCURP
            // 
            this.pictureBoxComproCURP.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(141)))), ((int)(((byte)(84)))));
            this.pictureBoxComproCURP.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBoxComproCURP.Location = new System.Drawing.Point(881, 145);
            this.pictureBoxComproCURP.Name = "pictureBoxComproCURP";
            this.pictureBoxComproCURP.Size = new System.Drawing.Size(193, 93);
            this.pictureBoxComproCURP.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxComproCURP.TabIndex = 60;
            this.pictureBoxComproCURP.TabStop = false;
            // 
            // lblImagendeComproCURPGuardada
            // 
            this.lblImagendeComproCURPGuardada.AutoSize = true;
            this.lblImagendeComproCURPGuardada.Font = new System.Drawing.Font("Segoe Print", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblImagendeComproCURPGuardada.Location = new System.Drawing.Point(770, 178);
            this.lblImagendeComproCURPGuardada.Name = "lblImagendeComproCURPGuardada";
            this.lblImagendeComproCURPGuardada.Size = new System.Drawing.Size(97, 12);
            this.lblImagendeComproCURPGuardada.TabIndex = 58;
            this.lblImagendeComproCURPGuardada.Text = "Imagen del Comprobante";
            this.lblImagendeComproCURPGuardada.Visible = false;
            // 
            // lblImagendelComproGuardada
            // 
            this.lblImagendelComproGuardada.AutoSize = true;
            this.lblImagendelComproGuardada.Font = new System.Drawing.Font("Segoe Print", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblImagendelComproGuardada.Location = new System.Drawing.Point(275, 98);
            this.lblImagendelComproGuardada.Name = "lblImagendelComproGuardada";
            this.lblImagendelComproGuardada.Size = new System.Drawing.Size(97, 12);
            this.lblImagendelComproGuardada.TabIndex = 57;
            this.lblImagendelComproGuardada.Text = "Imagen del Comprobante";
            this.lblImagendelComproGuardada.Visible = false;
            // 
            // lblImagenComproDom
            // 
            this.lblImagenComproDom.AutoSize = true;
            this.lblImagenComproDom.Font = new System.Drawing.Font("Segoe Print", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblImagenComproDom.Location = new System.Drawing.Point(275, 110);
            this.lblImagenComproDom.Name = "lblImagenComproDom";
            this.lblImagenComproDom.Size = new System.Drawing.Size(97, 12);
            this.lblImagenComproDom.TabIndex = 56;
            this.lblImagenComproDom.Text = "Imagen del Comprobante";
            this.lblImagenComproDom.Visible = false;
            // 
            // btnCargarImgDomicilio
            // 
            this.btnCargarImgDomicilio.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btnCargarImgDomicilio.FlatAppearance.BorderSize = 30;
            this.btnCargarImgDomicilio.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnCargarImgDomicilio.Image = global::pdv_uth_v1.Properties.Resources._1486505253_folder_folder_up_folder_upload_update_folder_upload_81427__2_;
            this.btnCargarImgDomicilio.Location = new System.Drawing.Point(663, 115);
            this.btnCargarImgDomicilio.Name = "btnCargarImgDomicilio";
            this.btnCargarImgDomicilio.Size = new System.Drawing.Size(45, 44);
            this.btnCargarImgDomicilio.TabIndex = 55;
            this.btnCargarImgDomicilio.UseVisualStyleBackColor = true;
            this.btnCargarImgDomicilio.Click += new System.EventHandler(this.btnCargarImgDomicilio_Click);
            // 
            // pictureBoxDomicilio
            // 
            this.pictureBoxDomicilio.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(141)))), ((int)(((byte)(84)))));
            this.pictureBoxDomicilio.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBoxDomicilio.Location = new System.Drawing.Point(453, 86);
            this.pictureBoxDomicilio.Name = "pictureBoxDomicilio";
            this.pictureBoxDomicilio.Size = new System.Drawing.Size(204, 88);
            this.pictureBoxDomicilio.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxDomicilio.TabIndex = 54;
            this.pictureBoxDomicilio.TabStop = false;
            this.pictureBoxDomicilio.Click += new System.EventHandler(this.pictureBoxDomicilio_Click);
            // 
            // btnEditar
            // 
            this.btnEditar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(141)))), ((int)(((byte)(84)))));
            this.btnEditar.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btnEditar.FlatAppearance.BorderSize = 30;
            this.btnEditar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnEditar.Image = global::pdv_uth_v1.Properties.Resources.businessapplication_edit_male_user_thepencil_theclient_negocio_2321;
            this.btnEditar.Location = new System.Drawing.Point(969, 322);
            this.btnEditar.Name = "btnEditar";
            this.btnEditar.Size = new System.Drawing.Size(41, 43);
            this.btnEditar.TabIndex = 53;
            this.btnEditar.UseVisualStyleBackColor = false;
            this.btnEditar.Click += new System.EventHandler(this.btnEditar_Click);
            // 
            // btnEliminar
            // 
            this.btnEliminar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(141)))), ((int)(((byte)(84)))));
            this.btnEliminar.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btnEliminar.FlatAppearance.BorderSize = 30;
            this.btnEliminar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnEliminar.Image = global::pdv_uth_v1.Properties.Resources.delete_delete_deleteusers_delete_male_user_maleclient_2348;
            this.btnEliminar.Location = new System.Drawing.Point(891, 322);
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Size = new System.Drawing.Size(51, 46);
            this.btnEliminar.TabIndex = 52;
            this.btnEliminar.UseVisualStyleBackColor = false;
            this.btnEliminar.Click += new System.EventHandler(this.btnEliminar_Click);
            // 
            // btnGuardarUsuario
            // 
            this.btnGuardarUsuario.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(141)))), ((int)(((byte)(84)))));
            this.btnGuardarUsuario.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btnGuardarUsuario.FlatAppearance.BorderSize = 30;
            this.btnGuardarUsuario.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnGuardarUsuario.Image = global::pdv_uth_v1.Properties.Resources.business_application_addmale_useradd_insert_add_user_client_2312;
            this.btnGuardarUsuario.Location = new System.Drawing.Point(824, 322);
            this.btnGuardarUsuario.Name = "btnGuardarUsuario";
            this.btnGuardarUsuario.Size = new System.Drawing.Size(53, 46);
            this.btnGuardarUsuario.TabIndex = 51;
            this.btnGuardarUsuario.UseVisualStyleBackColor = false;
            this.btnGuardarUsuario.Click += new System.EventHandler(this.btnGuardarUsuario_Click);
            // 
            // btnLimpiar
            // 
            this.btnLimpiar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(141)))), ((int)(((byte)(84)))));
            this.btnLimpiar.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btnLimpiar.FlatAppearance.BorderSize = 30;
            this.btnLimpiar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnLimpiar.Image = global::pdv_uth_v1.Properties.Resources._3792081_broom_halloween_magic_witch_109049;
            this.btnLimpiar.Location = new System.Drawing.Point(1033, 325);
            this.btnLimpiar.Name = "btnLimpiar";
            this.btnLimpiar.Size = new System.Drawing.Size(41, 40);
            this.btnLimpiar.TabIndex = 49;
            this.btnLimpiar.UseVisualStyleBackColor = false;
            this.btnLimpiar.Click += new System.EventHandler(this.btnLimpiar_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::pdv_uth_v1.Properties.Resources.Logo3;
            this.pictureBox1.Location = new System.Drawing.Point(1121, 119);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(242, 232);
            this.pictureBox1.TabIndex = 47;
            this.pictureBox1.TabStop = false;
            // 
            // comboBoxTipoUsuario
            // 
            this.comboBoxTipoUsuario.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(189)))), ((int)(((byte)(84)))));
            this.comboBoxTipoUsuario.Font = new System.Drawing.Font("Segoe Print", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxTipoUsuario.FormattingEnabled = true;
            this.comboBoxTipoUsuario.Items.AddRange(new object[] {
            "ADMINISTRADOR",
            "CAJERO"});
            this.comboBoxTipoUsuario.Location = new System.Drawing.Point(1237, 9);
            this.comboBoxTipoUsuario.Name = "comboBoxTipoUsuario";
            this.comboBoxTipoUsuario.Size = new System.Drawing.Size(121, 27);
            this.comboBoxTipoUsuario.TabIndex = 21;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Segoe Print", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.label24.Location = new System.Drawing.Point(1108, 54);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(86, 23);
            this.label24.TabIndex = 46;
            this.label24.Text = "Contraseña";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Segoe Print", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.label23.Location = new System.Drawing.Point(1095, 9);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(114, 23);
            this.label23.TabIndex = 45;
            this.label23.Text = "Tipo de Usuario";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Segoe Print", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.label22.Location = new System.Drawing.Point(709, 302);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(176, 23);
            this.label22.TabIndex = 44;
            this.label22.Text = "Número de Seguro Social";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Segoe Print", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.label21.Location = new System.Drawing.Point(715, 9);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(162, 23);
            this.label21.TabIndex = 43;
            this.label21.Text = "Certificado de estudios";
            // 
            // txtNumSeguroSocial
            // 
            this.txtNumSeguroSocial.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(189)))), ((int)(((byte)(84)))));
            this.txtNumSeguroSocial.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtNumSeguroSocial.Font = new System.Drawing.Font("Segoe Print", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNumSeguroSocial.Location = new System.Drawing.Point(891, 302);
            this.txtNumSeguroSocial.Name = "txtNumSeguroSocial";
            this.txtNumSeguroSocial.Size = new System.Drawing.Size(193, 20);
            this.txtNumSeguroSocial.TabIndex = 20;
            // 
            // txtContraseña
            // 
            this.txtContraseña.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(189)))), ((int)(((byte)(84)))));
            this.txtContraseña.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtContraseña.Font = new System.Drawing.Font("Segoe Print", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtContraseña.Location = new System.Drawing.Point(1237, 54);
            this.txtContraseña.Name = "txtContraseña";
            this.txtContraseña.Size = new System.Drawing.Size(121, 20);
            this.txtContraseña.TabIndex = 22;
            // 
            // txtCURP
            // 
            this.txtCURP.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(189)))), ((int)(((byte)(84)))));
            this.txtCURP.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtCURP.Font = new System.Drawing.Font("Segoe Print", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCURP.Location = new System.Drawing.Point(891, 257);
            this.txtCURP.Name = "txtCURP";
            this.txtCURP.Size = new System.Drawing.Size(193, 20);
            this.txtCURP.TabIndex = 16;
            // 
            // txtCorreo
            // 
            this.txtCorreo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(189)))), ((int)(((byte)(84)))));
            this.txtCorreo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtCorreo.Font = new System.Drawing.Font("Segoe Print", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCorreo.Location = new System.Drawing.Point(132, 171);
            this.txtCorreo.Name = "txtCorreo";
            this.txtCorreo.Size = new System.Drawing.Size(100, 20);
            this.txtCorreo.TabIndex = 7;
            this.txtCorreo.Leave += new System.EventHandler(this.txtCorreo_Leave);
            // 
            // txtCalle
            // 
            this.txtCalle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(189)))), ((int)(((byte)(84)))));
            this.txtCalle.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtCalle.Font = new System.Drawing.Font("Segoe Print", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCalle.Location = new System.Drawing.Point(132, 198);
            this.txtCalle.Name = "txtCalle";
            this.txtCalle.Size = new System.Drawing.Size(100, 20);
            this.txtCalle.TabIndex = 8;
            // 
            // txtLocalidad
            // 
            this.txtLocalidad.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(189)))), ((int)(((byte)(84)))));
            this.txtLocalidad.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtLocalidad.Font = new System.Drawing.Font("Segoe Print", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLocalidad.Location = new System.Drawing.Point(375, 35);
            this.txtLocalidad.Name = "txtLocalidad";
            this.txtLocalidad.Size = new System.Drawing.Size(100, 20);
            this.txtLocalidad.TabIndex = 13;
            // 
            // txtMunicipio
            // 
            this.txtMunicipio.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(189)))), ((int)(((byte)(84)))));
            this.txtMunicipio.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtMunicipio.Font = new System.Drawing.Font("Segoe Print", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMunicipio.Location = new System.Drawing.Point(375, 64);
            this.txtMunicipio.Name = "txtMunicipio";
            this.txtMunicipio.Size = new System.Drawing.Size(100, 20);
            this.txtMunicipio.TabIndex = 14;
            // 
            // txtTelefono
            // 
            this.txtTelefono.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(189)))), ((int)(((byte)(84)))));
            this.txtTelefono.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtTelefono.Font = new System.Drawing.Font("Segoe Print", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTelefono.Location = new System.Drawing.Point(132, 145);
            this.txtTelefono.Name = "txtTelefono";
            this.txtTelefono.Size = new System.Drawing.Size(100, 20);
            this.txtTelefono.TabIndex = 6;
            this.txtTelefono.Leave += new System.EventHandler(this.txtTelefono_Leave);
            // 
            // txtCelular
            // 
            this.txtCelular.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(189)))), ((int)(((byte)(84)))));
            this.txtCelular.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtCelular.Font = new System.Drawing.Font("Segoe Print", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCelular.Location = new System.Drawing.Point(132, 119);
            this.txtCelular.Name = "txtCelular";
            this.txtCelular.Size = new System.Drawing.Size(100, 20);
            this.txtCelular.TabIndex = 5;
            this.txtCelular.TextChanged += new System.EventHandler(this.txtCelular_TextChanged);
            this.txtCelular.Leave += new System.EventHandler(this.txtCelular_Leave);
            // 
            // txtColonia
            // 
            this.txtColonia.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(189)))), ((int)(((byte)(84)))));
            this.txtColonia.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtColonia.Font = new System.Drawing.Font("Segoe Print", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtColonia.Location = new System.Drawing.Point(132, 283);
            this.txtColonia.Name = "txtColonia";
            this.txtColonia.Size = new System.Drawing.Size(100, 20);
            this.txtColonia.TabIndex = 11;
            // 
            // txtCP
            // 
            this.txtCP.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(189)))), ((int)(((byte)(84)))));
            this.txtCP.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtCP.Font = new System.Drawing.Font("Segoe Print", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCP.Location = new System.Drawing.Point(132, 257);
            this.txtCP.Name = "txtCP";
            this.txtCP.Size = new System.Drawing.Size(100, 20);
            this.txtCP.TabIndex = 10;
            this.txtCP.Leave += new System.EventHandler(this.txtCP_Leave);
            // 
            // txtFraccionamiento
            // 
            this.txtFraccionamiento.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(189)))), ((int)(((byte)(84)))));
            this.txtFraccionamiento.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtFraccionamiento.Font = new System.Drawing.Font("Segoe Print", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFraccionamiento.Location = new System.Drawing.Point(375, 9);
            this.txtFraccionamiento.Name = "txtFraccionamiento";
            this.txtFraccionamiento.Size = new System.Drawing.Size(100, 20);
            this.txtFraccionamiento.TabIndex = 12;
            // 
            // txtApellidoMaterno
            // 
            this.txtApellidoMaterno.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(189)))), ((int)(((byte)(84)))));
            this.txtApellidoMaterno.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtApellidoMaterno.Font = new System.Drawing.Font("Segoe Print", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtApellidoMaterno.Location = new System.Drawing.Point(132, 60);
            this.txtApellidoMaterno.Name = "txtApellidoMaterno";
            this.txtApellidoMaterno.Size = new System.Drawing.Size(100, 20);
            this.txtApellidoMaterno.TabIndex = 3;
            this.txtApellidoMaterno.Leave += new System.EventHandler(this.txtApellidoMaterno_Leave);
            // 
            // txtApellidoPaterno
            // 
            this.txtApellidoPaterno.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(189)))), ((int)(((byte)(84)))));
            this.txtApellidoPaterno.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtApellidoPaterno.Font = new System.Drawing.Font("Segoe Print", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtApellidoPaterno.Location = new System.Drawing.Point(132, 32);
            this.txtApellidoPaterno.Name = "txtApellidoPaterno";
            this.txtApellidoPaterno.Size = new System.Drawing.Size(100, 20);
            this.txtApellidoPaterno.TabIndex = 2;
            this.txtApellidoPaterno.Leave += new System.EventHandler(this.txtApellidoPaterno_Leave);
            // 
            // txtNumCasa
            // 
            this.txtNumCasa.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(189)))), ((int)(((byte)(84)))));
            this.txtNumCasa.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtNumCasa.Font = new System.Drawing.Font("Segoe Print", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNumCasa.Location = new System.Drawing.Point(132, 229);
            this.txtNumCasa.Name = "txtNumCasa";
            this.txtNumCasa.Size = new System.Drawing.Size(100, 20);
            this.txtNumCasa.TabIndex = 9;
            // 
            // txtNombre
            // 
            this.txtNombre.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(189)))), ((int)(((byte)(84)))));
            this.txtNombre.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtNombre.Font = new System.Drawing.Font("Segoe Print", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNombre.Location = new System.Drawing.Point(132, 6);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(100, 20);
            this.txtNombre.TabIndex = 1;
            this.txtNombre.Leave += new System.EventHandler(this.txtNombre_Leave);
            // 
            // lblComproDomicilio
            // 
            this.lblComproDomicilio.AutoSize = true;
            this.lblComproDomicilio.Font = new System.Drawing.Font("Segoe Print", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblComproDomicilio.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.lblComproDomicilio.Location = new System.Drawing.Point(273, 99);
            this.lblComproDomicilio.Name = "lblComproDomicilio";
            this.lblComproDomicilio.Size = new System.Drawing.Size(185, 23);
            this.lblComproDomicilio.TabIndex = 20;
            this.lblComproDomicilio.Text = "Comprobante de Domicilio";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Segoe Print", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.label18.Location = new System.Drawing.Point(249, 188);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(149, 23);
            this.label18.TabIndex = 18;
            this.label18.Text = "Comprobante de INE";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Segoe Print", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.label17.Location = new System.Drawing.Point(746, 257);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(47, 23);
            this.label17.TabIndex = 17;
            this.label17.Text = "CURP";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Segoe Print", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.label16.Location = new System.Drawing.Point(713, 181);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(162, 23);
            this.label16.TabIndex = 16;
            this.label16.Text = "Comprobante de CURP";
            // 
            // dtpFechaDeNacimiento
            // 
            this.dtpFechaDeNacimiento.Font = new System.Drawing.Font("Segoe Print", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpFechaDeNacimiento.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpFechaDeNacimiento.Location = new System.Drawing.Point(152, 86);
            this.dtpFechaDeNacimiento.Name = "dtpFechaDeNacimiento";
            this.dtpFechaDeNacimiento.Size = new System.Drawing.Size(115, 27);
            this.dtpFechaDeNacimiento.TabIndex = 4;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Segoe Print", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.label15.Location = new System.Drawing.Point(6, 257);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(36, 23);
            this.label15.TabIndex = 14;
            this.label15.Text = "C. P";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Segoe Print", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.label14.Location = new System.Drawing.Point(3, 82);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(151, 23);
            this.label14.TabIndex = 13;
            this.label14.Text = "Fecha De Nacimiento";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Segoe Print", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.label13.Location = new System.Drawing.Point(3, 226);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(121, 23);
            this.label13.TabIndex = 12;
            this.label13.Text = "Número De Casa";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Segoe Print", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.label12.Location = new System.Drawing.Point(5, 26);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(121, 23);
            this.label12.TabIndex = 11;
            this.label12.Text = "Apellido Paterno";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Segoe Print", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.label11.Location = new System.Drawing.Point(6, 109);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(57, 23);
            this.label11.TabIndex = 10;
            this.label11.Text = "Celular";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Segoe Print", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.label10.Location = new System.Drawing.Point(3, 136);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(65, 23);
            this.label10.TabIndex = 9;
            this.label10.Text = "Teléfono";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Segoe Print", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.label9.Location = new System.Drawing.Point(5, 284);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(58, 23);
            this.label9.TabIndex = 8;
            this.label9.Text = "Colonia";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Segoe Print", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.label8.Location = new System.Drawing.Point(249, 64);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(73, 23);
            this.label8.TabIndex = 7;
            this.label8.Text = "Municipio";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Segoe Print", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.label7.Location = new System.Drawing.Point(248, 6);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(121, 23);
            this.label7.TabIndex = 6;
            this.label7.Text = "Fraccionamiento";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Segoe Print", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.label6.Location = new System.Drawing.Point(248, 35);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(74, 23);
            this.label6.TabIndex = 5;
            this.label6.Text = "Localidad";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe Print", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.label5.Location = new System.Drawing.Point(3, 54);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(124, 23);
            this.label5.TabIndex = 4;
            this.label5.Text = "Apellido Materno";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe Print", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.label4.Location = new System.Drawing.Point(6, 161);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(54, 23);
            this.label4.TabIndex = 3;
            this.label4.Text = "Correo";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe Print", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.label3.Location = new System.Drawing.Point(3, 195);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 23);
            this.label3.TabIndex = 2;
            this.label3.Text = "Calle";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe Print", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.label2.Location = new System.Drawing.Point(12, 3);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 23);
            this.label2.TabIndex = 1;
            this.label2.Text = "Nombre";
            // 
            // errorProviderForm
            // 
            this.errorProviderForm.ContainerControl = this;
            this.errorProviderForm.Icon = ((System.Drawing.Icon)(resources.GetObject("errorProviderForm.Icon")));
            // 
            // panelDataGrid
            // 
            this.panelDataGrid.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(207)))), ((int)(((byte)(167)))));
            this.panelDataGrid.Controls.Add(this.dgUsuarios);
            this.panelDataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelDataGrid.Location = new System.Drawing.Point(0, 420);
            this.panelDataGrid.Name = "panelDataGrid";
            this.panelDataGrid.Size = new System.Drawing.Size(1366, 93);
            this.panelDataGrid.TabIndex = 3;
            // 
            // dgUsuarios
            // 
            this.dgUsuarios.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgUsuarios.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(156)))), ((int)(((byte)(0)))));
            this.dgUsuarios.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(207)))), ((int)(((byte)(167)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe Print", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(156)))), ((int)(((byte)(0)))));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(141)))), ((int)(((byte)(84)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(156)))), ((int)(((byte)(0)))));
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgUsuarios.DefaultCellStyle = dataGridViewCellStyle1;
            this.dgUsuarios.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgUsuarios.Location = new System.Drawing.Point(0, 0);
            this.dgUsuarios.Name = "dgUsuarios";
            this.dgUsuarios.Size = new System.Drawing.Size(1366, 93);
            this.dgUsuarios.TabIndex = 0;
            this.dgUsuarios.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgUsuarios_CellContentClick_1);
            // 
            // openFileDialogImagenACargar
            // 
            this.openFileDialogImagenACargar.FileName = "openFileDialog1";
            // 
            // FrmUsuarios
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1366, 513);
            this.Controls.Add(this.panelDataGrid);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmUsuarios";
            this.Text = "FrmUsuarios";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FrmUsuarios_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxNext)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCertificadoEstudios)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxActaNacimiento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxComproINE)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxComproCURP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxDomicilio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderForm)).EndInit();
            this.panelDataGrid.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgUsuarios)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ComboBox comboBoxTipoUsuario;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtNumSeguroSocial;
        private System.Windows.Forms.TextBox txtContraseña;
        private System.Windows.Forms.TextBox txtCURP;
        private System.Windows.Forms.TextBox txtCorreo;
        private System.Windows.Forms.TextBox txtCalle;
        private System.Windows.Forms.TextBox txtLocalidad;
        private System.Windows.Forms.TextBox txtMunicipio;
        private System.Windows.Forms.TextBox txtTelefono;
        private System.Windows.Forms.TextBox txtCelular;
        private System.Windows.Forms.TextBox txtColonia;
        private System.Windows.Forms.TextBox txtCP;
        private System.Windows.Forms.TextBox txtFraccionamiento;
        private System.Windows.Forms.TextBox txtApellidoMaterno;
        private System.Windows.Forms.TextBox txtApellidoPaterno;
        private System.Windows.Forms.TextBox txtNumCasa;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.Label lblComproDomicilio;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.DateTimePicker dtpFechaDeNacimiento;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btnEditar;
        private System.Windows.Forms.Button btnEliminar;
        private System.Windows.Forms.Button btnGuardarUsuario;
        private System.Windows.Forms.Button btnLimpiar;
        private System.Windows.Forms.ErrorProvider errorProviderForm;
        private System.Windows.Forms.Button btnCerrar;
        private System.Windows.Forms.Panel panelDataGrid;
        private System.Windows.Forms.PictureBox pictureBoxDomicilio;
        private System.Windows.Forms.OpenFileDialog openFileDialogImagenACargar;
        private System.Windows.Forms.Button btnCargarImgDomicilio;
        private System.Windows.Forms.Label lblImagenComproDom;
        private System.Windows.Forms.Label lblImagendelComproGuardada;
        private System.Windows.Forms.Button btnComproCURPGuardada;
        private System.Windows.Forms.PictureBox pictureBoxComproCURP;
        private System.Windows.Forms.Label lblImagenComprobanteINE;
        private System.Windows.Forms.Label lblImagendeComproCURPGuardada;
        private System.Windows.Forms.PictureBox pictureBoxComproINE;
        //private System.Windows.Forms.Label lblImagenComprobanteINE;
        private System.Windows.Forms.Label lblImagenComproINEGuardada;
        private System.Windows.Forms.Button btnComproINEGuardada;
        private System.Windows.Forms.Label lblImagenComprobanteCURP;
        private System.Windows.Forms.Label lblImagenComproINE;
        private System.Windows.Forms.Button btnActaNacimientoGuardar;
        private System.Windows.Forms.PictureBox pictureBoxActaNacimiento;
        private System.Windows.Forms.Label lblImagenActaNacimiento;
        private System.Windows.Forms.Label lblImagenActaNacGuardada;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Button btnCertificadoEstudiosCargar;
        private System.Windows.Forms.PictureBox pictureBoxCertificadoEstudios;
        private System.Windows.Forms.Label lblImagenCertificadoEstudios;
        private System.Windows.Forms.Label lblCertificadoEstudiosGuardada;
        private System.Windows.Forms.DataGridView dgUsuarios;
        private System.Windows.Forms.PictureBox pictureBoxNext;
        private FontAwesome.Sharp.IconButton iconBtnHome;
        private FontAwesome.Sharp.IconButton iconBtnIngresarAUCliente;
        private System.Windows.Forms.ToolTip toolTipUsers;
    }
}