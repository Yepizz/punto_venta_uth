﻿using System;
using System.Drawing;
using System.Windows.Forms;
using Lib_pdv_uth_v1.usuarios;
using System.Threading;
using System.Net.NetworkInformation;
using System.Linq;

namespace pdv_uth_v1
{

    public partial class FrmLogin : Form
    {
        //instancia de usuario
        public static Usuario us = new Usuario();
        public static string nombrePersona;
        public FrmLogin()
        {
            Thread t = new Thread(new ThreadStart(pantalla));
            t.Start();
            Thread.Sleep(8000);
            InitializeComponent();
            t.Abort();


        }
        public void pantalla() { Application.Run(new Start()); }
        public void StartForm()
        {
            Application.Run(new Start());
        }


        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            //cerrar la forma
            if (MessageBox.Show("¿Deseas salir?", "¿Cerrar?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                this.Close();
            }
        }
        private void limpiarText()
        {
            //Se limpia los textos y el usuario
            txtCorreo.Text = txtContraseña.Text = "";
            FrmLogin.us = null;
        }
        private void FrmLogin_Load(object sender, EventArgs e)
        {
            toolTipLogin.SetToolTip(txtCorreo, "Ingresar un correo eléctronico. ");
            toolTipLogin.SetToolTip(txtContraseña, "Ingresar la contraseña de inicio de sesión. ");
            toolTipLogin.SetToolTip(btnIngresar, "Iniciar Sesión. ");
            toolTipLogin.SetToolTip(btnCancelar, "Cancelar inicio de sesión. ");
            toolTipLogin.SetToolTip(linkLblRecuperarPWD, "¿Olvidaste tu contraseña?, Restablecela dando un click aquí. ");
            txtCorreo.Focus();

            var macAddress = (from nic in NetworkInterface.GetAllNetworkInterfaces()
                           where nic.OperationalStatus == OperationalStatus.Up
                           select nic.GetPhysicalAddress().ToString()).FirstOrDefault();
            lblMacAddress.Text = macAddress;
        }




        private void txtCorreo_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtContraseña_TextChanged(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void txtCorreo_Leave(object sender, EventArgs e)
        {
            btnIngresar.Enabled = validarLogin();
            btnIngresar.BackColor = btnIngresar.Enabled ? Color.WhiteSmoke : Color.DimGray;
        }

        private bool validarLogin()
        {
            if (txtContraseña.Text != "" && txtCorreo.Text != "")
            {
                //Verificamos si los campos no se encuentren vacios
                if (txtContraseña.Text.Length >= 6)
                {
                    if (txtCorreo.Text.Contains("@") && (txtCorreo.Text.Contains(".edu") || txtCorreo.Text.Contains(".com")) && txtCorreo.Text.Length >= 7)
                    {
                        return true;
                    }
                    else
                    {
                        MessageBox.Show("Error en tu correo");
                        return false;
                    }

                }
                else
                {
                    MessageBox.Show("Error en tu contraseña para ingresar");
                    return false;
                }
            }
            else
            {
                MessageBox.Show("Capture el correo y contraseña");
                return false;
            }
        }

        private void txtContraseña_Leave(object sender, EventArgs e)
        {
            //Verificamos que ya contenga un correo y contraseña activo
            btnIngresar.Enabled = validarLogin();
            btnIngresar.BackColor = btnIngresar.Enabled ? Color.WhiteSmoke : Color.DimGray;
        }

        private void btnIngresar_Click_1(object sender, EventArgs e)
        {

            //login
            if (us.login(txtCorreo.Text, txtContraseña.Text) == TipoUsuario.ADMINISTRADOR)
            {
                //abrimos el menu principal de ADMIN
                MenuPrincipal priii = new MenuPrincipal();
                //escondemos el login
                this.Hide();
                //mostramos la forma de DIALOG para que est{e sobre todas enfrente
                priii.ShowDialog();

            }
            else if (us.login(txtCorreo.Text, txtContraseña.Text) == TipoUsuario.CAJERO)
            {
                //abrimos el Caja
                FormCaja frmcaja = new FormCaja();
                //escondemos el l0gin
                this.Hide();
                //mostramos la forma de DIALOG para que est{e sobre todas enfrente
                frmcaja.ShowDialog();

            }
            else
            {
                //ERROR
                MessageBox.Show("Usuario no existe", "Error al ingresar", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }


        private void txtContraseña_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                btnIngresar_Click_1(sender, e);
            }
        }

        private void btnIngresar_TextChanged(object sender, EventArgs e)
        {
            txtContraseña.PasswordChar = '*';

            txtContraseña.MaxLength = 30;
        }

        private void checkBoxShowPassword_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxShowPassword.CheckState == CheckState.Checked)
            {
                txtContraseña.UseSystemPasswordChar = false;

            }
            else
            {
                txtContraseña.UseSystemPasswordChar = true;
            }
        }

        private void linkLblRecuperarPWD_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            LibNotificaciones.CorreoElectronico recuperarPassword = new LibNotificaciones.CorreoElectronico();

            if (txtCorreo.Text.Contains("@") && (txtCorreo.Text.Contains(".edu") || txtCorreo.Text.Contains(".com")) && txtCorreo.Text.Length >= 13)
            {
                if (recuperarPassword.notificar(txtCorreo.Text, "Prueba", "Para restablecer tu contraseña de nuevo utiliza: 609-837. "))
                {
                    MessageBox.Show("Tu contraseña se reenvio a tu correo...", "Recuperacion de contraseña", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show("Escribe tu correo registrado para recuperar contraseña", "Restablecer contraseña. ", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }

        private void lblMacAddress_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {

        }
    }
}

