﻿namespace pdv_uth_v1
{
    partial class FrmCatalogoProductos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmCatalogoProductos));
            this.panel1 = new System.Windows.Forms.Panel();
            this.iconBtnCerrar = new FontAwesome.Sharp.IconButton();
            this.iconBtnCasa = new FontAwesome.Sharp.IconButton();
            this.label1 = new System.Windows.Forms.Label();
            this.panelCatalogoProductosForm = new System.Windows.Forms.Panel();
            this.flowLayoutPanelAccionesCrud = new System.Windows.Forms.FlowLayoutPanel();
            this.btnAgregar = new System.Windows.Forms.Button();
            this.btnEditar = new System.Windows.Forms.Button();
            this.btnEliminar = new System.Windows.Forms.Button();
            this.btnCerrar = new System.Windows.Forms.Button();
            this.btnLimpiar = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dgProductos = new System.Windows.Forms.DataGridView();
            this.iconBtnBack = new FontAwesome.Sharp.IconButton();
            this.lblText = new System.Windows.Forms.Label();
            this.pictureBoxLogo = new System.Windows.Forms.PictureBox();
            this.picBoxCodBarras = new System.Windows.Forms.PictureBox();
            this.lblImagenGuardada = new System.Windows.Forms.Label();
            this.lblImagenProd = new System.Windows.Forms.Label();
            this.btnCargarImagen = new System.Windows.Forms.Button();
            this.checkPerecedero = new System.Windows.Forms.CheckBox();
            this.comboUnidadDeMedida = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtDescripcion = new System.Windows.Forms.TextBox();
            this.txtPrecio = new System.Windows.Forms.TextBox();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.txtCodigoBarras = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.pictureBoxImagenProd = new System.Windows.Forms.PictureBox();
            this.openFileDialogImagenACargar = new System.Windows.Forms.OpenFileDialog();
            this.errorProviderForm = new System.Windows.Forms.ErrorProvider(this.components);
            this.toolTipProductos = new System.Windows.Forms.ToolTip(this.components);
            this.panel1.SuspendLayout();
            this.panelCatalogoProductosForm.SuspendLayout();
            this.flowLayoutPanelAccionesCrud.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgProductos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxCodBarras)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxImagenProd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderForm)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(171)))), ((int)(((byte)(39)))));
            this.panel1.Controls.Add(this.iconBtnCerrar);
            this.panel1.Controls.Add(this.iconBtnCasa);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1115, 49);
            this.panel1.TabIndex = 0;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // iconBtnCerrar
            // 
            this.iconBtnCerrar.Dock = System.Windows.Forms.DockStyle.Right;
            this.iconBtnCerrar.FlatAppearance.BorderSize = 0;
            this.iconBtnCerrar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.iconBtnCerrar.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.iconBtnCerrar.IconChar = FontAwesome.Sharp.IconChar.PowerOff;
            this.iconBtnCerrar.IconColor = System.Drawing.Color.Red;
            this.iconBtnCerrar.IconSize = 50;
            this.iconBtnCerrar.Location = new System.Drawing.Point(936, 0);
            this.iconBtnCerrar.Name = "iconBtnCerrar";
            this.iconBtnCerrar.Rotation = 0D;
            this.iconBtnCerrar.Size = new System.Drawing.Size(89, 49);
            this.iconBtnCerrar.TabIndex = 38;
            this.iconBtnCerrar.UseVisualStyleBackColor = true;
            this.iconBtnCerrar.Click += new System.EventHandler(this.iconBtnCerrar_Click);
            // 
            // iconBtnCasa
            // 
            this.iconBtnCasa.Dock = System.Windows.Forms.DockStyle.Right;
            this.iconBtnCasa.FlatAppearance.BorderSize = 0;
            this.iconBtnCasa.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.iconBtnCasa.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.errorProviderForm.SetIconAlignment(this.iconBtnCasa, System.Windows.Forms.ErrorIconAlignment.MiddleLeft);
            this.iconBtnCasa.IconChar = FontAwesome.Sharp.IconChar.Home;
            this.iconBtnCasa.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(29)))), ((int)(((byte)(187)))), ((int)(((byte)(123)))));
            this.iconBtnCasa.IconSize = 55;
            this.iconBtnCasa.Location = new System.Drawing.Point(1025, 0);
            this.iconBtnCasa.Name = "iconBtnCasa";
            this.iconBtnCasa.Rotation = 0D;
            this.iconBtnCasa.Size = new System.Drawing.Size(90, 49);
            this.iconBtnCasa.TabIndex = 37;
            this.iconBtnCasa.UseVisualStyleBackColor = true;
            this.iconBtnCasa.Click += new System.EventHandler(this.iconBtnClose_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe Print", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(176)))), ((int)(((byte)(105)))));
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(320, 42);
            this.label1.TabIndex = 0;
            this.label1.Text = "Formulario de productos";
            // 
            // panelCatalogoProductosForm
            // 
            this.panelCatalogoProductosForm.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(189)))), ((int)(((byte)(84)))));
            this.panelCatalogoProductosForm.Controls.Add(this.flowLayoutPanelAccionesCrud);
            this.panelCatalogoProductosForm.Controls.Add(this.panel2);
            this.panelCatalogoProductosForm.Controls.Add(this.iconBtnBack);
            this.panelCatalogoProductosForm.Controls.Add(this.lblText);
            this.panelCatalogoProductosForm.Controls.Add(this.pictureBoxLogo);
            this.panelCatalogoProductosForm.Controls.Add(this.picBoxCodBarras);
            this.panelCatalogoProductosForm.Controls.Add(this.lblImagenGuardada);
            this.panelCatalogoProductosForm.Controls.Add(this.lblImagenProd);
            this.panelCatalogoProductosForm.Controls.Add(this.btnCargarImagen);
            this.panelCatalogoProductosForm.Controls.Add(this.checkPerecedero);
            this.panelCatalogoProductosForm.Controls.Add(this.comboUnidadDeMedida);
            this.panelCatalogoProductosForm.Controls.Add(this.label3);
            this.panelCatalogoProductosForm.Controls.Add(this.txtDescripcion);
            this.panelCatalogoProductosForm.Controls.Add(this.txtPrecio);
            this.panelCatalogoProductosForm.Controls.Add(this.txtNombre);
            this.panelCatalogoProductosForm.Controls.Add(this.txtCodigoBarras);
            this.panelCatalogoProductosForm.Controls.Add(this.label11);
            this.panelCatalogoProductosForm.Controls.Add(this.label10);
            this.panelCatalogoProductosForm.Controls.Add(this.label8);
            this.panelCatalogoProductosForm.Controls.Add(this.label2);
            this.panelCatalogoProductosForm.Controls.Add(this.pictureBoxImagenProd);
            this.panelCatalogoProductosForm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelCatalogoProductosForm.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(141)))), ((int)(((byte)(84)))));
            this.panelCatalogoProductosForm.Location = new System.Drawing.Point(0, 49);
            this.panelCatalogoProductosForm.Name = "panelCatalogoProductosForm";
            this.panelCatalogoProductosForm.Size = new System.Drawing.Size(1115, 478);
            this.panelCatalogoProductosForm.TabIndex = 1;
            this.panelCatalogoProductosForm.Paint += new System.Windows.Forms.PaintEventHandler(this.panelCatalogoProductosForm_Paint);
            // 
            // flowLayoutPanelAccionesCrud
            // 
            this.flowLayoutPanelAccionesCrud.Controls.Add(this.btnAgregar);
            this.flowLayoutPanelAccionesCrud.Controls.Add(this.btnEditar);
            this.flowLayoutPanelAccionesCrud.Controls.Add(this.btnEliminar);
            this.flowLayoutPanelAccionesCrud.Controls.Add(this.btnCerrar);
            this.flowLayoutPanelAccionesCrud.Controls.Add(this.btnLimpiar);
            this.flowLayoutPanelAccionesCrud.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.flowLayoutPanelAccionesCrud.Location = new System.Drawing.Point(0, 288);
            this.flowLayoutPanelAccionesCrud.Name = "flowLayoutPanelAccionesCrud";
            this.flowLayoutPanelAccionesCrud.Size = new System.Drawing.Size(1115, 90);
            this.flowLayoutPanelAccionesCrud.TabIndex = 40;
            // 
            // btnAgregar
            // 
            this.btnAgregar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(171)))), ((int)(((byte)(39)))));
            this.btnAgregar.FlatAppearance.BorderSize = 30;
            this.btnAgregar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnAgregar.Image = global::pdv_uth_v1.Properties.Resources.shopping_basket_add256_24909__1_;
            this.btnAgregar.Location = new System.Drawing.Point(3, 3);
            this.btnAgregar.Name = "btnAgregar";
            this.btnAgregar.Size = new System.Drawing.Size(105, 87);
            this.btnAgregar.TabIndex = 0;
            this.btnAgregar.UseVisualStyleBackColor = false;
            this.btnAgregar.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnEditar
            // 
            this.btnEditar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(29)))), ((int)(((byte)(187)))), ((int)(((byte)(123)))));
            this.btnEditar.FlatAppearance.BorderSize = 30;
            this.btnEditar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnEditar.Image = global::pdv_uth_v1.Properties.Resources.documentediting_editdocuments_text_documentedi_2820;
            this.btnEditar.Location = new System.Drawing.Point(114, 3);
            this.btnEditar.Name = "btnEditar";
            this.btnEditar.Size = new System.Drawing.Size(116, 87);
            this.btnEditar.TabIndex = 2;
            this.btnEditar.UseVisualStyleBackColor = false;
            this.btnEditar.Click += new System.EventHandler(this.btnEditar_Click);
            // 
            // btnEliminar
            // 
            this.btnEliminar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(171)))), ((int)(((byte)(39)))));
            this.btnEliminar.FlatAppearance.BorderSize = 30;
            this.btnEliminar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnEliminar.Image = global::pdv_uth_v1.Properties.Resources.recycle_recyclebin_full_delete_trash_1772;
            this.btnEliminar.Location = new System.Drawing.Point(236, 3);
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Size = new System.Drawing.Size(146, 87);
            this.btnEliminar.TabIndex = 1;
            this.btnEliminar.UseVisualStyleBackColor = false;
            this.btnEliminar.Click += new System.EventHandler(this.btnEliminar_Click);
            // 
            // btnCerrar
            // 
            this.btnCerrar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(29)))), ((int)(((byte)(187)))), ((int)(((byte)(123)))));
            this.btnCerrar.FlatAppearance.BorderSize = 30;
            this.btnCerrar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnCerrar.Image = global::pdv_uth_v1.Properties.Resources.forceexit_103817;
            this.btnCerrar.Location = new System.Drawing.Point(388, 3);
            this.btnCerrar.Name = "btnCerrar";
            this.btnCerrar.Size = new System.Drawing.Size(127, 87);
            this.btnCerrar.TabIndex = 3;
            this.btnCerrar.UseVisualStyleBackColor = false;
            this.btnCerrar.Click += new System.EventHandler(this.btnCerrar_Click_1);
            // 
            // btnLimpiar
            // 
            this.btnLimpiar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(171)))), ((int)(((byte)(39)))));
            this.btnLimpiar.FlatAppearance.BorderSize = 30;
            this.btnLimpiar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnLimpiar.Image = global::pdv_uth_v1.Properties.Resources._3792081_broom_halloween_magic_witch_109049__2_;
            this.btnLimpiar.Location = new System.Drawing.Point(521, 3);
            this.btnLimpiar.Name = "btnLimpiar";
            this.btnLimpiar.Size = new System.Drawing.Size(118, 87);
            this.btnLimpiar.TabIndex = 4;
            this.btnLimpiar.UseVisualStyleBackColor = false;
            this.btnLimpiar.Click += new System.EventHandler(this.btnCerrar_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.panel2.Controls.Add(this.dgProductos);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 378);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1115, 100);
            this.panel2.TabIndex = 39;
            // 
            // dgProductos
            // 
            this.dgProductos.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgProductos.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(156)))), ((int)(((byte)(0)))));
            this.dgProductos.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgProductos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(207)))), ((int)(((byte)(167)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe Print", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(141)))), ((int)(((byte)(84)))));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(189)))), ((int)(((byte)(84)))));
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgProductos.DefaultCellStyle = dataGridViewCellStyle1;
            this.dgProductos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgProductos.Location = new System.Drawing.Point(0, 0);
            this.dgProductos.Name = "dgProductos";
            this.dgProductos.RowHeadersWidth = 80;
            this.dgProductos.Size = new System.Drawing.Size(1115, 100);
            this.dgProductos.TabIndex = 0;
            this.dgProductos.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgProductos_CellContentClick);
            // 
            // iconBtnBack
            // 
            this.iconBtnBack.FlatAppearance.BorderSize = 0;
            this.iconBtnBack.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.iconBtnBack.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.errorProviderForm.SetIconAlignment(this.iconBtnBack, System.Windows.Forms.ErrorIconAlignment.MiddleLeft);
            this.iconBtnBack.IconChar = FontAwesome.Sharp.IconChar.ArrowLeft;
            this.iconBtnBack.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.iconBtnBack.IconSize = 55;
            this.iconBtnBack.Location = new System.Drawing.Point(1322, 287);
            this.iconBtnBack.Name = "iconBtnBack";
            this.iconBtnBack.Rotation = 0D;
            this.iconBtnBack.Size = new System.Drawing.Size(39, 44);
            this.iconBtnBack.TabIndex = 38;
            this.iconBtnBack.UseVisualStyleBackColor = true;
            this.iconBtnBack.Click += new System.EventHandler(this.iconBtnBack_Click);
            // 
            // lblText
            // 
            this.lblText.AutoSize = true;
            this.lblText.Location = new System.Drawing.Point(33, 257);
            this.lblText.Name = "lblText";
            this.lblText.Size = new System.Drawing.Size(0, 13);
            this.lblText.TabIndex = 36;
            this.lblText.Visible = false;
            // 
            // pictureBoxLogo
            // 
            this.pictureBoxLogo.Image = global::pdv_uth_v1.Properties.Resources.Logo2;
            this.pictureBoxLogo.Location = new System.Drawing.Point(1102, 0);
            this.pictureBoxLogo.Name = "pictureBoxLogo";
            this.pictureBoxLogo.Size = new System.Drawing.Size(261, 250);
            this.pictureBoxLogo.TabIndex = 34;
            this.pictureBoxLogo.TabStop = false;
            // 
            // picBoxCodBarras
            // 
            this.picBoxCodBarras.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(29)))), ((int)(((byte)(187)))), ((int)(((byte)(123)))));
            this.picBoxCodBarras.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.picBoxCodBarras.Location = new System.Drawing.Point(233, 178);
            this.picBoxCodBarras.Name = "picBoxCodBarras";
            this.picBoxCodBarras.Size = new System.Drawing.Size(104, 50);
            this.picBoxCodBarras.TabIndex = 32;
            this.picBoxCodBarras.TabStop = false;
            // 
            // lblImagenGuardada
            // 
            this.lblImagenGuardada.AutoSize = true;
            this.lblImagenGuardada.Font = new System.Drawing.Font("Segoe Print", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblImagenGuardada.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(141)))), ((int)(((byte)(84)))));
            this.lblImagenGuardada.Location = new System.Drawing.Point(575, 50);
            this.lblImagenGuardada.Name = "lblImagenGuardada";
            this.lblImagenGuardada.Size = new System.Drawing.Size(155, 28);
            this.lblImagenGuardada.TabIndex = 31;
            this.lblImagenGuardada.Text = "Imagen a guardar";
            this.lblImagenGuardada.Visible = false;
            // 
            // lblImagenProd
            // 
            this.lblImagenProd.AutoSize = true;
            this.lblImagenProd.Font = new System.Drawing.Font("Segoe Print", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblImagenProd.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(141)))), ((int)(((byte)(84)))));
            this.lblImagenProd.Location = new System.Drawing.Point(575, 5);
            this.lblImagenProd.Name = "lblImagenProd";
            this.lblImagenProd.Size = new System.Drawing.Size(179, 28);
            this.lblImagenProd.TabIndex = 30;
            this.lblImagenProd.Text = "Imagen del producto";
            this.lblImagenProd.Visible = false;
            // 
            // btnCargarImagen
            // 
            this.btnCargarImagen.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(119)))), ((int)(((byte)(71)))));
            this.btnCargarImagen.FlatAppearance.BorderSize = 30;
            this.btnCargarImagen.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnCargarImagen.Image = global::pdv_uth_v1.Properties.Resources.logito__5_;
            this.btnCargarImagen.Location = new System.Drawing.Point(684, 105);
            this.btnCargarImagen.Name = "btnCargarImagen";
            this.btnCargarImagen.Size = new System.Drawing.Size(148, 123);
            this.btnCargarImagen.TabIndex = 8;
            this.btnCargarImagen.UseVisualStyleBackColor = false;
            this.btnCargarImagen.Click += new System.EventHandler(this.btnCargarImagen_Click);
            // 
            // checkPerecedero
            // 
            this.checkPerecedero.AutoSize = true;
            this.checkPerecedero.Font = new System.Drawing.Font("Segoe Print", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkPerecedero.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(141)))), ((int)(((byte)(84)))));
            this.checkPerecedero.Location = new System.Drawing.Point(12, 247);
            this.checkPerecedero.Name = "checkPerecedero";
            this.checkPerecedero.Size = new System.Drawing.Size(158, 32);
            this.checkPerecedero.TabIndex = 4;
            this.checkPerecedero.Text = "¿Es Perecedero?";
            this.checkPerecedero.UseVisualStyleBackColor = true;
            // 
            // comboUnidadDeMedida
            // 
            this.comboUnidadDeMedida.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(156)))), ((int)(((byte)(0)))));
            this.comboUnidadDeMedida.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.comboUnidadDeMedida.Font = new System.Drawing.Font("Segoe Print", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboUnidadDeMedida.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(141)))), ((int)(((byte)(84)))));
            this.comboUnidadDeMedida.FormattingEnabled = true;
            this.comboUnidadDeMedida.Items.AddRange(new object[] {
            "UNIDAD",
            "LITRO",
            "KILO",
            "CAJA",
            "PAQUETE"});
            this.comboUnidadDeMedida.Location = new System.Drawing.Point(355, 133);
            this.comboUnidadDeMedida.Name = "comboUnidadDeMedida";
            this.comboUnidadDeMedida.Size = new System.Drawing.Size(173, 31);
            this.comboUnidadDeMedida.TabIndex = 5;
            this.comboUnidadDeMedida.Text = "UNIDAD";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe Print", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(141)))), ((int)(((byte)(84)))));
            this.label3.Location = new System.Drawing.Point(350, 91);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(162, 28);
            this.label3.TabIndex = 24;
            this.label3.Text = "Unidad de medida";
            // 
            // txtDescripcion
            // 
            this.txtDescripcion.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(156)))), ((int)(((byte)(0)))));
            this.txtDescripcion.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtDescripcion.Font = new System.Drawing.Font("Segoe Print", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDescripcion.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.txtDescripcion.Location = new System.Drawing.Point(355, 49);
            this.txtDescripcion.Name = "txtDescripcion";
            this.txtDescripcion.Size = new System.Drawing.Size(214, 29);
            this.txtDescripcion.TabIndex = 4;
            // 
            // txtPrecio
            // 
            this.txtPrecio.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(156)))), ((int)(((byte)(0)))));
            this.txtPrecio.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtPrecio.Font = new System.Drawing.Font("Segoe Print", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPrecio.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.txtPrecio.Location = new System.Drawing.Point(10, 111);
            this.txtPrecio.Name = "txtPrecio";
            this.txtPrecio.Size = new System.Drawing.Size(98, 29);
            this.txtPrecio.TabIndex = 2;
            // 
            // txtNombre
            // 
            this.txtNombre.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(156)))), ((int)(((byte)(0)))));
            this.txtNombre.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtNombre.Font = new System.Drawing.Font("Segoe Print", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNombre.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.txtNombre.Location = new System.Drawing.Point(10, 36);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(257, 29);
            this.txtNombre.TabIndex = 1;
            this.txtNombre.Leave += new System.EventHandler(this.txtNombre_Leave);
            // 
            // txtCodigoBarras
            // 
            this.txtCodigoBarras.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(156)))), ((int)(((byte)(0)))));
            this.txtCodigoBarras.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtCodigoBarras.Font = new System.Drawing.Font("Segoe Print", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCodigoBarras.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.txtCodigoBarras.Location = new System.Drawing.Point(10, 199);
            this.txtCodigoBarras.Name = "txtCodigoBarras";
            this.txtCodigoBarras.Size = new System.Drawing.Size(197, 29);
            this.txtCodigoBarras.TabIndex = 3;
            this.txtCodigoBarras.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCodigoBarras_KeyPress_1);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Segoe Print", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(141)))), ((int)(((byte)(84)))));
            this.label11.Location = new System.Drawing.Point(5, 5);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(184, 28);
            this.label11.TabIndex = 15;
            this.label11.Text = "Nombre del producto";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Segoe Print", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(141)))), ((int)(((byte)(84)))));
            this.label10.Location = new System.Drawing.Point(360, 5);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(104, 28);
            this.label10.TabIndex = 14;
            this.label10.Text = "Descripción";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Segoe Print", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(141)))), ((int)(((byte)(84)))));
            this.label8.Location = new System.Drawing.Point(12, 72);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(61, 28);
            this.label8.TabIndex = 12;
            this.label8.Text = "Precio";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe Print", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(141)))), ((int)(((byte)(84)))));
            this.label2.Location = new System.Drawing.Point(12, 157);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(150, 28);
            this.label2.TabIndex = 2;
            this.label2.Text = "Código de Barras";
            // 
            // pictureBoxImagenProd
            // 
            this.pictureBoxImagenProd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(156)))), ((int)(((byte)(0)))));
            this.pictureBoxImagenProd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBoxImagenProd.Location = new System.Drawing.Point(838, 6);
            this.pictureBoxImagenProd.Name = "pictureBoxImagenProd";
            this.pictureBoxImagenProd.Size = new System.Drawing.Size(220, 253);
            this.pictureBoxImagenProd.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxImagenProd.TabIndex = 1;
            this.pictureBoxImagenProd.TabStop = false;
            // 
            // openFileDialogImagenACargar
            // 
            this.openFileDialogImagenACargar.FileName = "openFileDialog1";
            // 
            // errorProviderForm
            // 
            this.errorProviderForm.ContainerControl = this;
            // 
            // FrmCatalogoProductos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1115, 527);
            this.Controls.Add(this.panelCatalogoProductosForm);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmCatalogoProductos";
            this.Text = "FrmCatalogoProductos";
            this.Load += new System.EventHandler(this.FrmCatalogoProductos_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panelCatalogoProductosForm.ResumeLayout(false);
            this.panelCatalogoProductosForm.PerformLayout();
            this.flowLayoutPanelAccionesCrud.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgProductos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxCodBarras)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxImagenProd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderForm)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panelCatalogoProductosForm;
        private System.Windows.Forms.PictureBox pictureBoxImagenProd;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtDescripcion;
        private System.Windows.Forms.TextBox txtPrecio;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.TextBox txtCodigoBarras;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox comboUnidadDeMedida;
        private System.Windows.Forms.CheckBox checkPerecedero;
        private System.Windows.Forms.Button btnCargarImagen;
        private System.Windows.Forms.OpenFileDialog openFileDialogImagenACargar;
        private System.Windows.Forms.Label lblImagenProd;
        private System.Windows.Forms.Label lblImagenGuardada;
        private System.Windows.Forms.PictureBox picBoxCodBarras;
        private System.Windows.Forms.ErrorProvider errorProviderForm;
        private System.Windows.Forms.PictureBox pictureBoxLogo;
        private System.Windows.Forms.Button btnAgregar;
        private System.Windows.Forms.Button btnEditar;
        private System.Windows.Forms.Button btnEliminar;
        private System.Windows.Forms.Button btnCerrar;
        private System.Windows.Forms.Button btnLimpiar;
        private System.Windows.Forms.Label lblText;
        private FontAwesome.Sharp.IconButton iconBtnCasa;
        private FontAwesome.Sharp.IconButton iconBtnBack;
        private FontAwesome.Sharp.IconButton iconBtnCerrar;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridView dgProductos;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanelAccionesCrud;
        private System.Windows.Forms.ToolTip toolTipProductos;
    }
}