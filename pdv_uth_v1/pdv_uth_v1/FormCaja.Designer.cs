﻿namespace pdv_uth_v1
{
    partial class FormCaja
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormCaja));
            this.panel1 = new System.Windows.Forms.Panel();
            this.iconButton2 = new FontAwesome.Sharp.IconButton();
            this.iconBtnSalir = new FontAwesome.Sharp.IconButton();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.iconButton3 = new FontAwesome.Sharp.IconButton();
            this.iconButton1 = new FontAwesome.Sharp.IconButton();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.dgProductos = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel6 = new System.Windows.Forms.Panel();
            this.lblImagenDelProducto = new System.Windows.Forms.Label();
            this.txtCodBarras = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.pictureBoxCam = new System.Windows.Forms.PictureBox();
            this.cboCamera = new System.Windows.Forms.ComboBox();
            this.picImagenProductoNew = new System.Windows.Forms.PictureBox();
            this.iconButtonStart = new FontAwesome.Sharp.IconButton();
            this.numericCantidad = new System.Windows.Forms.NumericUpDown();
            this.label10 = new System.Windows.Forms.Label();
            this.iconBtnCancelarOperacion = new FontAwesome.Sharp.IconButton();
            this.iconBtnAdd = new FontAwesome.Sharp.IconButton();
            this.txtTotal = new System.Windows.Forms.TextBox();
            this.txtSubTotal = new System.Windows.Forms.TextBox();
            this.lblTotal = new System.Windows.Forms.Label();
            this.lblSubtotal = new System.Windows.Forms.Label();
            this.txtIva = new System.Windows.Forms.TextBox();
            this.lblIva = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.iconbtnTicket = new FontAwesome.Sharp.IconButton();
            this.txtCambio = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.iconBtnCreditos = new FontAwesome.Sharp.IconButton();
            this.iconBtnProductos = new FontAwesome.Sharp.IconButton();
            this.iconBtnInicio = new FontAwesome.Sharp.IconButton();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.iconBtnPagar = new FontAwesome.Sharp.IconButton();
            this.iconButton9 = new FontAwesome.Sharp.IconButton();
            this.iconBtnVentas = new FontAwesome.Sharp.IconButton();
            this.iconButton7 = new FontAwesome.Sharp.IconButton();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.iconButton6 = new FontAwesome.Sharp.IconButton();
            this.txtEfectivo = new System.Windows.Forms.TextBox();
            this.iconButton5 = new FontAwesome.Sharp.IconButton();
            this.panel7 = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label9 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgProductos)).BeginInit();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCam)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picImagenProductoNew)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericCantidad)).BeginInit();
            this.panel5.SuspendLayout();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(156)))), ((int)(((byte)(0)))));
            this.panel1.Controls.Add(this.iconButton2);
            this.panel1.Controls.Add(this.iconBtnSalir);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1370, 71);
            this.panel1.TabIndex = 0;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // iconButton2
            // 
            this.iconButton2.FlatAppearance.BorderSize = 0;
            this.iconButton2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.iconButton2.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.iconButton2.IconChar = FontAwesome.Sharp.IconChar.CashRegister;
            this.iconButton2.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.iconButton2.IconSize = 60;
            this.iconButton2.Location = new System.Drawing.Point(520, 5);
            this.iconButton2.Name = "iconButton2";
            this.iconButton2.Rotation = 0D;
            this.iconButton2.Size = new System.Drawing.Size(75, 62);
            this.iconButton2.TabIndex = 3;
            this.iconButton2.UseVisualStyleBackColor = true;
            // 
            // iconBtnSalir
            // 
            this.iconBtnSalir.FlatAppearance.BorderSize = 0;
            this.iconBtnSalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.iconBtnSalir.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.iconBtnSalir.IconChar = FontAwesome.Sharp.IconChar.TimesCircle;
            this.iconBtnSalir.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.iconBtnSalir.IconSize = 65;
            this.iconBtnSalir.Location = new System.Drawing.Point(1201, 3);
            this.iconBtnSalir.Name = "iconBtnSalir";
            this.iconBtnSalir.Rotation = 0D;
            this.iconBtnSalir.Size = new System.Drawing.Size(85, 67);
            this.iconBtnSalir.TabIndex = 1;
            this.iconBtnSalir.UseVisualStyleBackColor = true;
            this.iconBtnSalir.Click += new System.EventHandler(this.iconBtnSalir_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe Print", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(325, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(194, 62);
            this.label1.TabIndex = 0;
            this.label1.Text = "CAJA #1";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(208)))), ((int)(((byte)(135)))));
            this.panel2.Controls.Add(this.iconButton3);
            this.panel2.Controls.Add(this.iconButton1);
            this.panel2.Controls.Add(this.textBox1);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 71);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1370, 58);
            this.panel2.TabIndex = 1;
            // 
            // iconButton3
            // 
            this.iconButton3.FlatAppearance.BorderSize = 0;
            this.iconButton3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.iconButton3.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.iconButton3.IconChar = FontAwesome.Sharp.IconChar.Barcode;
            this.iconButton3.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(141)))), ((int)(((byte)(84)))));
            this.iconButton3.IconSize = 60;
            this.iconButton3.Location = new System.Drawing.Point(741, 2);
            this.iconButton3.Name = "iconButton3";
            this.iconButton3.Rotation = 0D;
            this.iconButton3.Size = new System.Drawing.Size(75, 58);
            this.iconButton3.TabIndex = 3;
            this.iconButton3.UseVisualStyleBackColor = true;
            // 
            // iconButton1
            // 
            this.iconButton1.FlatAppearance.BorderSize = 0;
            this.iconButton1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.iconButton1.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.iconButton1.IconChar = FontAwesome.Sharp.IconChar.Search;
            this.iconButton1.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(156)))), ((int)(((byte)(0)))));
            this.iconButton1.IconSize = 60;
            this.iconButton1.Location = new System.Drawing.Point(837, 3);
            this.iconButton1.Name = "iconButton1";
            this.iconButton1.Rotation = 0D;
            this.iconButton1.Size = new System.Drawing.Size(75, 58);
            this.iconButton1.TabIndex = 2;
            this.iconButton1.UseVisualStyleBackColor = true;
            // 
            // textBox1
            // 
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox1.Location = new System.Drawing.Point(336, 23);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(399, 20);
            this.textBox1.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe Print", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 10);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(314, 36);
            this.label2.TabIndex = 0;
            this.label2.Text = "Buscar por código de barras";
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(208)))), ((int)(((byte)(135)))));
            this.panel4.Controls.Add(this.dgProductos);
            this.panel4.Controls.Add(this.panel6);
            this.panel4.Controls.Add(this.panel5);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(0, 129);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(1370, 562);
            this.panel4.TabIndex = 3;
            // 
            // dgProductos
            // 
            this.dgProductos.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgProductos.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(189)))), ((int)(((byte)(84)))));
            this.dgProductos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgProductos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column3,
            this.Column7,
            this.Column4,
            this.Column8,
            this.Column5,
            this.Column6});
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(189)))), ((int)(((byte)(84)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe Print", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(141)))), ((int)(((byte)(84)))));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(207)))), ((int)(((byte)(167)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgProductos.DefaultCellStyle = dataGridViewCellStyle1;
            this.dgProductos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgProductos.Location = new System.Drawing.Point(433, 0);
            this.dgProductos.Name = "dgProductos";
            this.dgProductos.Size = new System.Drawing.Size(618, 562);
            this.dgProductos.TabIndex = 2;
            this.dgProductos.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgProductos_CellContentClick);
            this.dgProductos.Layout += new System.Windows.Forms.LayoutEventHandler(this.dgProductos_Layout);
            // 
            // Column1
            // 
            this.Column1.HeaderText = "ID";
            this.Column1.Name = "Column1";
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Código de barras";
            this.Column3.Name = "Column3";
            // 
            // Column7
            // 
            this.Column7.HeaderText = "Producto";
            this.Column7.Name = "Column7";
            // 
            // Column4
            // 
            this.Column4.HeaderText = "Descripción";
            this.Column4.Name = "Column4";
            // 
            // Column8
            // 
            this.Column8.HeaderText = "Precio";
            this.Column8.Name = "Column8";
            // 
            // Column5
            // 
            this.Column5.HeaderText = "Cantidad";
            this.Column5.Name = "Column5";
            // 
            // Column6
            // 
            this.Column6.HeaderText = "Subtotal";
            this.Column6.Name = "Column6";
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(156)))), ((int)(((byte)(0)))));
            this.panel6.Controls.Add(this.lblImagenDelProducto);
            this.panel6.Controls.Add(this.txtCodBarras);
            this.panel6.Controls.Add(this.label11);
            this.panel6.Controls.Add(this.pictureBoxCam);
            this.panel6.Controls.Add(this.cboCamera);
            this.panel6.Controls.Add(this.picImagenProductoNew);
            this.panel6.Controls.Add(this.iconButtonStart);
            this.panel6.Controls.Add(this.numericCantidad);
            this.panel6.Controls.Add(this.label10);
            this.panel6.Controls.Add(this.iconBtnCancelarOperacion);
            this.panel6.Controls.Add(this.iconBtnAdd);
            this.panel6.Controls.Add(this.txtTotal);
            this.panel6.Controls.Add(this.txtSubTotal);
            this.panel6.Controls.Add(this.lblTotal);
            this.panel6.Controls.Add(this.lblSubtotal);
            this.panel6.Controls.Add(this.txtIva);
            this.panel6.Controls.Add(this.lblIva);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel6.Location = new System.Drawing.Point(1051, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(319, 562);
            this.panel6.TabIndex = 1;
            // 
            // lblImagenDelProducto
            // 
            this.lblImagenDelProducto.AutoSize = true;
            this.lblImagenDelProducto.Font = new System.Drawing.Font("Segoe Print", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblImagenDelProducto.ForeColor = System.Drawing.Color.Navy;
            this.lblImagenDelProducto.Location = new System.Drawing.Point(4, 6);
            this.lblImagenDelProducto.Name = "lblImagenDelProducto";
            this.lblImagenDelProducto.Size = new System.Drawing.Size(292, 28);
            this.lblImagenDelProducto.TabIndex = 30;
            this.lblImagenDelProducto.Text = "Nombre de la imágen del producto";
            // 
            // txtCodBarras
            // 
            this.txtCodBarras.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(207)))), ((int)(((byte)(167)))));
            this.txtCodBarras.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtCodBarras.Font = new System.Drawing.Font("Segoe Print", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCodBarras.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(44)))), ((int)(((byte)(0)))));
            this.txtCodBarras.Location = new System.Drawing.Point(10, 331);
            this.txtCodBarras.Margin = new System.Windows.Forms.Padding(2);
            this.txtCodBarras.Name = "txtCodBarras";
            this.txtCodBarras.Size = new System.Drawing.Size(211, 27);
            this.txtCodBarras.TabIndex = 29;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Segoe Print", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(8)))), ((int)(((byte)(103)))));
            this.label11.Location = new System.Drawing.Point(2, 296);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(140, 26);
            this.label11.TabIndex = 28;
            this.label11.Text = "Código de barras";
            // 
            // pictureBoxCam
            // 
            this.pictureBoxCam.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBoxCam.Location = new System.Drawing.Point(1, 165);
            this.pictureBoxCam.Name = "pictureBoxCam";
            this.pictureBoxCam.Size = new System.Drawing.Size(316, 128);
            this.pictureBoxCam.TabIndex = 27;
            this.pictureBoxCam.TabStop = false;
            // 
            // cboCamera
            // 
            this.cboCamera.FormattingEnabled = true;
            this.cboCamera.Location = new System.Drawing.Point(193, 142);
            this.cboCamera.Name = "cboCamera";
            this.cboCamera.Size = new System.Drawing.Size(121, 21);
            this.cboCamera.TabIndex = 26;
            // 
            // picImagenProductoNew
            // 
            this.picImagenProductoNew.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.picImagenProductoNew.Image = global::pdv_uth_v1.Properties.Resources._20191002152136_bk;
            this.picImagenProductoNew.Location = new System.Drawing.Point(81, 46);
            this.picImagenProductoNew.Name = "picImagenProductoNew";
            this.picImagenProductoNew.Size = new System.Drawing.Size(154, 92);
            this.picImagenProductoNew.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picImagenProductoNew.TabIndex = 25;
            this.picImagenProductoNew.TabStop = false;
            // 
            // iconButtonStart
            // 
            this.iconButtonStart.FlatAppearance.BorderSize = 0;
            this.iconButtonStart.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.iconButtonStart.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.iconButtonStart.IconChar = FontAwesome.Sharp.IconChar.Video;
            this.iconButtonStart.IconColor = System.Drawing.Color.Navy;
            this.iconButtonStart.IconSize = 50;
            this.iconButtonStart.Location = new System.Drawing.Point(248, 313);
            this.iconButtonStart.Name = "iconButtonStart";
            this.iconButtonStart.Rotation = 0D;
            this.iconButtonStart.Size = new System.Drawing.Size(59, 45);
            this.iconButtonStart.TabIndex = 24;
            this.iconButtonStart.UseVisualStyleBackColor = true;
            this.iconButtonStart.Click += new System.EventHandler(this.iconButtonStart_Click);
            // 
            // numericCantidad
            // 
            this.numericCantidad.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(207)))), ((int)(((byte)(167)))));
            this.numericCantidad.DecimalPlaces = 3;
            this.numericCantidad.Font = new System.Drawing.Font("Segoe Print", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericCantidad.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(44)))), ((int)(((byte)(0)))));
            this.numericCantidad.Location = new System.Drawing.Point(140, 362);
            this.numericCantidad.Margin = new System.Windows.Forms.Padding(2);
            this.numericCantidad.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericCantidad.Name = "numericCantidad";
            this.numericCantidad.Size = new System.Drawing.Size(107, 34);
            this.numericCantidad.TabIndex = 23;
            this.numericCantidad.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericCantidad.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Segoe Print", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(8)))), ((int)(((byte)(103)))));
            this.label10.Location = new System.Drawing.Point(2, 373);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(81, 26);
            this.label10.TabIndex = 22;
            this.label10.Text = "Cantidad";
            // 
            // iconBtnCancelarOperacion
            // 
            this.iconBtnCancelarOperacion.FlatAppearance.BorderSize = 30;
            this.iconBtnCancelarOperacion.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.iconBtnCancelarOperacion.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.iconBtnCancelarOperacion.IconChar = FontAwesome.Sharp.IconChar.Times;
            this.iconBtnCancelarOperacion.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.iconBtnCancelarOperacion.IconSize = 50;
            this.iconBtnCancelarOperacion.Location = new System.Drawing.Point(190, 514);
            this.iconBtnCancelarOperacion.Name = "iconBtnCancelarOperacion";
            this.iconBtnCancelarOperacion.Rotation = 0D;
            this.iconBtnCancelarOperacion.Size = new System.Drawing.Size(84, 45);
            this.iconBtnCancelarOperacion.TabIndex = 20;
            this.iconBtnCancelarOperacion.UseVisualStyleBackColor = true;
            this.iconBtnCancelarOperacion.Click += new System.EventHandler(this.iconBtnCancelarOperacion_Click_1);
            // 
            // iconBtnAdd
            // 
            this.iconBtnAdd.FlatAppearance.BorderSize = 30;
            this.iconBtnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.iconBtnAdd.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.iconBtnAdd.IconChar = FontAwesome.Sharp.IconChar.Check;
            this.iconBtnAdd.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.iconBtnAdd.IconSize = 50;
            this.iconBtnAdd.Location = new System.Drawing.Point(3, 517);
            this.iconBtnAdd.Name = "iconBtnAdd";
            this.iconBtnAdd.Rotation = 0D;
            this.iconBtnAdd.Size = new System.Drawing.Size(94, 42);
            this.iconBtnAdd.TabIndex = 21;
            this.iconBtnAdd.UseVisualStyleBackColor = true;
            this.iconBtnAdd.Click += new System.EventHandler(this.iconBtnAdd_Click);
            // 
            // txtTotal
            // 
            this.txtTotal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(207)))), ((int)(((byte)(167)))));
            this.txtTotal.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtTotal.Font = new System.Drawing.Font("Segoe Print", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotal.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(44)))), ((int)(((byte)(0)))));
            this.txtTotal.Location = new System.Drawing.Point(142, 482);
            this.txtTotal.Margin = new System.Windows.Forms.Padding(2);
            this.txtTotal.Name = "txtTotal";
            this.txtTotal.Size = new System.Drawing.Size(147, 27);
            this.txtTotal.TabIndex = 20;
            this.txtTotal.Text = "1111111111111";
            // 
            // txtSubTotal
            // 
            this.txtSubTotal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(207)))), ((int)(((byte)(167)))));
            this.txtSubTotal.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtSubTotal.Font = new System.Drawing.Font("Segoe Print", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSubTotal.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(44)))), ((int)(((byte)(0)))));
            this.txtSubTotal.Location = new System.Drawing.Point(142, 408);
            this.txtSubTotal.Margin = new System.Windows.Forms.Padding(2);
            this.txtSubTotal.Name = "txtSubTotal";
            this.txtSubTotal.Size = new System.Drawing.Size(147, 27);
            this.txtSubTotal.TabIndex = 16;
            this.txtSubTotal.Text = "1111111111111";
            // 
            // lblTotal
            // 
            this.lblTotal.AutoSize = true;
            this.lblTotal.Font = new System.Drawing.Font("Segoe Print", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotal.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(8)))), ((int)(((byte)(103)))));
            this.lblTotal.Location = new System.Drawing.Point(2, 488);
            this.lblTotal.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(65, 26);
            this.lblTotal.TabIndex = 19;
            this.lblTotal.Text = "TOTAL";
            // 
            // lblSubtotal
            // 
            this.lblSubtotal.AutoSize = true;
            this.lblSubtotal.Font = new System.Drawing.Font("Segoe Print", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSubtotal.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(8)))), ((int)(((byte)(103)))));
            this.lblSubtotal.Location = new System.Drawing.Point(2, 410);
            this.lblSubtotal.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSubtotal.Name = "lblSubtotal";
            this.lblSubtotal.Size = new System.Drawing.Size(75, 26);
            this.lblSubtotal.TabIndex = 15;
            this.lblSubtotal.Text = "Subtotal";
            // 
            // txtIva
            // 
            this.txtIva.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(207)))), ((int)(((byte)(167)))));
            this.txtIva.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtIva.Font = new System.Drawing.Font("Segoe Print", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtIva.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(153)))), ((int)(((byte)(44)))), ((int)(((byte)(0)))));
            this.txtIva.Location = new System.Drawing.Point(142, 447);
            this.txtIva.Margin = new System.Windows.Forms.Padding(2);
            this.txtIva.Name = "txtIva";
            this.txtIva.Size = new System.Drawing.Size(147, 27);
            this.txtIva.TabIndex = 18;
            this.txtIva.Text = "1111111111111";
            // 
            // lblIva
            // 
            this.lblIva.AutoSize = true;
            this.lblIva.Font = new System.Drawing.Font("Segoe Print", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIva.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(8)))), ((int)(((byte)(103)))));
            this.lblIva.Location = new System.Drawing.Point(2, 451);
            this.lblIva.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblIva.Name = "lblIva";
            this.lblIva.Size = new System.Drawing.Size(40, 26);
            this.lblIva.TabIndex = 17;
            this.lblIva.Text = "IVA";
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(176)))), ((int)(((byte)(105)))));
            this.panel5.Controls.Add(this.iconbtnTicket);
            this.panel5.Controls.Add(this.txtCambio);
            this.panel5.Controls.Add(this.label12);
            this.panel5.Controls.Add(this.iconBtnCreditos);
            this.panel5.Controls.Add(this.iconBtnProductos);
            this.panel5.Controls.Add(this.iconBtnInicio);
            this.panel5.Controls.Add(this.textBox3);
            this.panel5.Controls.Add(this.iconBtnPagar);
            this.panel5.Controls.Add(this.iconButton9);
            this.panel5.Controls.Add(this.iconBtnVentas);
            this.panel5.Controls.Add(this.iconButton7);
            this.panel5.Controls.Add(this.textBox2);
            this.panel5.Controls.Add(this.iconButton6);
            this.panel5.Controls.Add(this.txtEfectivo);
            this.panel5.Controls.Add(this.iconButton5);
            this.panel5.Controls.Add(this.panel7);
            this.panel5.Controls.Add(this.pictureBox1);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(433, 562);
            this.panel5.TabIndex = 0;
            // 
            // iconbtnTicket
            // 
            this.iconbtnTicket.FlatAppearance.BorderSize = 30;
            this.iconbtnTicket.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.iconbtnTicket.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.iconbtnTicket.Font = new System.Drawing.Font("Segoe Print", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.iconbtnTicket.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.iconbtnTicket.IconChar = FontAwesome.Sharp.IconChar.Download;
            this.iconbtnTicket.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.iconbtnTicket.IconSize = 50;
            this.iconbtnTicket.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.iconbtnTicket.Location = new System.Drawing.Point(287, 494);
            this.iconbtnTicket.Name = "iconbtnTicket";
            this.iconbtnTicket.Rotation = 0D;
            this.iconbtnTicket.Size = new System.Drawing.Size(140, 59);
            this.iconbtnTicket.TabIndex = 21;
            this.iconbtnTicket.Text = "TICKET";
            this.iconbtnTicket.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            this.iconbtnTicket.UseVisualStyleBackColor = true;
            this.iconbtnTicket.Click += new System.EventHandler(this.iconbtnTicket_Click);
            // 
            // txtCambio
            // 
            this.txtCambio.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(156)))), ((int)(((byte)(0)))));
            this.txtCambio.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtCambio.Font = new System.Drawing.Font("Segoe Print", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCambio.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.txtCambio.Location = new System.Drawing.Point(7, 524);
            this.txtCambio.Margin = new System.Windows.Forms.Padding(2);
            this.txtCambio.Name = "txtCambio";
            this.txtCambio.Size = new System.Drawing.Size(157, 23);
            this.txtCambio.TabIndex = 20;
            this.txtCambio.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Segoe Print", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.label12.Location = new System.Drawing.Point(4, 488);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(162, 28);
            this.label12.TabIndex = 6;
            this.label12.Text = "Cambio del Cliente";
            this.label12.Click += new System.EventHandler(this.label12_Click);
            // 
            // iconBtnCreditos
            // 
            this.iconBtnCreditos.FlatAppearance.BorderSize = 30;
            this.iconBtnCreditos.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.iconBtnCreditos.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.iconBtnCreditos.Font = new System.Drawing.Font("Segoe Print", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.iconBtnCreditos.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.iconBtnCreditos.IconChar = FontAwesome.Sharp.IconChar.Wallet;
            this.iconBtnCreditos.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.iconBtnCreditos.IconSize = 72;
            this.iconBtnCreditos.Location = new System.Drawing.Point(130, 140);
            this.iconBtnCreditos.Name = "iconBtnCreditos";
            this.iconBtnCreditos.Rotation = 0D;
            this.iconBtnCreditos.Size = new System.Drawing.Size(103, 83);
            this.iconBtnCreditos.TabIndex = 19;
            this.iconBtnCreditos.Text = "CRÉDITOS";
            this.iconBtnCreditos.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.iconBtnCreditos.UseVisualStyleBackColor = true;
            this.iconBtnCreditos.Click += new System.EventHandler(this.iconBtnCreditos_Click);
            // 
            // iconBtnProductos
            // 
            this.iconBtnProductos.FlatAppearance.BorderSize = 30;
            this.iconBtnProductos.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.iconBtnProductos.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.iconBtnProductos.Font = new System.Drawing.Font("Segoe Print", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.iconBtnProductos.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.iconBtnProductos.IconChar = FontAwesome.Sharp.IconChar.ShoppingBasket;
            this.iconBtnProductos.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.iconBtnProductos.IconSize = 75;
            this.iconBtnProductos.Location = new System.Drawing.Point(0, 139);
            this.iconBtnProductos.Name = "iconBtnProductos";
            this.iconBtnProductos.Rotation = 0D;
            this.iconBtnProductos.Size = new System.Drawing.Size(108, 83);
            this.iconBtnProductos.TabIndex = 18;
            this.iconBtnProductos.Text = "PRODUCTOS";
            this.iconBtnProductos.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.iconBtnProductos.UseVisualStyleBackColor = true;
            this.iconBtnProductos.Click += new System.EventHandler(this.iconBtnProductos_Click);
            // 
            // iconBtnInicio
            // 
            this.iconBtnInicio.FlatAppearance.BorderSize = 30;
            this.iconBtnInicio.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.iconBtnInicio.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.iconBtnInicio.Font = new System.Drawing.Font("Segoe Print", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.iconBtnInicio.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.iconBtnInicio.IconChar = FontAwesome.Sharp.IconChar.Home;
            this.iconBtnInicio.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.iconBtnInicio.IconSize = 75;
            this.iconBtnInicio.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.iconBtnInicio.Location = new System.Drawing.Point(3, 6);
            this.iconBtnInicio.Name = "iconBtnInicio";
            this.iconBtnInicio.Rotation = 0D;
            this.iconBtnInicio.Size = new System.Drawing.Size(107, 83);
            this.iconBtnInicio.TabIndex = 17;
            this.iconBtnInicio.Text = "INICIO";
            this.iconBtnInicio.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.iconBtnInicio.UseVisualStyleBackColor = true;
            this.iconBtnInicio.Click += new System.EventHandler(this.iconBtnInicio_Click);
            // 
            // textBox3
            // 
            this.textBox3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(156)))), ((int)(((byte)(0)))));
            this.textBox3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox3.Font = new System.Drawing.Font("Segoe Print", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.textBox3.Location = new System.Drawing.Point(182, 452);
            this.textBox3.Margin = new System.Windows.Forms.Padding(2);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(81, 23);
            this.textBox3.TabIndex = 16;
            this.textBox3.Text = "0.00";
            this.textBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // iconBtnPagar
            // 
            this.iconBtnPagar.FlatAppearance.BorderSize = 30;
            this.iconBtnPagar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.iconBtnPagar.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.iconBtnPagar.Font = new System.Drawing.Font("Segoe Print", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.iconBtnPagar.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.iconBtnPagar.IconChar = FontAwesome.Sharp.IconChar.CheckSquare;
            this.iconBtnPagar.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.iconBtnPagar.IconSize = 50;
            this.iconBtnPagar.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.iconBtnPagar.Location = new System.Drawing.Point(290, 423);
            this.iconBtnPagar.Name = "iconBtnPagar";
            this.iconBtnPagar.Rotation = 0D;
            this.iconBtnPagar.Size = new System.Drawing.Size(140, 59);
            this.iconBtnPagar.TabIndex = 15;
            this.iconBtnPagar.Text = "PAGAR";
            this.iconBtnPagar.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            this.iconBtnPagar.UseVisualStyleBackColor = true;
            this.iconBtnPagar.Click += new System.EventHandler(this.iconBtnPagar_Click);
            // 
            // iconButton9
            // 
            this.iconButton9.FlatAppearance.BorderSize = 30;
            this.iconButton9.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.iconButton9.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.iconButton9.Font = new System.Drawing.Font("Segoe Print", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.iconButton9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.iconButton9.IconChar = FontAwesome.Sharp.IconChar.UserFriends;
            this.iconButton9.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.iconButton9.IconSize = 50;
            this.iconButton9.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.iconButton9.Location = new System.Drawing.Point(290, 343);
            this.iconButton9.Name = "iconButton9";
            this.iconButton9.Rotation = 0D;
            this.iconButton9.Size = new System.Drawing.Size(140, 58);
            this.iconButton9.TabIndex = 14;
            this.iconButton9.Text = "CLIENTE";
            this.iconButton9.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            this.iconButton9.UseVisualStyleBackColor = true;
            // 
            // iconBtnVentas
            // 
            this.iconBtnVentas.FlatAppearance.BorderSize = 30;
            this.iconBtnVentas.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.iconBtnVentas.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.iconBtnVentas.Font = new System.Drawing.Font("Segoe Print", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.iconBtnVentas.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.iconBtnVentas.IconChar = FontAwesome.Sharp.IconChar.ShoppingCart;
            this.iconBtnVentas.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.iconBtnVentas.IconSize = 75;
            this.iconBtnVentas.Location = new System.Drawing.Point(130, 6);
            this.iconBtnVentas.Name = "iconBtnVentas";
            this.iconBtnVentas.Rotation = 0D;
            this.iconBtnVentas.Size = new System.Drawing.Size(104, 83);
            this.iconBtnVentas.TabIndex = 13;
            this.iconBtnVentas.Text = "VENTAS";
            this.iconBtnVentas.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.iconBtnVentas.UseVisualStyleBackColor = true;
            this.iconBtnVentas.Click += new System.EventHandler(this.iconBtnVentas_Click);
            // 
            // iconButton7
            // 
            this.iconButton7.FlatAppearance.BorderSize = 30;
            this.iconButton7.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.iconButton7.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.iconButton7.IconChar = FontAwesome.Sharp.IconChar.UserAlt;
            this.iconButton7.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.iconButton7.IconSize = 85;
            this.iconButton7.Location = new System.Drawing.Point(184, 345);
            this.iconButton7.Name = "iconButton7";
            this.iconButton7.Rotation = 0D;
            this.iconButton7.Size = new System.Drawing.Size(84, 100);
            this.iconButton7.TabIndex = 12;
            this.iconButton7.UseVisualStyleBackColor = true;
            // 
            // textBox2
            // 
            this.textBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(156)))), ((int)(((byte)(0)))));
            this.textBox2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox2.Font = new System.Drawing.Font("Segoe Print", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.textBox2.Location = new System.Drawing.Point(97, 452);
            this.textBox2.Margin = new System.Windows.Forms.Padding(2);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(81, 23);
            this.textBox2.TabIndex = 11;
            this.textBox2.Text = "0.00";
            this.textBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // iconButton6
            // 
            this.iconButton6.FlatAppearance.BorderSize = 30;
            this.iconButton6.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.iconButton6.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.iconButton6.IconChar = FontAwesome.Sharp.IconChar.CreditCard;
            this.iconButton6.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.iconButton6.IconSize = 85;
            this.iconButton6.Location = new System.Drawing.Point(94, 343);
            this.iconButton6.Name = "iconButton6";
            this.iconButton6.Rotation = 0D;
            this.iconButton6.Size = new System.Drawing.Size(84, 102);
            this.iconButton6.TabIndex = 10;
            this.iconButton6.UseVisualStyleBackColor = true;
            // 
            // txtEfectivo
            // 
            this.txtEfectivo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(156)))), ((int)(((byte)(0)))));
            this.txtEfectivo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtEfectivo.Font = new System.Drawing.Font("Segoe Print", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEfectivo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.txtEfectivo.Location = new System.Drawing.Point(3, 452);
            this.txtEfectivo.Margin = new System.Windows.Forms.Padding(2);
            this.txtEfectivo.Name = "txtEfectivo";
            this.txtEfectivo.Size = new System.Drawing.Size(89, 23);
            this.txtEfectivo.TabIndex = 9;
            this.txtEfectivo.Text = "0.00";
            this.txtEfectivo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtEfectivo.MouseLeave += new System.EventHandler(this.txtEfectivo_MouseLeave);
            // 
            // iconButton5
            // 
            this.iconButton5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(176)))), ((int)(((byte)(105)))));
            this.iconButton5.FlatAppearance.BorderSize = 30;
            this.iconButton5.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.iconButton5.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.iconButton5.IconChar = FontAwesome.Sharp.IconChar.MoneyBill;
            this.iconButton5.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.iconButton5.IconSize = 85;
            this.iconButton5.Location = new System.Drawing.Point(3, 343);
            this.iconButton5.Name = "iconButton5";
            this.iconButton5.Rotation = 0D;
            this.iconButton5.Size = new System.Drawing.Size(81, 102);
            this.iconButton5.TabIndex = 8;
            this.iconButton5.UseVisualStyleBackColor = false;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.label8);
            this.panel7.Controls.Add(this.label7);
            this.panel7.Controls.Add(this.label6);
            this.panel7.Controls.Add(this.label5);
            this.panel7.Controls.Add(this.label4);
            this.panel7.Controls.Add(this.label3);
            this.panel7.Location = new System.Drawing.Point(0, 229);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(435, 108);
            this.panel7.TabIndex = 1;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Segoe Print", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.label8.Location = new System.Drawing.Point(166, 5);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(143, 28);
            this.label8.TabIndex = 5;
            this.label8.Text = "Israel Yepiz Villa";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Segoe Print", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.label7.Location = new System.Drawing.Point(166, 34);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(85, 28);
            this.label7.TabIndex = 4;
            this.label7.Text = "$350.00";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Segoe Print", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.label6.Location = new System.Drawing.Point(166, 67);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(114, 28);
            this.label6.TabIndex = 3;
            this.label6.Text = "1/01/2021";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe Print", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.label5.Location = new System.Drawing.Point(2, 34);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(99, 28);
            this.label5.TabIndex = 2;
            this.label5.Text = "Disponible:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe Print", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.label4.Location = new System.Drawing.Point(2, 67);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(127, 28);
            this.label4.TabIndex = 1;
            this.label4.Text = "Próximo pago:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe Print", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.label3.Location = new System.Drawing.Point(2, 5);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 28);
            this.label3.TabIndex = 0;
            this.label3.Text = "Cliente:";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::pdv_uth_v1.Properties.Resources.Logo4;
            this.pictureBox1.Location = new System.Drawing.Point(239, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(196, 228);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(141)))), ((int)(((byte)(84)))));
            this.panel3.Controls.Add(this.label9);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(0, 691);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1370, 58);
            this.panel3.TabIndex = 2;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Segoe Print", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Navy;
            this.label9.Location = new System.Drawing.Point(6, 3);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(106, 37);
            this.label9.TabIndex = 0;
            this.label9.Text = "FOOTER";
            // 
            // FormCaja
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1370, 749);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormCaja";
            this.Text = "FormCaja";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormCaja_FormClosing);
            this.Load += new System.EventHandler(this.FormCaja_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgProductos)).EndInit();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCam)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picImagenProductoNew)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericCantidad)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private FontAwesome.Sharp.IconButton iconBtnSalir;
        private System.Windows.Forms.Label label1;
        private FontAwesome.Sharp.IconButton iconButton2;
        private System.Windows.Forms.Panel panel2;
        private FontAwesome.Sharp.IconButton iconButton3;
        private FontAwesome.Sharp.IconButton iconButton1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.DataGridView dgProductos;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private FontAwesome.Sharp.IconButton iconBtnCreditos;
        private FontAwesome.Sharp.IconButton iconBtnProductos;
        private FontAwesome.Sharp.IconButton iconBtnInicio;
        private System.Windows.Forms.TextBox textBox3;
        private FontAwesome.Sharp.IconButton iconBtnPagar;
        private FontAwesome.Sharp.IconButton iconButton9;
        private FontAwesome.Sharp.IconButton iconBtnVentas;
        private FontAwesome.Sharp.IconButton iconButton7;
        private System.Windows.Forms.TextBox textBox2;
        private FontAwesome.Sharp.IconButton iconButton6;
        private System.Windows.Forms.TextBox txtEfectivo;
        private FontAwesome.Sharp.IconButton iconButton5;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label9;
        private FontAwesome.Sharp.IconButton iconBtnCancelarOperacion;
        private FontAwesome.Sharp.IconButton iconBtnAdd;
        private System.Windows.Forms.TextBox txtTotal;
        private System.Windows.Forms.TextBox txtSubTotal;
        private System.Windows.Forms.Label lblTotal;
        private System.Windows.Forms.Label lblSubtotal;
        private System.Windows.Forms.TextBox txtIva;
        private System.Windows.Forms.Label lblIva;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.PictureBox picImagenProductoNew;
        private FontAwesome.Sharp.IconButton iconButtonStart;
        private System.Windows.Forms.NumericUpDown numericCantidad;
        private System.Windows.Forms.TextBox txtCodBarras;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.PictureBox pictureBoxCam;
        private System.Windows.Forms.ComboBox cboCamera;
        private System.Windows.Forms.Label lblImagenDelProducto;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.TextBox txtCambio;
        private System.Windows.Forms.Label label12;
        private FontAwesome.Sharp.IconButton iconbtnTicket;
    }
}