﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib_pdv_uth_v1.clientes;
using Lib_pdv_uth_v1;
using System.Text.RegularExpressions;
using ValidacionesRegEx;


namespace pdv_uth_v1
{
    public partial class FrmCatalogoCliente : Form
    {
        //Objeto RegEx para validaciones
      
        //vars para acciones CRUD
        Cliente cli = new Cliente();
        int idCliente = 0;
        public static int idParaPersona;

        public FrmCatalogoCliente()
        {
            InitializeComponent();

        }

        private void FrmCatalogoCliente_Load(object sender, EventArgs e)
        {
            toolTipClientes.SetToolTip(btnGuardar, "Presiona este botón para registrar un nuevo cliente. ");
            toolTipClientes.SetToolTip(btnModificar, "Presiona este botón para modificar a este cliente. ");
            toolTipClientes.SetToolTip(btnBorrar, "Presiona este botón para borrar a este cliente. ");
            toolTipClientes.SetToolTip(btnLimpiar, "Presiona este botón para limpiar el formulario. ");
            toolTipClientes.SetToolTip(btnCargarImgDomicilio, "Presiona este botón para cargar una imagen como comprobante de domicilio. ");
            toolTipClientes.SetToolTip(btnComproINEGuardada, "Presiona este botón para cargar una imagen como comprobante de INE. ");
            toolTipClientes.SetToolTip(btnComproCURPGuardada, "Presiona este botón para cargar una imagen como comprobante de CURP. ");
            toolTipClientes.SetToolTip(btnCerrarTodo, "Presiona este botón para cerrar este programa");
            toolTipClientes.SetToolTip(iconBtnBackToHome, "Presiona este botón para regresar al menú. ");
            toolTipClientes.SetToolTip(iconBtnIngresarAUsuarios, "Presiona este botón para abrir el formulario de personas alternativas. ");

            //FrmPersonasAlternativas per = new FrmPersonasAlternativas();

            //per.Activate();
            lblUsuario.Text = FrmLogin.nombrePersona;
            FrmPersonasAlternativas p = new FrmPersonasAlternativas();
            p.Activate();


            //vamos a actualizar el datagrid clientes, con los registros de BD
            limpiarform();
            mostrarRegistrosEnDG();

        }

        public void habilitarLosForms()
        {
            //habilitar forms
            txtNombre.Enabled = txtApPat.Enabled = txtApMat.Enabled = dtpFechaNacimiento.Enabled = txtCorreo.Enabled =
            txtCelular.Enabled = txtTelefono.Enabled = txtCalle.Enabled = txtNumCalle.Enabled =
            txtCP.Enabled = txtColonia.Enabled = txtFraccionamiento.Enabled = txtLocalidad.Enabled = txtMunicipio.Enabled =
             txtCurp.Enabled = true;
           //Cambiar los colores del texto
            txtNombre.BackColor = txtApPat.BackColor = txtApMat.BackColor = txtCorreo.BackColor = txtCelular.BackColor =
            txtTelefono.BackColor = txtCalle.BackColor = txtNumCalle.BackColor = txtCP.BackColor = txtColonia.BackColor =
            txtFraccionamiento.BackColor = txtLocalidad.BackColor = txtMunicipio.BackColor = 
             txtCurp.BackColor = Color.White;
        }

        private void mostrarRegistrosEnDG()
        {
            dgCliente.DataSource = null;
            //borrar todos los ren del DG
            dgCliente.Rows.Clear();
            //se crea lista de criterios de busqueda
            List<CriteriosBusqueda> criterios = new List<CriteriosBusqueda>();
            //se crea objeto de crioterio para busqueda
            CriteriosBusqueda criterio = new CriteriosBusqueda();
            //se asignan los datos del criterio del WHERE
            criterio.campo = " 1 ";
            criterio.operadorIntermedio = OperadorDeConsulta.IGUAL;
            criterio.valor = "1";
            criterio.operadorFinal = OperadorDeConsulta.NINGUNO;
            //se incluye el criterio en la lista de criterios
            criterios.Add(criterio);
            //se ejecuta la consulta y se asigna el resultado al DtaGrid
            dgCliente.DataSource = cli.consultar(criterios);
            //se refresca el dataGrid para mostrar los datos
            dgCliente.Refresh();
        }
        public void limpiarform()
        {
            //borramos
            txtNombre.Text = "";
            txtApPat.Text = "";
            txtApMat.Text = "";
            txtCorreo.Text = "";
            txtCelular.Text = "";
            txtTelefono.Text = "";
            txtCalle.Text = "";
            txtColonia.Text = "";
            txtCP.Text = "";
            txtNumCalle.Text = "";
            txtColonia.Text = "";
            txtMunicipio.Text = "";
            txtLocalidad.Text = "";
            pictureBoxDomicilio.Image = null;
            pictureBoxComproINE.Image = null;
            pictureBoxComproCURP.Image = null;
            txtCurp.Text = "";
            txtFraccionamiento.Text = "";




        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
        
            Cliente paraAlta = new Cliente();
            paraAlta.Nombre = txtNombre.Text;
            paraAlta.ApellidoPaterno = txtApPat.Text;
            paraAlta.ApellidoMaterno = txtApMat.Text;
            paraAlta.Celular = txtCelular.Text;
            paraAlta.Correo = txtCorreo.Text;
            paraAlta.FechaDeNacimiento = dtpFechaNacimiento.Value;
            paraAlta.Domicilio.calle = txtCalle.Text;
            paraAlta.Domicilio.numero = txtNumCalle.Text;
            paraAlta.Domicilio.colonia = txtColonia.Text;
            paraAlta.Domicilio.codigoPostal = txtCP.Text;
            paraAlta.Domicilio.localidad = txtLocalidad.Text;
            paraAlta.Domicilio.municipio = txtMunicipio.Text;
            paraAlta.Domicilio.fotoComprobante = lblImagendelComproGuardada.Text;
            paraAlta.Domicilio.seccionFraccionamiento = txtFraccionamiento.Text;
            paraAlta.Curp = txtCurp.Text;
            paraAlta.ComprobanteINE = lblImagenComproINEGuardada.Text;
            paraAlta.CurpCompro = lblImagendeComproCURPGuardada.Text;
            paraAlta.Telefono = txtTelefono.Text;

            if (paraAlta.alta())
            {

                MessageBox.Show("Registro Exitoso");
                mostrarRegistrosEnDG();
                limpiarform();
            }
            else
            {
                MessageBox.Show("Error al guardar cliente. " + Cliente.msgError);
            }
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
     
            //Datos Esta tomando el ultimo dato y lo sustituye en el sig add.
       
            Cliente paraModif = new Cliente();
            List<DatosParaActualizar> datos = new List<DatosParaActualizar>();

            datos.Add(new DatosParaActualizar("nombre", txtNombre.Text));
            datos.Add(new DatosParaActualizar("apellido_paterno", txtApPat.Text));
            datos.Add(new DatosParaActualizar("apellido_materno", txtApMat.Text));
            datos.Add(new DatosParaActualizar("fecha_de_nacimiento", dtpFechaNacimiento.Value.Year + "-" + dtpFechaNacimiento.Value.Month + "-" + dtpFechaNacimiento.Value.Day));
            datos.Add(new DatosParaActualizar("celular", txtCelular.Text));
            datos.Add(new DatosParaActualizar("telefono", txtTelefono.Text));
            datos.Add(new DatosParaActualizar("correo", txtCorreo.Text));
            datos.Add(new DatosParaActualizar("calle", txtCalle.Text));
            datos.Add(new DatosParaActualizar("numero_casa", txtNumCalle.Text));
            datos.Add(new DatosParaActualizar("codigo_postal", txtCP.Text));
            datos.Add(new DatosParaActualizar("colonia", txtColonia.Text));
            datos.Add(new DatosParaActualizar("fraccionamiento", txtFraccionamiento.Text));
            datos.Add(new DatosParaActualizar("localidad", txtLocalidad.Text));
            datos.Add(new DatosParaActualizar("municipio", txtMunicipio.Text));
            datos.Add(new DatosParaActualizar("img_comprobante_domicilio", lblImagendelComproGuardada.Text));
            datos.Add(new DatosParaActualizar("ine_comprobante", lblImagenComproINEGuardada.Text));
            datos.Add(new DatosParaActualizar("curp", txtCurp.Text));
            datos.Add(new DatosParaActualizar("curp_comprobante", lblImagendeComproCURPGuardada.Text));

            if (paraModif.modificar(datos, idCliente))
            {
                MessageBox.Show("cliente modificado");
            }
            else MessageBox.Show("Error cliente no modificado");

            mostrarRegistrosEnDG();
        }

        private void btnBorrar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Realmente quieres eliminar este cliente?", "Borrar cliente", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                if (cli.eliminar(idCliente))
                {
                    MessageBox.Show("Cliente eliminado ");
                    dgCliente.Refresh();
                }
                else MessageBox.Show("Error Cliente NO eliminado ");
            }
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            limpiarform();
        }



        private void txtNombre_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtApPat_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtApMat_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtCelular_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtCorreo_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtCalle_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtNumCalle_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtNombre_Leave(object sender, EventArgs e)
        {

            if (Validar.nombrePersonal(txtNombre.Text) == false)
            {

                errorProviderForm.SetError(txtNombre, "El nombre debe tener al menos 20 caracteres y no carateres especiales o numeros");

            }
            else
            {
                errorProviderForm.SetError(txtNombre, "");
            }
        }

        private void txtApPat_Leave(object sender, EventArgs e)
        {
            if (Validar.nombrePersonal(txtApPat.Text) == false)
            {
                errorProviderForm.SetError(txtApPat, "El apellido Paterno esta mal escrito!");
            }
            else
            {
                errorProviderForm.SetError(txtApPat, "");
            }
        }

        private void txtApMat_Leave(object sender, EventArgs e)
        {
            if (Validar.nombrePersonal(txtApMat.Text) == false)
            {
                errorProviderForm.SetError(txtApMat, "El apellido materno esta mal escrito!");
            }
            else
            {
                errorProviderForm.SetError(txtApMat, "");
            }
        }

        private void txtCelular_Leave(object sender, EventArgs e)
        {
            if (Validar.telefono(txtCelular.Text) == false)
            {
                errorProviderForm.SetError(txtCelular, "El celular no tiene formato correcto (62222222)!");
            }
            else
            {
                errorProviderForm.SetError(txtCelular, "");
            }
        }

        private void txtCorreo_Leave(object sender, EventArgs e)
        {
            if (Validar.correoElectronico(txtCorreo.Text) == false)
            {
                errorProviderForm.SetError(txtCorreo, "El correo no tiene el formato correcto, intente de nuevo");

            }
            else
            {
                errorProviderForm.SetError(txtCorreo, "");
            }
        }

        private void txtCP_Leave(object sender, EventArgs e)
        {
            if (Validar.codigoPostal(txtCP.Text) == false)
            {
                errorProviderForm.SetError(txtCP, "El codigo postal no contiene el formato correcto");

            }
            else
            {
                errorProviderForm.SetError(txtCP, "");
            }
        }

        private void txtTelefono_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtTelefono_Leave(object sender, EventArgs e)
        {
            if (Validar.telefono(txtTelefono.Text) == false)
            {
                errorProviderForm.SetError(txtTelefono, "El celular no tiene formato correcto (62222222)!");
            }
            else
            {
                errorProviderForm.SetError(txtTelefono, "");
            }

        }

        private void btnCerrarTodo_Click(object sender, EventArgs e)
        {
            this.Hide();

            MenuPrincipal pri = new MenuPrincipal();

            pri.ShowDialog();
        }

        private void dgCliente_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            
            idCliente = int.Parse(dgCliente.Rows[e.RowIndex].Cells[0].Value.ToString());
            txtNombre.Text = dgCliente.Rows[e.RowIndex].Cells[1].Value.ToString();
            txtApPat.Text = dgCliente.Rows[e.RowIndex].Cells[2].Value.ToString();
            txtApMat.Text = dgCliente.Rows[e.RowIndex].Cells[3].Value.ToString();
            txtCelular.Text = dgCliente.Rows[e.RowIndex].Cells[5].Value.ToString();
            txtCorreo.Text = dgCliente.Rows[e.RowIndex].Cells[7].Value.ToString();
            txtCalle.Text = dgCliente.Rows[e.RowIndex].Cells[8].Value.ToString();
            txtNumCalle.Text = dgCliente.Rows[e.RowIndex].Cells[9].Value.ToString();
            txtColonia.Text = dgCliente.Rows[e.RowIndex].Cells[11].Value.ToString();
            txtCP.Text = dgCliente.Rows[e.RowIndex].Cells[10].Value.ToString();
            txtLocalidad.Text = dgCliente.Rows[e.RowIndex].Cells[13].Value.ToString();
            txtMunicipio.Text = dgCliente.Rows[e.RowIndex].Cells[14].Value.ToString();
            lblImagendelComproGuardada.Text = dgCliente.Rows[e.RowIndex].Cells[15].Value.ToString();
            lblImagenComproINEGuardada.Text = dgCliente.Rows[e.RowIndex].Cells[16].Value.ToString();
            txtCurp.Text = dgCliente.Rows[e.RowIndex].Cells[17].Value.ToString();
            dtpFechaNacimiento.Value = DateTime.Parse(dgCliente.Rows[e.RowIndex].Cells[4].Value.ToString());
            txtTelefono.Text = dgCliente.Rows[e.RowIndex].Cells[18].Value.ToString();
            txtFraccionamiento.Text = dgCliente.Rows[e.RowIndex].Cells[17].Value.ToString();
            idParaPersona = idCliente;
            //idPersona = idCliente;




        }

        private void btnCargarImgDomicilio_Click(object sender, EventArgs e)
        {
            //Filtros para abrir imagen
            openFileDialogImagenACargar.Filter = "Image Files|*.png;";
            openFileDialogImagenACargar.Filter = "Gif Image (.gif)|*.gif|JPEG Image (.jpeg)|*.jpeg|Png Image (.png)|*.png|JPG Image (.jpg)|*.jpg";
            openFileDialogImagenACargar.Title = "Abrir imagen para el comprobante de domicilio";
            openFileDialogImagenACargar.DefaultExt = ".png";

            if (openFileDialogImagenACargar.ShowDialog() == DialogResult.OK)
            {
                //Tomamos el nombre del archivo
                string fileName = openFileDialogImagenACargar.FileName;


                //Cargamos la imagen al PictureBox
                pictureBoxDomicilio.Image = Image.FromFile(openFileDialogImagenACargar.FileName);

                //Tomamos el nombre original de la imagen
                lblImagenComproDom.Text = openFileDialogImagenACargar.SafeFileName;

                //Se calcula la extencion de la imagen
                string extension = lblImagenComproDom.Text.Substring(lblImagenComproDom.Text.Length - 3);

                //si es '.jpeg' va a decir 'peg', asi que acompletamos.
                extension = extension.ToLower() == "peg" ? "jpeg" : extension.ToLower();

                //Creamo el nombre unico para la imagen con DateTime.
                lblImagendelComproGuardada.Text = string.Format(@"DOM_{0}." + extension, DateTime.Now.Ticks);
            }
        }

        private void btnComproINEGuardada_Click(object sender, EventArgs e)
        {
            //Filtros para abrir imagen
            openFileDialogImagenACargar.Filter = "Image Files|*.png;";
            openFileDialogImagenACargar.Filter = "Gif Image (.gif)|*.gif|JPEG Image (.jpeg)|*.jpeg|Png Image (.png)|*.png|JPG Image (.jpg)|*.jpg";
            openFileDialogImagenACargar.Title = "Abrir imagen para el comprobante de INE";
            openFileDialogImagenACargar.DefaultExt = ".png";

            if (openFileDialogImagenACargar.ShowDialog() == DialogResult.OK)
            {
                //Tomamos el nombre del archivo
                string fileName = openFileDialogImagenACargar.FileName;


                //Cargamos la imagen al PictureBox
                pictureBoxComproINE.Image = Image.FromFile(openFileDialogImagenACargar.FileName);

                //Tomamos el nombre original de la imagen
                lblImagenComproINE.Text = openFileDialogImagenACargar.SafeFileName;

                //Se calcula la extencion de la imagen
                string extension = lblImagenComproINE.Text.Substring(lblImagenComproINE.Text.Length - 3);

                //si es '.jpeg' va a decir 'peg', asi que acompletamos.
                extension = extension.ToLower() == "peg" ? "jpeg" : extension.ToLower();

                //Creamo el nombre unico para la imagen con DateTime.
                lblImagenComproINEGuardada.Text = string.Format(@"INE_{0}." + extension, DateTime.Now.Ticks);
            }
        }

        private void btnComproCURPGuardada_Click(object sender, EventArgs e)
        {
            //Filtros para abrir imagen
            openFileDialogImagenACargar.Filter = "Image Files|*.png;";
            openFileDialogImagenACargar.Filter = "Gif Image (.gif)|*.gif|JPEG Image (.jpeg)|*.jpeg|Png Image (.png)|*.png|JPG Image (.jpg)|*.jpg";
            openFileDialogImagenACargar.Title = "Abrir imagen para el comprobante de CURP";
            openFileDialogImagenACargar.DefaultExt = ".png";

            if (openFileDialogImagenACargar.ShowDialog() == DialogResult.OK)
            {
                //Tomamos el nombre del archivo
                string fileName = openFileDialogImagenACargar.FileName;


                //Cargamos la imagen al PictureBox
                pictureBoxComproCURP.Image = Image.FromFile(openFileDialogImagenACargar.FileName);

                //Tomamos el nombre original de la imagen
                lblImagenComprobanteCURP.Text = openFileDialogImagenACargar.SafeFileName;

                //Se calcula la extencion de la imagen
                string extension = lblImagenComprobanteCURP.Text.Substring(lblImagenComprobanteCURP.Text.Length - 3);

                //si es '.jpeg' va a decir 'peg', asi que acompletamos.
                extension = extension.ToLower() == "peg" ? "jpeg" : extension.ToLower();

                //Creamo el nombre unico para la imagen con DateTime.
                lblImagendeComproCURPGuardada.Text = string.Format(@"CURP_{0}." + extension, DateTime.Now.Ticks);
            }
        }

        private void dgCliente_RowDefaultCellStyleChanged(object sender, DataGridViewRowEventArgs e)
        {
            this.dgCliente.DefaultCellStyle.Font = new Font("Segoe Print", 15);
        }

        private void iconBtnIngresarAUsuarios_Click(object sender, EventArgs e)
        {
          
            FrmPersonasAlternativas us = new FrmPersonasAlternativas();

            us.ShowDialog();
        }

        private void iconBtnBackToHome_Click(object sender, EventArgs e)
        {
            this.Hide();

            MenuPrincipal men = new MenuPrincipal();

            men.ShowDialog();
        }

        private void iconBtnParaID_Click(object sender, EventArgs e)
        {
            this.Hide();

            FrmCreditos cred = new FrmCreditos();

            cred.ShowDialog();
        }
    }
}
