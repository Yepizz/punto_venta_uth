﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace pdv_uth_v1
{
    public partial class MenuPrincipal : Form
    {
        public MenuPrincipal()
        {
            InitializeComponent();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Hide();

            FrmLogin log = new FrmLogin();

            log.ShowDialog();
       
        }

        private void btnCreditos_Click(object sender, EventArgs e)
        {
            //Ocultamos el Menu Principal
            this.Hide();
            //Hacemos una instancia de FrmCatalogoCliente
            FrmCreditos frmCreditos = new FrmCreditos();
            frmCreditos.ShowDialog();
        }

        private void btnClientes_Click(object sender, EventArgs e)
        {
            //Ocultamos el Menu Principal
            this.Hide();
            //Hacemos una instancia de FrmCatalogoCliente
            FrmCatalogoCliente frmCatalogoCliente = new FrmCatalogoCliente();
            frmCatalogoCliente.ShowDialog();
        }

        private void btnVentas_Click(object sender, EventArgs e)
        {
            //Ocultamos el menu principal
            this.Hide();
            //Hacemos una instancia del FrmVentas
            FrmVentas frmVentas = new FrmVentas();
            frmVentas.ShowDialog();
        }

        private void btnProductos_Click(object sender, EventArgs e)
        {
            //Ocultamos el menu principal
            this.Hide();
            //Hacemos una instancia de FrmCatalogoProductos
            FrmCatalogoProductos frmCatalogoProductos = new FrmCatalogoProductos();
            frmCatalogoProductos.ShowDialog();
        }

        private void btnUsuarios_Click(object sender, EventArgs e)
        {
            //Ocultamos el menu principal
            this.Hide();
            //Hacemos una instancia de FrmUsuarios
            FrmUsuarios frmUsuarios = new FrmUsuarios();
            frmUsuarios.ShowDialog();
        }

        private void tnLOGs_Click(object sender, EventArgs e)
        {
            //Ocultamos el Menu Principal
            this.Hide();
            //Hacemos una instancia de FrmLOGs
            FrmLog frmLog = new FrmLog();
            frmLog.ShowDialog();
        }

        private void btnCreditos_MouseLeave(object sender, EventArgs e)
        {
            
        }

        private void btnCreditos_MouseHover(object sender, EventArgs e)
        {
           
        }

        private void iconButton2_Click(object sender, EventArgs e)
        {
            this.Hide();

            FormCaja formcajaaa = new FormCaja();
            formcajaaa.ShowDialog();
        }

        private void MenuPrincipal_Load(object sender, EventArgs e)
        {
            toolTipHome.SetToolTip(btnCreditos, "Presiona este botón para abrir la ventana de créditos");
            toolTipHome.SetToolTip(btnVentas, "Presiona este botón para abrir la ventana de ventas. ");
            toolTipHome.SetToolTip(btnClientes, "Presiona este boton para abrir las ventanas de formularios de clientes");
            toolTipHome.SetToolTip(btnProductos, "Presiona este boton para abrir la ventana de formulario de registro de productos");
            toolTipHome.SetToolTip(btnUsuarios, "Presiona este boton para abrir la ventana de formulario de Usuarios");
            toolTipHome.SetToolTip(tnLOGs, "Presiona este boton para abrir los movimientos de usuario");
            toolTipHome.SetToolTip(iconBtnAbrirCaja, "Presiona este boton para abrir caja");
            toolTipHome.SetToolTip(btnBack, "Presiona este boton para regresar al inicio de sesión");
        }
    }
}
