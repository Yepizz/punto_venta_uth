﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib_pdv_uth_v1.usuarios;
using Lib_pdv_uth_v1.clientes;
using Lib_pdv_uth_v1;
using ValidacionesRegEx;


namespace pdv_uth_v1
{
    public partial class FrmUsuarios : Form
    {
        int idUsuario = 0;
        Usuario borrar = new Usuario();
        Usuario usu = new Usuario();

        Usuario per = new Usuario();
        public FrmUsuarios()
        {
            InitializeComponent();
        }

      

        private void FrmUsuarios_Load(object sender, EventArgs e)
        {
            toolTipUsers.SetToolTip(btnGuardarUsuario, "Presiona este botón para guardar a este usuario");
            toolTipUsers.SetToolTip(btnEditar, "Presiona este botón para editar a este usuario");
            toolTipUsers.SetToolTip(btnEliminar, "Presiona este botón para eliminar a este usuario");
            toolTipUsers.SetToolTip(btnLimpiar, "Presiona este botón para limpiar el formulario de usuarios");
            toolTipUsers.SetToolTip(btnCargarImgDomicilio, "Presiona este botón para cargar una imagen como comprobante oficial de domicilio");
            toolTipUsers.SetToolTip(btnComproINEGuardada, "Presiona este boton para cargar una imagen como comprobante oficial de INE");
            toolTipUsers.SetToolTip(btnActaNacimientoGuardar, "Presiona este botón para cargar una imagen como comprobante oficial de acta de nacimiento. ");
            toolTipUsers.SetToolTip(btnCertificadoEstudiosCargar, "Presiona este botón para cargar una imagen como comprobante oficial de certificado de estudios. ");
            toolTipUsers.SetToolTip(btnComproCURPGuardada, "Presiona este boton para cargar una imagen como comprobante oficial de comprobante de CURP. ");
            toolTipUsers.SetToolTip(iconBtnIngresarAUCliente, "Presiona este botón para devolverte al formulario de personas alternativas. ");
            toolTipUsers.SetToolTip(pictureBoxNext, "Presiona este botón para ingresar al formulario de productos");
            toolTipUsers.SetToolTip(iconBtnHome, "Presiona este botón para abrir el menú principal. ");
            mostrarRegistrosEnDG();
            comboBoxTipoUsuario.SelectedItem = comboBoxTipoUsuario.Items[0];
        }
        private void mostrarRegistrosEnDG()
        {
            dgUsuarios.DataSource = null;
            //borrar todos los ren del DG
            dgUsuarios.Rows.Clear();
            //se crea lista de criterios de busqueda
            List<CriteriosBusqueda> criterios = new List<CriteriosBusqueda>();
            //se crea objeto de crioterio para busqueda
            CriteriosBusqueda criterio = new CriteriosBusqueda();
            //se asignan los datos del criterio del WHERE
            criterio.campo = " 1 ";
            criterio.operadorIntermedio = OperadorDeConsulta.IGUAL;
            criterio.valor = "1";
            criterio.operadorFinal = OperadorDeConsulta.NINGUNO;
            //se incluye el criterio en la lista de criterios
            criterios.Add(criterio);
            //se ejecuta la consulta y se asigna el resultado al DtaGrid
            dgUsuarios.DataSource = per.consultar(criterios);
            //se refresca el dataGrid para mostrar los datos
            dgUsuarios.Refresh();
        }



        private void btnGuardarUsuario_Click(object sender, EventArgs e)
        {
            //Hacemos una instancia de Usuario
            Usuario guardar = new Usuario();
            guardar.Nombre = txtNombre.Text;
            guardar.ApellidoPaterno = txtApellidoPaterno.Text;
            guardar.ApellidoMaterno = txtApellidoMaterno.Text;
            guardar.FechaNacimiento = dtpFechaDeNacimiento.Value;
            guardar.Celular = txtCelular.Text;
            guardar.Telefono = txtTelefono.Text;
            guardar.Correo = txtCorreo.Text;
            guardar.Calle = txtCalle.Text;
            guardar.NumeroCasa = txtNumCasa.Text;
            guardar.CodigoPostal = txtCP.Text;
            guardar.Colonia = txtColonia.Text;
            guardar.Fraccionamiento = txtFraccionamiento.Text;
            guardar.Localidad = txtLocalidad.Text;
            guardar.Municipio = txtMunicipio.Text;
            guardar.ComproDom = lblImagendelComproGuardada.Text;
            guardar.ComproINE = lblImagenComproINEGuardada.Text;
            guardar.Curp = txtCURP.Text;
            guardar.ComproCurp = lblImagendeComproCURPGuardada.Text;
            guardar.ActaNacimiento = lblImagenActaNacGuardada.Text;
            guardar.CertificadoEstudios = lblCertificadoEstudiosGuardada.Text;
            guardar.TipoUsuario = (TipoUsuario)Enum.Parse(typeof(TipoUsuario), comboBoxTipoUsuario.SelectedItem.ToString());
            guardar.NumeroSeguroSocial = txtNumSeguroSocial.Text;
            guardar.Contraseña = txtContraseña.Text;


            if (guardar.alta())
            {

                MessageBox.Show("El Usuario <" + txtNombre.Text + "> Se ha registrado exitosamente. ", "Nuevo Usuario registrado", MessageBoxButtons.OK, MessageBoxIcon.Information); ;
                mostrarRegistrosEnDG();
                limpiarFormulario();
            }
            else
            {
                MessageBox.Show("Error al guardar al Usuario. " + Usuario.msgError);
            }

        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            Usuario modif = new Usuario();
            List<ModificarInformacionUsuarios> datos = new List<ModificarInformacionUsuarios>();

            datos.Add(new ModificarInformacionUsuarios("nombre", txtNombre.Text));
            datos.Add(new ModificarInformacionUsuarios("apellido_paterno", txtApellidoPaterno.Text));
            datos.Add(new ModificarInformacionUsuarios("apellido_materno", txtApellidoMaterno.Text));
            datos.Add(new ModificarInformacionUsuarios("fecha_de_nacimiento", dtpFechaDeNacimiento.Value.Year + "-" + dtpFechaDeNacimiento.Value.Month + "-" + dtpFechaDeNacimiento.Value.Day));
            datos.Add(new ModificarInformacionUsuarios("celular", txtCelular.Text));
            datos.Add(new ModificarInformacionUsuarios("telefono", txtTelefono.Text));
            datos.Add(new ModificarInformacionUsuarios("correo", txtCorreo.Text));
            datos.Add(new ModificarInformacionUsuarios("calle", txtCalle.Text));
            datos.Add(new ModificarInformacionUsuarios("numero_casa", txtNumCasa.Text));
            datos.Add(new ModificarInformacionUsuarios("codigo_postal", txtCP.Text));
            datos.Add(new ModificarInformacionUsuarios("colonia", txtColonia.Text));
            datos.Add(new ModificarInformacionUsuarios("fraccionamiento", txtFraccionamiento.Text));
            datos.Add(new ModificarInformacionUsuarios("localidad", txtLocalidad.Text));
            datos.Add(new ModificarInformacionUsuarios("municipio", txtMunicipio.Text));
            datos.Add(new ModificarInformacionUsuarios("img_comprobante_domicilio", lblImagendelComproGuardada.Text));
            datos.Add(new ModificarInformacionUsuarios("ine_comprobante", lblImagenComproINEGuardada.Text));
            datos.Add(new ModificarInformacionUsuarios("curp", txtCURP.Text));
            datos.Add(new ModificarInformacionUsuarios("curp_comprobante", lblImagendeComproCURPGuardada.Text));
            datos.Add(new ModificarInformacionUsuarios("acta_nacimiento", lblImagenActaNacGuardada.Text));
            datos.Add(new ModificarInformacionUsuarios("certificado_estudios", lblCertificadoEstudiosGuardada.Text));
            datos.Add(new ModificarInformacionUsuarios("numero_seguro_social", txtNumSeguroSocial.Text));
            datos.Add(new ModificarInformacionUsuarios("tipo_usuario", comboBoxTipoUsuario.Text));
            datos.Add(new ModificarInformacionUsuarios("pwd", txtContraseña.Text));


            if (modif.modificar(datos, idUsuario))
            {
                MessageBox.Show("Bien, se modificaron los datos del usuario exitosamente");
            }
            else MessageBox.Show("Error usuario no modificado");

            mostrarRegistrosEnDG();
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Realmente quieres eliminar este Usuario?", "Borrar Usuario", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                if (borrar.borrar(idUsuario))
                {
                    MessageBox.Show("Usuario Eliminado ");
                    dgUsuarios.Refresh(); // Make sure this comes first 
                    dgUsuarios.Parent.Refresh(); // Make sure this comes second 
                }
                else MessageBox.Show("Error usuario no eliminado. Intente de Nuevo ");
            }
        }
        public void limpiarFormulario()
        {
            //borramos
            txtNombre.Text = "";
            txtApellidoPaterno.Text = "";
            txtApellidoMaterno.Text = "";
            txtCelular.Text = "";
            txtTelefono.Text = "";
            txtCorreo.Text = "";
            txtCalle.Text = "";
            txtNumCasa.Text = "";
            txtCP.Text = "";
            txtColonia.Text = "";
            txtFraccionamiento.Text = "";
            txtLocalidad.Text = "";
            txtMunicipio.Text = "";
            pictureBoxDomicilio.Image = null;
            pictureBoxComproINE.Image = null;
            txtCURP.Text = "";
            pictureBoxComproCURP.Image = null;
            pictureBoxActaNacimiento.Image = null;
            pictureBoxCertificadoEstudios.Image = null;
            txtNumSeguroSocial.Text = "";
            txtContraseña.Text = "";
            txtContraseña.Text = "";

        }
        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            limpiarFormulario();

        }

        private void txtNombre_Leave(object sender, EventArgs e)
        {
            if (Validar.nombrePersonal(txtNombre.Text) == false)
            {

                errorProviderForm.SetError(txtNombre, "El nombre debe tener al menos 20 caracteres y no carateres especiales o numeros");

            }
            else
            {
                errorProviderForm.SetError(txtNombre, "");
            }
        }

        private void txtApellidoPaterno_Leave(object sender, EventArgs e)
        {
            if (Validar.nombrePersonal(txtApellidoPaterno.Text) == false)
            {
                errorProviderForm.SetError(txtApellidoPaterno, "El apellido Paterno esta mal escrito!");
            }
            else
            {
                errorProviderForm.SetError(txtApellidoPaterno, "");
            }
        }

        private void txtApellidoMaterno_Leave(object sender, EventArgs e)
        {
            if (Validar.nombrePersonal(txtApellidoMaterno.Text) == false)
            {
                errorProviderForm.SetError(txtApellidoMaterno, "El apellido materno esta mal escrito!");
            }
            else
            {
                errorProviderForm.SetError(txtApellidoMaterno, "");
            }
        }

        private void txtCelular_Leave(object sender, EventArgs e)
        {
            if (Validar.telefono(txtCelular.Text) == false)
            {
                errorProviderForm.SetError(txtCelular, "El celular no tiene formato correcto (62222222)!");
            }
            else
            {
                errorProviderForm.SetError(txtCelular, "");
            }
        }

        private void txtTelefono_Leave(object sender, EventArgs e)
        {
            if (Validar.telefono(txtTelefono.Text) == false)
            {
                errorProviderForm.SetError(txtTelefono, "El celular no tiene formato correcto (62222222)!");
            }
            else
            {
                errorProviderForm.SetError(txtTelefono, "");
            }
        }

        private void txtCorreo_Leave(object sender, EventArgs e)
        {
            if (Validar.correoElectronico(txtCorreo.Text) == false)
            {
                errorProviderForm.SetError(txtCorreo, "El correo no tiene el formato correcto, intente de nuevo");

            }
            else
            {
                errorProviderForm.SetError(txtCorreo, "");
            }
        }

        private void txtCP_Leave(object sender, EventArgs e)
        {
            if (Validar.codigoPostal(txtCP.Text) == false)
            {
                errorProviderForm.SetError(txtCP, "El codigo postal no contiene el formato correcto");

            }
            else
            {
                errorProviderForm.SetError(txtCP, "");
            }
        }



        private void txtCelular_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgUsuarios_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {
            idUsuario = int.Parse(dgUsuarios.Rows[e.RowIndex].Cells[0].Value.ToString());
            txtNombre.Text = dgUsuarios.Rows[e.RowIndex].Cells[1].Value.ToString();
            txtApellidoPaterno.Text = dgUsuarios.Rows[e.RowIndex].Cells[2].Value.ToString();
            txtApellidoMaterno.Text = dgUsuarios.Rows[e.RowIndex].Cells[3].Value.ToString();
            dtpFechaDeNacimiento.Value = DateTime.Parse(dgUsuarios.Rows[e.RowIndex].Cells[4].Value.ToString());
            txtCelular.Text = dgUsuarios.Rows[e.RowIndex].Cells[5].Value.ToString();
            txtTelefono.Text = dgUsuarios.Rows[e.RowIndex].Cells[6].Value.ToString();
            txtCorreo.Text = dgUsuarios.Rows[e.RowIndex].Cells[7].Value.ToString();
            txtCalle.Text = dgUsuarios.Rows[e.RowIndex].Cells[8].Value.ToString();
            txtNumCasa.Text = dgUsuarios.Rows[e.RowIndex].Cells[9].Value.ToString();
            txtCP.Text = dgUsuarios.Rows[e.RowIndex].Cells[10].Value.ToString();
            txtColonia.Text = dgUsuarios.Rows[e.RowIndex].Cells[11].Value.ToString();
            txtFraccionamiento.Text = dgUsuarios.Rows[e.RowIndex].Cells[12].Value.ToString();
            txtLocalidad.Text = dgUsuarios.Rows[e.RowIndex].Cells[13].Value.ToString();
            txtMunicipio.Text = dgUsuarios.Rows[e.RowIndex].Cells[14].Value.ToString();
            lblImagendelComproGuardada.Text = dgUsuarios.Rows[e.RowIndex].Cells[15].Value.ToString();
            lblImagenComproINEGuardada.Text = dgUsuarios.Rows[e.RowIndex].Cells[16].Value.ToString();
            txtCURP.Text = dgUsuarios.Rows[e.RowIndex].Cells[17].Value.ToString();
            lblImagendeComproCURPGuardada.Text = dgUsuarios.Rows[e.RowIndex].Cells[18].Value.ToString();
            lblImagenActaNacGuardada.Text = dgUsuarios.Rows[e.RowIndex].Cells[19].Value.ToString();
            lblCertificadoEstudiosGuardada.Text = dgUsuarios.Rows[e.RowIndex].Cells[20].Value.ToString();
            txtNumSeguroSocial.Text = dgUsuarios.Rows[e.RowIndex].Cells[21].Value.ToString();
            comboBoxTipoUsuario.Text = dgUsuarios.Rows[e.RowIndex].Cells[22].Value.ToString();
            txtContraseña.Text = dgUsuarios.Rows[e.RowIndex].Cells[23].Value.ToString();
        }

        private void btnCargarImgDomicilio_Click(object sender, EventArgs e)
        {
            //Filtros para abrir imagen
            openFileDialogImagenACargar.Filter = "Image Files|*.png;";
            openFileDialogImagenACargar.Filter = "Gif Image (.gif)|*.gif|JPEG Image (.jpeg)|*.jpeg|Png Image (.png)|*.png|JPG Image (.jpg)|*.jpg";
            openFileDialogImagenACargar.Title = "Abrir imagen para el comprobante de domicilio";
            openFileDialogImagenACargar.DefaultExt = ".png";

            if (openFileDialogImagenACargar.ShowDialog() == DialogResult.OK)
            {
                //Tomamos el nombre del archivo
                string fileName = openFileDialogImagenACargar.FileName;


                //Cargamos la imagen al PictureBox
                pictureBoxDomicilio.Image = Image.FromFile(openFileDialogImagenACargar.FileName);

                //Tomamos el nombre original de la imagen
                lblImagenComproDom.Text = openFileDialogImagenACargar.SafeFileName;

                //Se calcula la extencion de la imagen
                string extension = lblImagenComproDom.Text.Substring(lblImagenComproDom.Text.Length - 3);

                //si es '.jpeg' va a decir 'peg', asi que acompletamos.
                extension = extension.ToLower() == "peg" ? "jpeg" : extension.ToLower();

                //Creamo el nombre unico para la imagen con DateTime.
                lblImagendelComproGuardada.Text = string.Format(@"DOM_{0}." + extension, DateTime.Now.Ticks);
            }
        }

        private void btnComproINEGuardada_Click(object sender, EventArgs e)
        {
            //Filtros para abrir imagen
            openFileDialogImagenACargar.Filter = "Image Files|*.png;";
            openFileDialogImagenACargar.Filter = "Gif Image (.gif)|*.gif|JPEG Image (.jpeg)|*.jpeg|Png Image (.png)|*.png|JPG Image (.jpg)|*.jpg";
            openFileDialogImagenACargar.Title = "Abrir imagen para el comprobante de CURP";
            openFileDialogImagenACargar.DefaultExt = ".png";

            if (openFileDialogImagenACargar.ShowDialog() == DialogResult.OK)
            {
                //Tomamos el nombre del archivo
                string fileName = openFileDialogImagenACargar.FileName;


                //Cargamos la imagen al PictureBox
                pictureBoxComproCURP.Image = Image.FromFile(openFileDialogImagenACargar.FileName);

                //Tomamos el nombre original de la imagen
                lblImagenComprobanteCURP.Text = openFileDialogImagenACargar.SafeFileName;

                //Se calcula la extencion de la imagen
                string extension = lblImagenComprobanteCURP.Text.Substring(lblImagenComprobanteCURP.Text.Length - 3);

                //si es '.jpeg' va a decir 'peg', asi que acompletamos.
                extension = extension.ToLower() == "peg" ? "jpeg" : extension.ToLower();

                //Creamo el nombre unico para la imagen con DateTime.
                lblImagendeComproCURPGuardada.Text = string.Format(@"CURP_{0}." + extension, DateTime.Now.Ticks);
            }
        }

        private void btnComproINEGuardada_Click_1(object sender, EventArgs e)
        {
            //Filtros para abrir imagen
            openFileDialogImagenACargar.Filter = "Image Files|*.png;";
            openFileDialogImagenACargar.Filter = "Gif Image (.gif)|*.gif|JPEG Image (.jpeg)|*.jpeg|Png Image (.png)|*.png|JPG Image (.jpg)|*.jpg";
            openFileDialogImagenACargar.Title = "Abrir imagen para el comprobante de INE";
            openFileDialogImagenACargar.DefaultExt = ".png";

            if (openFileDialogImagenACargar.ShowDialog() == DialogResult.OK)
            {
                //Tomamos el nombre del archivo
                string fileName = openFileDialogImagenACargar.FileName;


                //Cargamos la imagen al PictureBox
                pictureBoxComproINE.Image = Image.FromFile(openFileDialogImagenACargar.FileName);

                //Tomamos el nombre original de la imagen
                lblImagenComproINE.Text = openFileDialogImagenACargar.SafeFileName;

                //Se calcula la extencion de la imagen
                string extension = lblImagenComproINE.Text.Substring(lblImagenComproINE.Text.Length - 3);

                //si es '.jpeg' va a decir 'peg', asi que acompletamos.
                extension = extension.ToLower() == "peg" ? "jpeg" : extension.ToLower();

                //Creamo el nombre unico para la imagen con DateTime.
                lblImagenComproINEGuardada.Text = string.Format(@"INE_{0}." + extension, DateTime.Now.Ticks);
            }
        }

        private void btnActaNacimientoGuardar_Click(object sender, EventArgs e)
        {
            //Filtros para abrir imagen
            openFileDialogImagenACargar.Filter = "Image Files|*.png;";
            openFileDialogImagenACargar.Filter = "Gif Image (.gif)|*.gif|JPEG Image (.jpeg)|*.jpeg|Png Image (.png)|*.png|JPG Image (.jpg)|*.jpg";
            openFileDialogImagenACargar.Title = "Abrir imagen para el Acta de nacimiento";
            openFileDialogImagenACargar.DefaultExt = ".png";

            if (openFileDialogImagenACargar.ShowDialog() == DialogResult.OK)
            {
                //Tomamos el nombre del archivo
                string fileName = openFileDialogImagenACargar.FileName;


                //Cargamos la imagen al PictureBox
                pictureBoxActaNacimiento.Image = Image.FromFile(openFileDialogImagenACargar.FileName);

                //Tomamos el nombre original de la imagen
                lblImagenActaNacimiento.Text = openFileDialogImagenACargar.SafeFileName;

                //Se calcula la extencion de la imagen
                string extension = lblImagenActaNacimiento.Text.Substring(lblImagenActaNacimiento.Text.Length - 3);

                //si es '.jpeg' va a decir 'peg', asi que acompletamos.
                extension = extension.ToLower() == "peg" ? "jpeg" : extension.ToLower();

                //Creamo el nombre unico para la imagen con DateTime.
                lblImagenActaNacGuardada.Text = string.Format(@"ACTA_{0}." + extension, DateTime.Now.Ticks);
            }
        }

        private void btnCertificadoEstudiosCargar_Click(object sender, EventArgs e)
        {
            //Filtros para abrir imagen
            openFileDialogImagenACargar.Filter = "Image Files|*.png;";
            openFileDialogImagenACargar.Filter = "Gif Image (.gif)|*.gif|JPEG Image (.jpeg)|*.jpeg|Png Image (.png)|*.png|JPG Image (.jpg)|*.jpg";
            openFileDialogImagenACargar.Title = "Abrir imagen para el certificado de estudios";
            openFileDialogImagenACargar.DefaultExt = ".png";

            if (openFileDialogImagenACargar.ShowDialog() == DialogResult.OK)
            {
                //Tomamos el nombre del archivo
                string fileName = openFileDialogImagenACargar.FileName;


                //Cargamos la imagen al PictureBox
                pictureBoxCertificadoEstudios.Image = Image.FromFile(openFileDialogImagenACargar.FileName);

                //Tomamos el nombre original de la imagen
                lblImagenCertificadoEstudios.Text = openFileDialogImagenACargar.SafeFileName;

                //Se calcula la extencion de la imagen
                string extension = lblImagenCertificadoEstudios.Text.Substring(lblImagenCertificadoEstudios.Text.Length - 3);

                //si es '.jpeg' va a decir 'peg', asi que acompletamos.
                extension = extension.ToLower() == "peg" ? "jpeg" : extension.ToLower();

                //Creamo el nombre unico para la imagen con DateTime.
                lblCertificadoEstudiosGuardada.Text = string.Format(@"CERT_{0}." + extension, DateTime.Now.Ticks);
            }
        }

        private void pictureBoxDomicilio_Click(object sender, EventArgs e)
        {

        }

        private void pictureBoxNext_Click(object sender, EventArgs e)
        {
            //Ocultamos el FORM USUARIOS
            this.Hide();
            //Hacemos una instancia de FrmLOGs
            FrmCatalogoProductos frmPersonaAlternativa = new FrmCatalogoProductos();
            frmPersonaAlternativa.ShowDialog();
        }

        private void iconBtnIngresarAUCliente_Click(object sender, EventArgs e)
        {
            this.Hide();

            FrmPersonasAlternativas alt = new FrmPersonasAlternativas();

            alt.ShowDialog();
        }

        private void iconBtnHome_Click(object sender, EventArgs e)
        {
            this.Hide();

            MenuPrincipal p = new MenuPrincipal();

            p.ShowDialog();
        }
    }
}

