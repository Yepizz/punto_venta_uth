﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib_pdv_uth_v1.usuarios;
using Lib_pdv_uth_v1.clientes;
using Lib_pdv_uth_v1;
using ValidacionesRegEx;

namespace pdv_uth_v1
{
    public partial class FrmPersonasAlternativas : Form
    {
        //ID del usuario de persona alternativa
        int idPersonaAlternativa = 0;
        int idCliente = 0;
        //string idPersonaAlternativ = FrmCatalogoCliente.idPersona.ToString();

        string idPersonaSecundaria = FrmCatalogoCliente.idParaPersona.ToString();

        //FrmPersonasAlternativas frmPer = new FrmPersonasAlternativas();

        PersonaAlternativa personas = new PersonaAlternativa();

        PersonaAlternativa borrar = new PersonaAlternativa();
        PersonaAlternativa per = new PersonaAlternativa();
        public FrmPersonasAlternativas()
        {

            InitializeComponent();
        }

        private void mostrarRegistrosEnDG()
        {
            dgPersonasAlternativas.DataSource = null;
            //borrar todos los ren del DG
            dgPersonasAlternativas.Rows.Clear();
            //se crea lista de criterios de busqueda
            List<CriteriosBusqueda> criterios = new List<CriteriosBusqueda>();
            //se crea objeto de crioterio para busqueda
            CriteriosBusqueda criterio = new CriteriosBusqueda();
            //se asignan los datos del criterio del WHERE
            criterio.campo = " 1 ";
            criterio.operadorIntermedio = OperadorDeConsulta.IGUAL;
            criterio.valor = "1";
            criterio.operadorFinal = OperadorDeConsulta.NINGUNO;
            //se incluye el criterio en la lista de criterios
            criterios.Add(criterio);
            //se ejecuta la consulta y se asigna el resultado al DtaGrid
            dgPersonasAlternativas.DataSource = per.consultar(criterios);
            //se refresca el dataGrid para mostrar los datos
            dgPersonasAlternativas.Refresh();
        }

        private void textBox21_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox24_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox22_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox20_TextChanged(object sender, EventArgs e)
        {

        }

        private void FrmPersonasAlternativas_Load(object sender, EventArgs e)
        {
            toolTipPerAlter.SetToolTip(btnGuardar, "Presiona este botón para agregar una nueva persona alternativa");
            toolTipPerAlter.SetToolTip(btnEditar, "Presiona este botón para editar a esta persona alternativa");
            toolTipPerAlter.SetToolTip(btnEliminar, "Presiona este botón para eliminar a esta persona alternativa");
            toolTipPerAlter.SetToolTip(btnLimpiar, "Presiona este botón para limpiar este formulario");
            toolTipPerAlter.SetToolTip(btnCargarImgDomicilio, "Presiona este botón para cargar una imagen como comprobante de domicilio");
            toolTipPerAlter.SetToolTip(btnComproINEGuardada, "Presiona este botón para cargar un comprobante oficial de INE");
            toolTipPerAlter.SetToolTip(btnComproCURPGuardada, "Presiona este botón para agregar un comprobante oficial de CURP");
            toolTipPerAlter.SetToolTip(iconBtnCerrarForm, "Presiona este botón para cerrar el formulario");
            toolTipPerAlter.SetToolTip(iconBtnToHome, "Presiona este botón para regresar al inicio");
            toolTipPerAlter.SetToolTip(iconBtnToProducts, "Presiona este botón para abrir el formulario para usuarios");
            toolTipPerAlter.SetToolTip(iconBtnBackToUsers, "Presiona este botón para regresar al formulario de clientes");
            lblIIDdelCliente.Text = FrmCatalogoCliente.idParaPersona.ToString();

            //Vamos a actualizar el dgPersonasAlternativas, con los registros de la BD
            mostrarRegistrosEnDG();

            FrmCatalogoCliente cli = new FrmCatalogoCliente();

            cli.Activate();

            //Cargamos una lista de los clientes.

            //Cliente cliente = new Cliente();
            //List<CriteriosBusqueda> lista = new List<CriteriosBusqueda>();
            //lista.Add(new CriteriosBusqueda("1", OperadorDeConsulta.IGUAL, "1", OperadorDeConsulta.NINGUNO));
            //List<Cliente> res = cliente.consulta(lista);
            //if(res !=null)
            //{
            //    comboBoxidCliente.DataSource = res;
            //    comboBoxidCliente.DisplayMember = "Nombre";
            //    comboBoxidCliente.ValueMember = "Id";
            //}

        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            //Hacemos una instancia de Persona alternativa
            PersonaAlternativa guardar = new PersonaAlternativa();
            guardar.Nombre = txtNombre.Text;
            guardar.ApellidoPaterno = txtApellidoPaterno.Text;
            guardar.ApellidoMaterno = txtApellidoMaterno.Text;
            guardar.FechaNacimiento = dtpFechaNacimiento.Value;
            guardar.Celular = txtCelular.Text;
            guardar.Telefono = txtTelefono.Text;
            guardar.Correo = txtCorreoElectronico.Text;
            guardar.Calle = txtCalle.Text;
            guardar.NumCasa = txtNumCasa.Text;
            guardar.CodigoPostal = txtCP.Text;
            guardar.Colonia = txtColonia.Text;
            guardar.Fraccionamiento = txtFraccionamiento.Text;
            guardar.Localidad = txtLocalidad.Text;
            guardar.Municipio = txtMunicipio.Text;
            guardar.ComproDomicilio = lblImagendelComproGuardada.Text;
            guardar.ComprobanteINE = lblImagenComproINEGuardada.Text;
            guardar.Curp = txtCURP.Text;
            guardar.ComproCURP = lblImagendeComproCURPGuardada.Text;
            guardar.idCliente = int.Parse(idPersonaSecundaria);
            //guardar.ClienteID = int.Parse(comboBoxidCliente.SelectedValue.ToString());
            //guardar.ClienteID = int.Parse(lblIDPersona.Text);

            ////guardar.ClienteID = int.Parse(idPersonaAlternativ);




            if (guardar.Alta())
            {

                MessageBox.Show("La persona <" + txtNombre.Text + "> Se ha registrado exitosamente. ", "Nueva persona registrada", MessageBoxButtons.OK, MessageBoxIcon.Information); ;
                mostrarRegistrosEnDG();
                limpiarFormulario();
            }
            else
            {
                MessageBox.Show("Error al guardar al Usuario. " + PersonaAlternativa.msgError);
            }

        }

        public void limpiarFormulario()
        {
            //borramos
            txtNombre.Text = "";
            txtApellidoPaterno.Text = "";
            txtApellidoMaterno.Text = "";
            txtCelular.Text = "";
            txtTelefono.Text = "";
            txtCorreoElectronico.Text = "";
            txtCalle.Text = "";
            txtNumCasa.Text = "";
            txtCP.Text = "";
            txtColonia.Text = "";
            txtFraccionamiento.Text = "";
            txtLocalidad.Text = "";
            txtMunicipio.Text = "";
            pictureBoxDomicilio.Image = null;
            pictureBoxComproINE.Image = null;
            txtCURP.Text = "";
            pictureBoxComproCURP.Image = null;
          
            dtpFechaNacimiento.CustomFormat = null;
 
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
          
            PersonaAlternativa modif = new PersonaAlternativa();
            List<DatosActualizarUsuario> datos = new List<DatosActualizarUsuario>();

            datos.Add(new DatosActualizarUsuario("nombre", txtNombre.Text));
            datos.Add(new DatosActualizarUsuario("apellido_paterno", txtApellidoPaterno.Text));
            datos.Add(new DatosActualizarUsuario("apellido_materno", txtApellidoMaterno.Text));
            datos.Add(new DatosActualizarUsuario("fecha_de_nacimiento", dtpFechaNacimiento.Value.Year + "-" + dtpFechaNacimiento.Value.Month + "-" + dtpFechaNacimiento.Value.Day));
            datos.Add(new DatosActualizarUsuario("celular", txtCelular.Text));
            datos.Add(new DatosActualizarUsuario("telefono", txtTelefono.Text));
            datos.Add(new DatosActualizarUsuario("correo", txtCorreoElectronico.Text));
            datos.Add(new DatosActualizarUsuario("calle", txtCalle.Text));
            datos.Add(new DatosActualizarUsuario("numero_casa", txtNumCasa.Text));
            datos.Add(new DatosActualizarUsuario("codigo_postal", txtCP.Text));
            datos.Add(new DatosActualizarUsuario("colonia", txtColonia.Text));
            datos.Add(new DatosActualizarUsuario("fraccionamiento", txtFraccionamiento.Text));
            datos.Add(new DatosActualizarUsuario("localidad", txtLocalidad.Text));
            datos.Add(new DatosActualizarUsuario("municipio", txtMunicipio.Text));
            datos.Add(new DatosActualizarUsuario("img_comprobante_domicilio", lblImagendelComproGuardada.Text));
            datos.Add(new DatosActualizarUsuario("ine_comprobante", lblImagenComproINEGuardada.Text));
            datos.Add(new DatosActualizarUsuario("curp", txtCURP.Text));
            datos.Add(new DatosActualizarUsuario("curp_comprobante", lblImagendeComproCURPGuardada.Text));
          

            if (modif.modificar(datos, idPersonaAlternativa))
            {
                MessageBox.Show("Persona alternativa modificado");
            }
            else MessageBox.Show("Error persona alternativa no modificado");

            mostrarRegistrosEnDG();
        }


        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Realmente quieres eliminar a esta persona?", "Borrar Persona Alternativa", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                if (borrar.borrar(idPersonaAlternativa))
                {
                    MessageBox.Show("Persona alternativa Eliminado");
                    dgPersonasAlternativas.Refresh();
                }
                else MessageBox.Show("Error la persona alternativa no fue eliminado correctamente. Intente de Nuevo ");
            }
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            limpiarFormulario();
        }

        private void txtNombre_Leave(object sender, EventArgs e)
        {
            if (Validar.nombrePersonal(txtNombre.Text) == false)
            {

                errorProviderForm.SetError(txtNombre, "El nombre debe tener al menos 20 caracteres y no carateres especiales o numeros");

            }
            else
            {
                errorProviderForm.SetError(txtNombre, "");
            }
        }

        private void txtApellidoPaterno_Leave(object sender, EventArgs e)
        {
            if (Validar.nombrePersonal(txtApellidoPaterno.Text) == false)
            {
                errorProviderForm.SetError(txtApellidoPaterno, "El apellido Paterno esta mal escrito!");
            }
            else
            {
                errorProviderForm.SetError(txtApellidoPaterno, "");
            }
        }

        private void txtApellidoMaterno_Leave(object sender, EventArgs e)
        {
            if (Validar.nombrePersonal(txtApellidoMaterno.Text) == false)
            {
                errorProviderForm.SetError(txtApellidoMaterno, "El apellido materno esta mal escrito!");
            }
            else
            {
                errorProviderForm.SetError(txtApellidoMaterno, "");
            }
        }

        private void txtCelular_Leave(object sender, EventArgs e)
        {
            if (Validar.telefono(txtCelular.Text) == false)
            {
                errorProviderForm.SetError(txtCelular, "El celular no tiene formato correcto (62222222)!");
            }
            else
            {
                errorProviderForm.SetError(txtCelular, "");
            }
        }

        private void txtTelefono_Leave(object sender, EventArgs e)
        {
            if (Validar.telefono(txtTelefono.Text) == false)
            {
                errorProviderForm.SetError(txtTelefono, "El celular no tiene formato correcto (62222222)!");
            }
            else
            {
                errorProviderForm.SetError(txtCelular, "");
            }
        }

        private void txtCorreoElectronico_Leave(object sender, EventArgs e)
        {
            if (Validar.correoElectronico(txtCorreoElectronico.Text) == false)
            {
                errorProviderForm.SetError(txtCorreoElectronico, "El correo no tiene el formato correcto, intente de nuevo");

            }
            else
            {
                errorProviderForm.SetError(txtCorreoElectronico, "");
            }
        }

        private void txtCP_Leave(object sender, EventArgs e)
        {
            if (Validar.codigoPostal(txtCP.Text) == false)
            {
                errorProviderForm.SetError(txtCP, "El codigo postal no contiene el formato correcto");

            }
            else
            {
                errorProviderForm.SetError(txtCP, "");
            }
        }

        private void btnCargarImgDomicilio_Click(object sender, EventArgs e)
        {
            //Filtros para abrir imagen
            openFileDialogImagenCargada.Filter = "Image Files|*.png;";
            openFileDialogImagenCargada.Filter = "Gif Image (.gif)|*.gif|JPEG Image (.jpeg)|*.jpeg|Png Image (.png)|*.png|JPG Image (.jpg)|*.jpg";
            openFileDialogImagenCargada.Title = "Abrir imagen para el comprobante de domicilio";
            openFileDialogImagenCargada.DefaultExt = ".png";

            if (openFileDialogImagenCargada.ShowDialog() == DialogResult.OK)
            {
                //Tomamos el nombre del archivo
                string fileName = openFileDialogImagenCargada.FileName;


                //Cargamos la imagen al PictureBox
                pictureBoxDomicilio.Image = Image.FromFile(openFileDialogImagenCargada.FileName);

                //Tomamos el nombre original de la imagen
                lblImagenComproDom.Text = openFileDialogImagenCargada.SafeFileName;

                //Se calcula la extencion de la imagen
                string extension = lblImagenComproDom.Text.Substring(lblImagenComproDom.Text.Length - 3);

                //si es '.jpeg' va a decir 'peg', asi que acompletamos.
                extension = extension.ToLower() == "peg" ? "jpeg" : extension.ToLower();

                //Creamo el nombre unico para la imagen con DateTime.
                lblImagendelComproGuardada.Text = string.Format(@"DOM_{0}." + extension, DateTime.Now.Ticks);
            }
        }

        private void btnComproINEGuardada_Click(object sender, EventArgs e)
        {
            //Filtros para abrir imagen
            openFileDialogImagenCargada.Filter = "Image Files|*.png;";
            openFileDialogImagenCargada.Filter = "Gif Image (.gif)|*.gif|JPEG Image (.jpeg)|*.jpeg|Png Image (.png)|*.png|JPG Image (.jpg)|*.jpg";
            openFileDialogImagenCargada.Title = "Abrir imagen para el comprobante de INE";
            openFileDialogImagenCargada.DefaultExt = ".png";

            if (openFileDialogImagenCargada.ShowDialog() == DialogResult.OK)
            {
                //Tomamos el nombre del archivo
                string fileName = openFileDialogImagenCargada.FileName;


                //Cargamos la imagen al PictureBox
                pictureBoxComproINE.Image = Image.FromFile(openFileDialogImagenCargada.FileName);

                //Tomamos el nombre original de la imagen
                lblImagenComproINE.Text = openFileDialogImagenCargada.SafeFileName;

                //Se calcula la extencion de la imagen
                string extension = lblImagenComproINE.Text.Substring(lblImagenComproINE.Text.Length - 3);

                //si es '.jpeg' va a decir 'peg', asi que acompletamos.
                extension = extension.ToLower() == "peg" ? "jpeg" : extension.ToLower();

                //Creamo el nombre unico para la imagen con DateTime.
                lblImagenComproINEGuardada.Text = string.Format(@"INE_{0}." + extension, DateTime.Now.Ticks);
            }
        }

        private void btnComproCURPGuardada_Click(object sender, EventArgs e)
        {
            //Filtros para abrir imagen
            openFileDialogImagenCargada.Filter = "Image Files|*.png;";
            openFileDialogImagenCargada.Filter = "Gif Image (.gif)|*.gif|JPEG Image (.jpeg)|*.jpeg|Png Image (.png)|*.png|JPG Image (.jpg)|*.jpg";
            openFileDialogImagenCargada.Title = "Abrir imagen para el comprobante de CURP";
            openFileDialogImagenCargada.DefaultExt = ".png";

            if (openFileDialogImagenCargada.ShowDialog() == DialogResult.OK)
            {
                //Tomamos el nombre del archivo
                string fileName = openFileDialogImagenCargada.FileName;


                //Cargamos la imagen al PictureBox
                pictureBoxComproCURP.Image = Image.FromFile(openFileDialogImagenCargada.FileName);

                //Tomamos el nombre original de la imagen
                lblImagenComprobanteCURP.Text = openFileDialogImagenCargada.SafeFileName;

                //Se calcula la extencion de la imagen
                string extension = lblImagenComprobanteCURP.Text.Substring(lblImagenComprobanteCURP.Text.Length - 3);

                //si es '.jpeg' va a decir 'peg', asi que acompletamos.
                extension = extension.ToLower() == "peg" ? "jpeg" : extension.ToLower();

                //Creamo el nombre unico para la imagen con DateTime.
                lblImagendeComproCURPGuardada.Text = string.Format(@"CURP_{0}." + extension, DateTime.Now.Ticks);
            }
        }

        private void dgPersonasAlternativas_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            idPersonaAlternativa = int.Parse(dgPersonasAlternativas.Rows[e.RowIndex].Cells[0].Value.ToString());
            txtNombre.Text = dgPersonasAlternativas.Rows[e.RowIndex].Cells[1].Value.ToString();
            txtApellidoPaterno.Text = dgPersonasAlternativas.Rows[e.RowIndex].Cells[2].Value.ToString();
            txtApellidoMaterno.Text = dgPersonasAlternativas.Rows[e.RowIndex].Cells[3].Value.ToString();
            dtpFechaNacimiento.Value = DateTime.Parse(dgPersonasAlternativas.Rows[e.RowIndex].Cells[4].Value.ToString());
            txtCelular.Text = dgPersonasAlternativas.Rows[e.RowIndex].Cells[5].Value.ToString();
            txtTelefono.Text = dgPersonasAlternativas.Rows[e.RowIndex].Cells[6].Value.ToString();
            txtCorreoElectronico.Text = dgPersonasAlternativas.Rows[e.RowIndex].Cells[7].Value.ToString();
            txtCalle.Text = dgPersonasAlternativas.Rows[e.RowIndex].Cells[8].Value.ToString();
            txtNumCasa.Text = dgPersonasAlternativas.Rows[e.RowIndex].Cells[9].Value.ToString();
            txtCP.Text = dgPersonasAlternativas.Rows[e.RowIndex].Cells[10].Value.ToString();
            txtColonia.Text = dgPersonasAlternativas.Rows[e.RowIndex].Cells[11].Value.ToString();
            txtFraccionamiento.Text = dgPersonasAlternativas.Rows[e.RowIndex].Cells[12].Value.ToString();
            txtLocalidad.Text = dgPersonasAlternativas.Rows[e.RowIndex].Cells[13].Value.ToString();
            txtMunicipio.Text = dgPersonasAlternativas.Rows[e.RowIndex].Cells[14].Value.ToString();
            lblImagendelComproGuardada.Text = dgPersonasAlternativas.Rows[e.RowIndex].Cells[15].Value.ToString();
            lblImagenComproINEGuardada.Text = dgPersonasAlternativas.Rows[e.RowIndex].Cells[16].Value.ToString();
            txtCURP.Text = dgPersonasAlternativas.Rows[e.RowIndex].Cells[17].Value.ToString();
            lblImagendeComproCURPGuardada.Text = dgPersonasAlternativas.Rows[e.RowIndex].Cells[18].Value.ToString();
            //idCliente = int.Parse(dgPersonasAlternativas.Rows[e.RowIndex].Cells[19].Value.ToString());


        }

        private void btnCerrarTodo_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void iconBtnCerrarForm_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void iconBtnToHome_Click(object sender, EventArgs e)
        {
            this.Hide();

            MenuPrincipal n = new MenuPrincipal();

            n.ShowDialog();
        }

        private void iconBtnBackToUsers_Click(object sender, EventArgs e)
        {
            this.Hide();

          
        }

        private void iconBtnToProducts_Click(object sender, EventArgs e)
        {
            this.Hide();

            FrmUsuarios pr = new FrmUsuarios();

            pr.ShowDialog();
        }

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {

        }

        private void lblIDPersona_Click(object sender, EventArgs e)
        {
            //lblIDPersona.Text = FrmCatalogoCliente.idPersona.ToString();
        }

        private void comboBoxidCliente_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void lblIIDdelCliente_Click(object sender, EventArgs e)
        {
           
        }
    }
}
