﻿namespace pdv_uth_v1
{
    partial class FrmVentas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmVentas));
            this.panel1 = new System.Windows.Forms.Panel();
            this.iconBtnHome = new FontAwesome.Sharp.IconButton();
            this.iconBtnClose = new FontAwesome.Sharp.IconButton();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.panel3 = new System.Windows.Forms.Panel();
            this.lblHora = new System.Windows.Forms.Label();
            this.iconBtnClock = new FontAwesome.Sharp.IconButton();
            this.iconBtnBack = new FontAwesome.Sharp.IconButton();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(176)))), ((int)(((byte)(105)))));
            this.panel1.Controls.Add(this.iconBtnHome);
            this.panel1.Controls.Add(this.iconBtnClose);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(977, 59);
            this.panel1.TabIndex = 0;
            // 
            // iconBtnHome
            // 
            this.iconBtnHome.Dock = System.Windows.Forms.DockStyle.Right;
            this.iconBtnHome.FlatAppearance.BorderSize = 0;
            this.iconBtnHome.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.iconBtnHome.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.iconBtnHome.IconChar = FontAwesome.Sharp.IconChar.Home;
            this.iconBtnHome.IconColor = System.Drawing.Color.Navy;
            this.iconBtnHome.IconSize = 75;
            this.iconBtnHome.Location = new System.Drawing.Point(787, 0);
            this.iconBtnHome.Name = "iconBtnHome";
            this.iconBtnHome.Rotation = 0D;
            this.iconBtnHome.Size = new System.Drawing.Size(90, 59);
            this.iconBtnHome.TabIndex = 8;
            this.iconBtnHome.UseVisualStyleBackColor = true;
            this.iconBtnHome.Click += new System.EventHandler(this.iconBtnHome_Click);
            // 
            // iconBtnClose
            // 
            this.iconBtnClose.Dock = System.Windows.Forms.DockStyle.Right;
            this.iconBtnClose.FlatAppearance.BorderSize = 0;
            this.iconBtnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.iconBtnClose.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.iconBtnClose.IconChar = FontAwesome.Sharp.IconChar.PowerOff;
            this.iconBtnClose.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.iconBtnClose.IconSize = 65;
            this.iconBtnClose.ImageAlign = System.Drawing.ContentAlignment.TopRight;
            this.iconBtnClose.Location = new System.Drawing.Point(877, 0);
            this.iconBtnClose.Name = "iconBtnClose";
            this.iconBtnClose.Rotation = 0D;
            this.iconBtnClose.Size = new System.Drawing.Size(100, 59);
            this.iconBtnClose.TabIndex = 7;
            this.iconBtnClose.UseVisualStyleBackColor = true;
            this.iconBtnClose.Click += new System.EventHandler(this.iconBtnClose_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe Print", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.label1.Location = new System.Drawing.Point(3, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(145, 51);
            this.label1.TabIndex = 6;
            this.label1.Text = "VENTAS";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.dataGridView1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(0, 59);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(351, 441);
            this.panel2.TabIndex = 1;
            // 
            // dataGridView1
            // 
            this.dataGridView1.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(189)))), ((int)(((byte)(84)))));
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Left;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(350, 441);
            this.dataGridView1.TabIndex = 0;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(207)))), ((int)(((byte)(167)))));
            this.panel3.Controls.Add(this.lblHora);
            this.panel3.Controls.Add(this.iconBtnClock);
            this.panel3.Controls.Add(this.iconBtnBack);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(351, 412);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(626, 88);
            this.panel3.TabIndex = 2;
            // 
            // lblHora
            // 
            this.lblHora.AutoSize = true;
            this.lblHora.Font = new System.Drawing.Font("Segoe Print", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHora.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(156)))), ((int)(((byte)(0)))));
            this.lblHora.Location = new System.Drawing.Point(77, 22);
            this.lblHora.Name = "lblHora";
            this.lblHora.Size = new System.Drawing.Size(0, 37);
            this.lblHora.TabIndex = 8;
            // 
            // iconBtnClock
            // 
            this.iconBtnClock.Dock = System.Windows.Forms.DockStyle.Left;
            this.iconBtnClock.FlatAppearance.BorderSize = 0;
            this.iconBtnClock.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.iconBtnClock.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.iconBtnClock.IconChar = FontAwesome.Sharp.IconChar.Clock;
            this.iconBtnClock.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(141)))), ((int)(((byte)(84)))));
            this.iconBtnClock.IconSize = 75;
            this.iconBtnClock.Location = new System.Drawing.Point(0, 0);
            this.iconBtnClock.Name = "iconBtnClock";
            this.iconBtnClock.Rotation = 0D;
            this.iconBtnClock.Size = new System.Drawing.Size(71, 88);
            this.iconBtnClock.TabIndex = 5;
            this.iconBtnClock.UseVisualStyleBackColor = true;
            // 
            // iconBtnBack
            // 
            this.iconBtnBack.Dock = System.Windows.Forms.DockStyle.Right;
            this.iconBtnBack.FlatAppearance.BorderSize = 0;
            this.iconBtnBack.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.iconBtnBack.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.iconBtnBack.IconChar = FontAwesome.Sharp.IconChar.ArrowCircleLeft;
            this.iconBtnBack.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(141)))), ((int)(((byte)(84)))));
            this.iconBtnBack.IconSize = 75;
            this.iconBtnBack.Location = new System.Drawing.Point(555, 0);
            this.iconBtnBack.Name = "iconBtnBack";
            this.iconBtnBack.Rotation = 0D;
            this.iconBtnBack.Size = new System.Drawing.Size(71, 88);
            this.iconBtnBack.TabIndex = 4;
            this.iconBtnBack.UseVisualStyleBackColor = true;
            this.iconBtnBack.Click += new System.EventHandler(this.iconBtnBack_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::pdv_uth_v1.Properties.Resources._90416716_145095500156411_8810673925076811776_n4;
            this.pictureBox1.Location = new System.Drawing.Point(636, 65);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(341, 297);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 7;
            this.pictureBox1.TabStop = false;
            // 
            // FrmVentas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(171)))), ((int)(((byte)(39)))));
            this.ClientSize = new System.Drawing.Size(977, 500);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmVentas";
            this.Text = "FrmVentas";
            this.Load += new System.EventHandler(this.FrmVentas_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Panel panel3;
        private FontAwesome.Sharp.IconButton iconBtnClock;
        private FontAwesome.Sharp.IconButton iconBtnBack;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private FontAwesome.Sharp.IconButton iconBtnClose;
        private System.Windows.Forms.Label lblHora;
        private FontAwesome.Sharp.IconButton iconBtnHome;
    }
}