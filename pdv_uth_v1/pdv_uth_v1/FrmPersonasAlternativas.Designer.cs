﻿namespace pdv_uth_v1
{
    partial class FrmPersonasAlternativas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmPersonasAlternativas));
            this.panel1 = new System.Windows.Forms.Panel();
            this.iconBtnToHome = new FontAwesome.Sharp.IconButton();
            this.iconBtnCerrarForm = new FontAwesome.Sharp.IconButton();
            this.label1 = new System.Windows.Forms.Label();
            this.panelDataGrid = new System.Windows.Forms.Panel();
            this.dgPersonasAlternativas = new System.Windows.Forms.DataGridView();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lblIIDdelCliente = new System.Windows.Forms.Label();
            this.lblIDPersona = new System.Windows.Forms.Label();
            this.iconBtnToProducts = new FontAwesome.Sharp.IconButton();
            this.iconBtnBackToUsers = new FontAwesome.Sharp.IconButton();
            this.lblImagenComprobanteCURP = new System.Windows.Forms.Label();
            this.lblImagendeComproCURPGuardada = new System.Windows.Forms.Label();
            this.lblImagenComproINE = new System.Windows.Forms.Label();
            this.lblImagenComproINEGuardada = new System.Windows.Forms.Label();
            this.lblImagendelComproGuardada = new System.Windows.Forms.Label();
            this.lblImagenComproDom = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnEditar = new System.Windows.Forms.Button();
            this.btnEliminar = new System.Windows.Forms.Button();
            this.btnLimpiar = new System.Windows.Forms.Button();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.btnComproCURPGuardada = new System.Windows.Forms.Button();
            this.pictureBoxComproCURP = new System.Windows.Forms.PictureBox();
            this.btnComproINEGuardada = new System.Windows.Forms.Button();
            this.btnCargarImgDomicilio = new System.Windows.Forms.Button();
            this.pictureBoxDomicilio = new System.Windows.Forms.PictureBox();
            this.txtApellidoPaterno = new System.Windows.Forms.TextBox();
            this.txtFraccionamiento = new System.Windows.Forms.TextBox();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.txtCURP = new System.Windows.Forms.TextBox();
            this.txtNumCasa = new System.Windows.Forms.TextBox();
            this.txtLocalidad = new System.Windows.Forms.TextBox();
            this.txtMunicipio = new System.Windows.Forms.TextBox();
            this.txtCalle = new System.Windows.Forms.TextBox();
            this.txtColonia = new System.Windows.Forms.TextBox();
            this.txtCelular = new System.Windows.Forms.TextBox();
            this.txtTelefono = new System.Windows.Forms.TextBox();
            this.txtCorreoElectronico = new System.Windows.Forms.TextBox();
            this.txtCP = new System.Windows.Forms.TextBox();
            this.txtApellidoMaterno = new System.Windows.Forms.TextBox();
            this.pictureBoxComproINE = new System.Windows.Forms.PictureBox();
            this.dtpFechaNacimiento = new System.Windows.Forms.DateTimePicker();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.openFileDialogImagenCargada = new System.Windows.Forms.OpenFileDialog();
            this.errorProviderForm = new System.Windows.Forms.ErrorProvider(this.components);
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolTipPerAlter = new System.Windows.Forms.ToolTip(this.components);
            this.panel1.SuspendLayout();
            this.panelDataGrid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgPersonasAlternativas)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxComproCURP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxDomicilio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxComproINE)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderForm)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(176)))), ((int)(((byte)(105)))));
            this.panel1.Controls.Add(this.iconBtnToHome);
            this.panel1.Controls.Add(this.iconBtnCerrarForm);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1366, 54);
            this.panel1.TabIndex = 0;
            // 
            // iconBtnToHome
            // 
            this.iconBtnToHome.Dock = System.Windows.Forms.DockStyle.Right;
            this.iconBtnToHome.FlatAppearance.BorderSize = 0;
            this.iconBtnToHome.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.iconBtnToHome.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.iconBtnToHome.IconChar = FontAwesome.Sharp.IconChar.Home;
            this.iconBtnToHome.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.iconBtnToHome.IconSize = 40;
            this.iconBtnToHome.Location = new System.Drawing.Point(1266, 0);
            this.iconBtnToHome.Name = "iconBtnToHome";
            this.iconBtnToHome.Rotation = 0D;
            this.iconBtnToHome.Size = new System.Drawing.Size(50, 54);
            this.iconBtnToHome.TabIndex = 75;
            this.iconBtnToHome.UseVisualStyleBackColor = true;
            this.iconBtnToHome.Click += new System.EventHandler(this.iconBtnToHome_Click);
            // 
            // iconBtnCerrarForm
            // 
            this.iconBtnCerrarForm.Dock = System.Windows.Forms.DockStyle.Right;
            this.iconBtnCerrarForm.FlatAppearance.BorderSize = 0;
            this.iconBtnCerrarForm.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.iconBtnCerrarForm.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.iconBtnCerrarForm.IconChar = FontAwesome.Sharp.IconChar.PowerOff;
            this.iconBtnCerrarForm.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.iconBtnCerrarForm.IconSize = 40;
            this.iconBtnCerrarForm.Location = new System.Drawing.Point(1316, 0);
            this.iconBtnCerrarForm.Name = "iconBtnCerrarForm";
            this.iconBtnCerrarForm.Rotation = 0D;
            this.iconBtnCerrarForm.Size = new System.Drawing.Size(50, 54);
            this.iconBtnCerrarForm.TabIndex = 76;
            this.iconBtnCerrarForm.UseVisualStyleBackColor = true;
            this.iconBtnCerrarForm.Click += new System.EventHandler(this.iconBtnCerrarForm_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe Print", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.label1.Location = new System.Drawing.Point(0, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(587, 43);
            this.label1.TabIndex = 0;
            this.label1.Text = "FORMULARIO DE PERSONAS ALTERNATIVAS";
            // 
            // panelDataGrid
            // 
            this.panelDataGrid.Controls.Add(this.dgPersonasAlternativas);
            this.panelDataGrid.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelDataGrid.Location = new System.Drawing.Point(0, 370);
            this.panelDataGrid.Name = "panelDataGrid";
            this.panelDataGrid.Size = new System.Drawing.Size(1366, 129);
            this.panelDataGrid.TabIndex = 2;
            // 
            // dgPersonasAlternativas
            // 
            this.dgPersonasAlternativas.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgPersonasAlternativas.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(192)))), ((int)(((byte)(140)))));
            this.dgPersonasAlternativas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(189)))), ((int)(((byte)(84)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe Print", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(141)))), ((int)(((byte)(84)))));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(207)))), ((int)(((byte)(167)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(156)))), ((int)(((byte)(0)))));
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgPersonasAlternativas.DefaultCellStyle = dataGridViewCellStyle1;
            this.dgPersonasAlternativas.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgPersonasAlternativas.Location = new System.Drawing.Point(0, 0);
            this.dgPersonasAlternativas.Name = "dgPersonasAlternativas";
            this.dgPersonasAlternativas.Size = new System.Drawing.Size(1366, 129);
            this.dgPersonasAlternativas.TabIndex = 0;
            this.dgPersonasAlternativas.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgPersonasAlternativas_CellContentClick);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(171)))), ((int)(((byte)(39)))));
            this.panel2.Controls.Add(this.lblIIDdelCliente);
            this.panel2.Controls.Add(this.lblIDPersona);
            this.panel2.Controls.Add(this.iconBtnToProducts);
            this.panel2.Controls.Add(this.iconBtnBackToUsers);
            this.panel2.Controls.Add(this.lblImagenComprobanteCURP);
            this.panel2.Controls.Add(this.lblImagendeComproCURPGuardada);
            this.panel2.Controls.Add(this.lblImagenComproINE);
            this.panel2.Controls.Add(this.lblImagenComproINEGuardada);
            this.panel2.Controls.Add(this.lblImagendelComproGuardada);
            this.panel2.Controls.Add(this.lblImagenComproDom);
            this.panel2.Controls.Add(this.pictureBox1);
            this.panel2.Controls.Add(this.btnEditar);
            this.panel2.Controls.Add(this.btnEliminar);
            this.panel2.Controls.Add(this.btnLimpiar);
            this.panel2.Controls.Add(this.btnGuardar);
            this.panel2.Controls.Add(this.btnComproCURPGuardada);
            this.panel2.Controls.Add(this.pictureBoxComproCURP);
            this.panel2.Controls.Add(this.btnComproINEGuardada);
            this.panel2.Controls.Add(this.btnCargarImgDomicilio);
            this.panel2.Controls.Add(this.pictureBoxDomicilio);
            this.panel2.Controls.Add(this.txtApellidoPaterno);
            this.panel2.Controls.Add(this.txtFraccionamiento);
            this.panel2.Controls.Add(this.txtNombre);
            this.panel2.Controls.Add(this.txtCURP);
            this.panel2.Controls.Add(this.txtNumCasa);
            this.panel2.Controls.Add(this.txtLocalidad);
            this.panel2.Controls.Add(this.txtMunicipio);
            this.panel2.Controls.Add(this.txtCalle);
            this.panel2.Controls.Add(this.txtColonia);
            this.panel2.Controls.Add(this.txtCelular);
            this.panel2.Controls.Add(this.txtTelefono);
            this.panel2.Controls.Add(this.txtCorreoElectronico);
            this.panel2.Controls.Add(this.txtCP);
            this.panel2.Controls.Add(this.txtApellidoMaterno);
            this.panel2.Controls.Add(this.pictureBoxComproINE);
            this.panel2.Controls.Add(this.dtpFechaNacimiento);
            this.panel2.Controls.Add(this.label20);
            this.panel2.Controls.Add(this.label19);
            this.panel2.Controls.Add(this.label17);
            this.panel2.Controls.Add(this.label16);
            this.panel2.Controls.Add(this.label15);
            this.panel2.Controls.Add(this.label14);
            this.panel2.Controls.Add(this.label13);
            this.panel2.Controls.Add(this.label12);
            this.panel2.Controls.Add(this.label11);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 54);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1366, 355);
            this.panel2.TabIndex = 3;
            // 
            // lblIIDdelCliente
            // 
            this.lblIIDdelCliente.AutoSize = true;
            this.lblIIDdelCliente.Font = new System.Drawing.Font("Segoe Print", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIIDdelCliente.ForeColor = System.Drawing.Color.Navy;
            this.lblIIDdelCliente.Location = new System.Drawing.Point(772, 229);
            this.lblIIDdelCliente.Name = "lblIIDdelCliente";
            this.lblIIDdelCliente.Size = new System.Drawing.Size(129, 33);
            this.lblIIDdelCliente.TabIndex = 77;
            this.lblIIDdelCliente.Text = "CLIENTE ID";
            this.lblIIDdelCliente.Click += new System.EventHandler(this.lblIIDdelCliente_Click);
            // 
            // lblIDPersona
            // 
            this.lblIDPersona.AutoSize = true;
            this.lblIDPersona.Location = new System.Drawing.Point(775, 249);
            this.lblIDPersona.Name = "lblIDPersona";
            this.lblIDPersona.Size = new System.Drawing.Size(0, 13);
            this.lblIDPersona.TabIndex = 76;
            this.lblIDPersona.Click += new System.EventHandler(this.lblIDPersona_Click);
            // 
            // iconBtnToProducts
            // 
            this.iconBtnToProducts.FlatAppearance.BorderSize = 0;
            this.iconBtnToProducts.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.iconBtnToProducts.Flip = FontAwesome.Sharp.FlipOrientation.Vertical;
            this.iconBtnToProducts.IconChar = FontAwesome.Sharp.IconChar.Share;
            this.iconBtnToProducts.IconColor = System.Drawing.Color.Olive;
            this.iconBtnToProducts.IconSize = 75;
            this.iconBtnToProducts.Location = new System.Drawing.Point(1282, 255);
            this.iconBtnToProducts.Name = "iconBtnToProducts";
            this.iconBtnToProducts.Rotation = 0D;
            this.iconBtnToProducts.Size = new System.Drawing.Size(72, 73);
            this.iconBtnToProducts.TabIndex = 75;
            this.iconBtnToProducts.UseVisualStyleBackColor = true;
            this.iconBtnToProducts.Click += new System.EventHandler(this.iconBtnToProducts_Click);
            // 
            // iconBtnBackToUsers
            // 
            this.iconBtnBackToUsers.FlatAppearance.BorderSize = 0;
            this.iconBtnBackToUsers.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.iconBtnBackToUsers.Flip = FontAwesome.Sharp.FlipOrientation.Horizontal;
            this.iconBtnBackToUsers.IconChar = FontAwesome.Sharp.IconChar.Share;
            this.iconBtnBackToUsers.IconColor = System.Drawing.Color.Red;
            this.iconBtnBackToUsers.IconSize = 75;
            this.iconBtnBackToUsers.Location = new System.Drawing.Point(1050, 260);
            this.iconBtnBackToUsers.Name = "iconBtnBackToUsers";
            this.iconBtnBackToUsers.Rotation = 0D;
            this.iconBtnBackToUsers.Size = new System.Drawing.Size(72, 73);
            this.iconBtnBackToUsers.TabIndex = 74;
            this.iconBtnBackToUsers.UseVisualStyleBackColor = true;
            this.iconBtnBackToUsers.Click += new System.EventHandler(this.iconBtnBackToUsers_Click);
            // 
            // lblImagenComprobanteCURP
            // 
            this.lblImagenComprobanteCURP.AutoSize = true;
            this.lblImagenComprobanteCURP.Font = new System.Drawing.Font("Segoe Print", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblImagenComprobanteCURP.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.lblImagenComprobanteCURP.Location = new System.Drawing.Point(633, 128);
            this.lblImagenComprobanteCURP.Name = "lblImagenComprobanteCURP";
            this.lblImagenComprobanteCURP.Size = new System.Drawing.Size(97, 12);
            this.lblImagenComprobanteCURP.TabIndex = 70;
            this.lblImagenComprobanteCURP.Text = "Imagen del Comprobante";
            this.lblImagenComprobanteCURP.Visible = false;
            // 
            // lblImagendeComproCURPGuardada
            // 
            this.lblImagendeComproCURPGuardada.AutoSize = true;
            this.lblImagendeComproCURPGuardada.Font = new System.Drawing.Font("Segoe Print", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblImagendeComproCURPGuardada.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.lblImagendeComproCURPGuardada.Location = new System.Drawing.Point(638, 107);
            this.lblImagendeComproCURPGuardada.Name = "lblImagendeComproCURPGuardada";
            this.lblImagendeComproCURPGuardada.Size = new System.Drawing.Size(97, 12);
            this.lblImagendeComproCURPGuardada.TabIndex = 69;
            this.lblImagendeComproCURPGuardada.Text = "Imagen del Comprobante";
            this.lblImagendeComproCURPGuardada.Visible = false;
            // 
            // lblImagenComproINE
            // 
            this.lblImagenComproINE.AutoSize = true;
            this.lblImagenComproINE.Font = new System.Drawing.Font("Segoe Print", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblImagenComproINE.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.lblImagenComproINE.Location = new System.Drawing.Point(638, 29);
            this.lblImagenComproINE.Name = "lblImagenComproINE";
            this.lblImagenComproINE.Size = new System.Drawing.Size(97, 12);
            this.lblImagenComproINE.TabIndex = 68;
            this.lblImagenComproINE.Text = "Imagen del Comprobante";
            this.lblImagenComproINE.Visible = false;
            // 
            // lblImagenComproINEGuardada
            // 
            this.lblImagenComproINEGuardada.AutoSize = true;
            this.lblImagenComproINEGuardada.Font = new System.Drawing.Font("Segoe Print", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblImagenComproINEGuardada.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.lblImagenComproINEGuardada.Location = new System.Drawing.Point(638, 6);
            this.lblImagenComproINEGuardada.Name = "lblImagenComproINEGuardada";
            this.lblImagenComproINEGuardada.Size = new System.Drawing.Size(97, 12);
            this.lblImagenComproINEGuardada.TabIndex = 67;
            this.lblImagenComproINEGuardada.Text = "Imagen del Comprobante";
            this.lblImagenComproINEGuardada.Visible = false;
            // 
            // lblImagendelComproGuardada
            // 
            this.lblImagendelComproGuardada.AutoSize = true;
            this.lblImagendelComproGuardada.Font = new System.Drawing.Font("Segoe Print", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblImagendelComproGuardada.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.lblImagendelComproGuardada.Location = new System.Drawing.Point(345, 170);
            this.lblImagendelComproGuardada.Name = "lblImagendelComproGuardada";
            this.lblImagendelComproGuardada.Size = new System.Drawing.Size(97, 12);
            this.lblImagendelComproGuardada.TabIndex = 59;
            this.lblImagendelComproGuardada.Text = "Imagen del Comprobante";
            this.lblImagendelComproGuardada.Visible = false;
            // 
            // lblImagenComproDom
            // 
            this.lblImagenComproDom.AutoSize = true;
            this.lblImagenComproDom.Font = new System.Drawing.Font("Segoe Print", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblImagenComproDom.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.lblImagenComproDom.Location = new System.Drawing.Point(345, 182);
            this.lblImagenComproDom.Name = "lblImagenComproDom";
            this.lblImagenComproDom.Size = new System.Drawing.Size(97, 12);
            this.lblImagenComproDom.TabIndex = 58;
            this.lblImagenComproDom.Text = "Imagen del Comprobante";
            this.lblImagenComproDom.Visible = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::pdv_uth_v1.Properties.Resources._90416716_145095500156411_8810673925076811776_n2;
            this.pictureBox1.Location = new System.Drawing.Point(1107, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(256, 251);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 54;
            this.pictureBox1.TabStop = false;
            // 
            // btnEditar
            // 
            this.btnEditar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(176)))), ((int)(((byte)(105)))));
            this.btnEditar.FlatAppearance.BorderSize = 30;
            this.btnEditar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnEditar.Image = global::pdv_uth_v1.Properties.Resources.businessapplication_edit_male_user_thepencil_theclient_negocio_23212;
            this.btnEditar.Location = new System.Drawing.Point(579, 305);
            this.btnEditar.Name = "btnEditar";
            this.btnEditar.Size = new System.Drawing.Size(86, 47);
            this.btnEditar.TabIndex = 53;
            this.btnEditar.UseVisualStyleBackColor = false;
            this.btnEditar.Click += new System.EventHandler(this.btnEditar_Click);
            // 
            // btnEliminar
            // 
            this.btnEliminar.FlatAppearance.BorderSize = 30;
            this.btnEliminar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnEliminar.Image = global::pdv_uth_v1.Properties.Resources.delete_delete_deleteusers_delete_male_user_maleclient_23482;
            this.btnEliminar.Location = new System.Drawing.Point(686, 305);
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Size = new System.Drawing.Size(86, 47);
            this.btnEliminar.TabIndex = 52;
            this.btnEliminar.UseVisualStyleBackColor = true;
            this.btnEliminar.Click += new System.EventHandler(this.btnEliminar_Click);
            // 
            // btnLimpiar
            // 
            this.btnLimpiar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(176)))), ((int)(((byte)(105)))));
            this.btnLimpiar.FlatAppearance.BorderSize = 30;
            this.btnLimpiar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnLimpiar.Image = global::pdv_uth_v1.Properties.Resources._3792081_broom_halloween_magic_witch_1090492;
            this.btnLimpiar.Location = new System.Drawing.Point(798, 305);
            this.btnLimpiar.Name = "btnLimpiar";
            this.btnLimpiar.Size = new System.Drawing.Size(86, 47);
            this.btnLimpiar.TabIndex = 51;
            this.btnLimpiar.UseVisualStyleBackColor = false;
            this.btnLimpiar.Click += new System.EventHandler(this.btnLimpiar_Click);
            // 
            // btnGuardar
            // 
            this.btnGuardar.FlatAppearance.BorderSize = 30;
            this.btnGuardar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnGuardar.Image = global::pdv_uth_v1.Properties.Resources.business_application_addmale_useradd_insert_add_user_client_23122;
            this.btnGuardar.Location = new System.Drawing.Point(472, 305);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(86, 47);
            this.btnGuardar.TabIndex = 50;
            this.btnGuardar.UseVisualStyleBackColor = true;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // btnComproCURPGuardada
            // 
            this.btnComproCURPGuardada.FlatAppearance.BorderSize = 30;
            this.btnComproCURPGuardada.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnComproCURPGuardada.Image = global::pdv_uth_v1.Properties.Resources._1486505253_folder_folder_up_folder_upload_update_folder_upload_81427__2_1;
            this.btnComproCURPGuardada.Location = new System.Drawing.Point(947, 181);
            this.btnComproCURPGuardada.Name = "btnComproCURPGuardada";
            this.btnComproCURPGuardada.Size = new System.Drawing.Size(45, 36);
            this.btnComproCURPGuardada.TabIndex = 18;
            this.btnComproCURPGuardada.UseVisualStyleBackColor = true;
            this.btnComproCURPGuardada.Click += new System.EventHandler(this.btnComproCURPGuardada_Click);
            // 
            // pictureBoxComproCURP
            // 
            this.pictureBoxComproCURP.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(156)))), ((int)(((byte)(0)))));
            this.pictureBoxComproCURP.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBoxComproCURP.Location = new System.Drawing.Point(778, 112);
            this.pictureBoxComproCURP.Name = "pictureBoxComproCURP";
            this.pictureBoxComproCURP.Size = new System.Drawing.Size(163, 105);
            this.pictureBoxComproCURP.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxComproCURP.TabIndex = 48;
            this.pictureBoxComproCURP.TabStop = false;
            // 
            // btnComproINEGuardada
            // 
            this.btnComproINEGuardada.FlatAppearance.BorderSize = 30;
            this.btnComproINEGuardada.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnComproINEGuardada.Image = global::pdv_uth_v1.Properties.Resources.iconfinder_upload_4341320_120532;
            this.btnComproINEGuardada.Location = new System.Drawing.Point(947, 70);
            this.btnComproINEGuardada.Name = "btnComproINEGuardada";
            this.btnComproINEGuardada.Size = new System.Drawing.Size(45, 36);
            this.btnComproINEGuardada.TabIndex = 17;
            this.btnComproINEGuardada.UseVisualStyleBackColor = true;
            this.btnComproINEGuardada.Click += new System.EventHandler(this.btnComproINEGuardada_Click);
            // 
            // btnCargarImgDomicilio
            // 
            this.btnCargarImgDomicilio.FlatAppearance.BorderSize = 0;
            this.btnCargarImgDomicilio.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCargarImgDomicilio.Image = global::pdv_uth_v1.Properties.Resources.upload_information_16316__1_1;
            this.btnCargarImgDomicilio.Location = new System.Drawing.Point(692, 238);
            this.btnCargarImgDomicilio.Name = "btnCargarImgDomicilio";
            this.btnCargarImgDomicilio.Size = new System.Drawing.Size(43, 35);
            this.btnCargarImgDomicilio.TabIndex = 15;
            this.btnCargarImgDomicilio.UseVisualStyleBackColor = true;
            this.btnCargarImgDomicilio.Click += new System.EventHandler(this.btnCargarImgDomicilio_Click);
            // 
            // pictureBoxDomicilio
            // 
            this.pictureBoxDomicilio.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(156)))), ((int)(((byte)(0)))));
            this.pictureBoxDomicilio.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBoxDomicilio.Location = new System.Drawing.Point(511, 168);
            this.pictureBoxDomicilio.Name = "pictureBoxDomicilio";
            this.pictureBoxDomicilio.Size = new System.Drawing.Size(175, 105);
            this.pictureBoxDomicilio.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxDomicilio.TabIndex = 46;
            this.pictureBoxDomicilio.TabStop = false;
            // 
            // txtApellidoPaterno
            // 
            this.txtApellidoPaterno.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(207)))), ((int)(((byte)(167)))));
            this.txtApellidoPaterno.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtApellidoPaterno.Font = new System.Drawing.Font("Segoe Print", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtApellidoPaterno.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.txtApellidoPaterno.Location = new System.Drawing.Point(158, 38);
            this.txtApellidoPaterno.Name = "txtApellidoPaterno";
            this.txtApellidoPaterno.Size = new System.Drawing.Size(109, 20);
            this.txtApellidoPaterno.TabIndex = 2;
            this.txtApellidoPaterno.TextChanged += new System.EventHandler(this.textBox24_TextChanged);
            this.txtApellidoPaterno.Leave += new System.EventHandler(this.txtApellidoPaterno_Leave);
            // 
            // txtFraccionamiento
            // 
            this.txtFraccionamiento.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(207)))), ((int)(((byte)(167)))));
            this.txtFraccionamiento.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtFraccionamiento.Font = new System.Drawing.Font("Segoe Print", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFraccionamiento.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.txtFraccionamiento.Location = new System.Drawing.Point(458, 53);
            this.txtFraccionamiento.Name = "txtFraccionamiento";
            this.txtFraccionamiento.Size = new System.Drawing.Size(109, 20);
            this.txtFraccionamiento.TabIndex = 12;
            // 
            // txtNombre
            // 
            this.txtNombre.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(207)))), ((int)(((byte)(167)))));
            this.txtNombre.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtNombre.Font = new System.Drawing.Font("Segoe Print", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNombre.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.txtNombre.Location = new System.Drawing.Point(159, 6);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(109, 20);
            this.txtNombre.TabIndex = 1;
            this.txtNombre.TextChanged += new System.EventHandler(this.textBox20_TextChanged);
            this.txtNombre.Leave += new System.EventHandler(this.txtNombre_Leave);
            // 
            // txtCURP
            // 
            this.txtCURP.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(207)))), ((int)(((byte)(167)))));
            this.txtCURP.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtCURP.Location = new System.Drawing.Point(458, 279);
            this.txtCURP.Name = "txtCURP";
            this.txtCURP.Size = new System.Drawing.Size(161, 13);
            this.txtCURP.TabIndex = 16;
            // 
            // txtNumCasa
            // 
            this.txtNumCasa.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(207)))), ((int)(((byte)(167)))));
            this.txtNumCasa.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtNumCasa.Font = new System.Drawing.Font("Segoe Print", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNumCasa.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.txtNumCasa.Location = new System.Drawing.Point(159, 258);
            this.txtNumCasa.Name = "txtNumCasa";
            this.txtNumCasa.Size = new System.Drawing.Size(109, 20);
            this.txtNumCasa.TabIndex = 9;
            // 
            // txtLocalidad
            // 
            this.txtLocalidad.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(207)))), ((int)(((byte)(167)))));
            this.txtLocalidad.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtLocalidad.Font = new System.Drawing.Font("Segoe Print", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLocalidad.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.txtLocalidad.Location = new System.Drawing.Point(458, 93);
            this.txtLocalidad.Name = "txtLocalidad";
            this.txtLocalidad.Size = new System.Drawing.Size(109, 20);
            this.txtLocalidad.TabIndex = 13;
            // 
            // txtMunicipio
            // 
            this.txtMunicipio.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(207)))), ((int)(((byte)(167)))));
            this.txtMunicipio.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtMunicipio.Font = new System.Drawing.Font("Segoe Print", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMunicipio.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.txtMunicipio.Location = new System.Drawing.Point(458, 133);
            this.txtMunicipio.Name = "txtMunicipio";
            this.txtMunicipio.Size = new System.Drawing.Size(109, 20);
            this.txtMunicipio.TabIndex = 14;
            // 
            // txtCalle
            // 
            this.txtCalle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(207)))), ((int)(((byte)(167)))));
            this.txtCalle.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtCalle.Font = new System.Drawing.Font("Segoe Print", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCalle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.txtCalle.Location = new System.Drawing.Point(158, 229);
            this.txtCalle.Name = "txtCalle";
            this.txtCalle.Size = new System.Drawing.Size(109, 20);
            this.txtCalle.TabIndex = 8;
            // 
            // txtColonia
            // 
            this.txtColonia.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(207)))), ((int)(((byte)(167)))));
            this.txtColonia.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtColonia.Font = new System.Drawing.Font("Segoe Print", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtColonia.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.txtColonia.Location = new System.Drawing.Point(458, 9);
            this.txtColonia.Name = "txtColonia";
            this.txtColonia.Size = new System.Drawing.Size(109, 20);
            this.txtColonia.TabIndex = 11;
            // 
            // txtCelular
            // 
            this.txtCelular.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(207)))), ((int)(((byte)(167)))));
            this.txtCelular.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtCelular.Font = new System.Drawing.Font("Segoe Print", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCelular.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.txtCelular.Location = new System.Drawing.Point(159, 130);
            this.txtCelular.Name = "txtCelular";
            this.txtCelular.Size = new System.Drawing.Size(109, 20);
            this.txtCelular.TabIndex = 5;
            this.txtCelular.Leave += new System.EventHandler(this.txtCelular_Leave);
            // 
            // txtTelefono
            // 
            this.txtTelefono.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(207)))), ((int)(((byte)(167)))));
            this.txtTelefono.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtTelefono.Font = new System.Drawing.Font("Segoe Print", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTelefono.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.txtTelefono.Location = new System.Drawing.Point(159, 162);
            this.txtTelefono.Name = "txtTelefono";
            this.txtTelefono.Size = new System.Drawing.Size(109, 20);
            this.txtTelefono.TabIndex = 6;
            this.txtTelefono.Leave += new System.EventHandler(this.txtTelefono_Leave);
            // 
            // txtCorreoElectronico
            // 
            this.txtCorreoElectronico.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(207)))), ((int)(((byte)(167)))));
            this.txtCorreoElectronico.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtCorreoElectronico.Font = new System.Drawing.Font("Segoe Print", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCorreoElectronico.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.txtCorreoElectronico.Location = new System.Drawing.Point(158, 196);
            this.txtCorreoElectronico.Name = "txtCorreoElectronico";
            this.txtCorreoElectronico.Size = new System.Drawing.Size(109, 20);
            this.txtCorreoElectronico.TabIndex = 7;
            this.txtCorreoElectronico.Leave += new System.EventHandler(this.txtCorreoElectronico_Leave);
            // 
            // txtCP
            // 
            this.txtCP.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(207)))), ((int)(((byte)(167)))));
            this.txtCP.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtCP.Font = new System.Drawing.Font("Segoe Print", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCP.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.txtCP.Location = new System.Drawing.Point(159, 292);
            this.txtCP.Name = "txtCP";
            this.txtCP.Size = new System.Drawing.Size(109, 20);
            this.txtCP.TabIndex = 10;
            this.txtCP.Leave += new System.EventHandler(this.txtCP_Leave);
            // 
            // txtApellidoMaterno
            // 
            this.txtApellidoMaterno.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(207)))), ((int)(((byte)(167)))));
            this.txtApellidoMaterno.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtApellidoMaterno.Font = new System.Drawing.Font("Segoe Print", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtApellidoMaterno.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.txtApellidoMaterno.Location = new System.Drawing.Point(158, 70);
            this.txtApellidoMaterno.Name = "txtApellidoMaterno";
            this.txtApellidoMaterno.Size = new System.Drawing.Size(109, 20);
            this.txtApellidoMaterno.TabIndex = 3;
            this.txtApellidoMaterno.Leave += new System.EventHandler(this.txtApellidoMaterno_Leave);
            // 
            // pictureBoxComproINE
            // 
            this.pictureBoxComproINE.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(176)))), ((int)(((byte)(105)))));
            this.pictureBoxComproINE.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBoxComproINE.Location = new System.Drawing.Point(765, 9);
            this.pictureBoxComproINE.Name = "pictureBoxComproINE";
            this.pictureBoxComproINE.Size = new System.Drawing.Size(176, 97);
            this.pictureBoxComproINE.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxComproINE.TabIndex = 21;
            this.pictureBoxComproINE.TabStop = false;
            // 
            // dtpFechaNacimiento
            // 
            this.dtpFechaNacimiento.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpFechaNacimiento.Location = new System.Drawing.Point(158, 96);
            this.dtpFechaNacimiento.Name = "dtpFechaNacimiento";
            this.dtpFechaNacimiento.Size = new System.Drawing.Size(101, 20);
            this.dtpFechaNacimiento.TabIndex = 4;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Segoe Print", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.label20.Location = new System.Drawing.Point(610, 109);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(162, 23);
            this.label20.TabIndex = 19;
            this.label20.Text = "Comprobante de CURP";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Segoe Print", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.label19.Location = new System.Drawing.Point(321, 276);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(47, 23);
            this.label19.TabIndex = 18;
            this.label19.Text = "CURP";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Segoe Print", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.label17.Location = new System.Drawing.Point(610, 9);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(149, 23);
            this.label17.TabIndex = 16;
            this.label17.Text = "Comprobante de INE";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Segoe Print", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.label16.Location = new System.Drawing.Point(321, 50);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(121, 23);
            this.label16.TabIndex = 15;
            this.label16.Text = "Fraccionamiento";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Segoe Print", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.label15.Location = new System.Drawing.Point(321, 96);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(74, 23);
            this.label15.TabIndex = 14;
            this.label15.Text = "Localidad";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Segoe Print", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.label14.Location = new System.Drawing.Point(3, 162);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(65, 23);
            this.label14.TabIndex = 13;
            this.label14.Text = "Teléfono";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Segoe Print", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.label13.Location = new System.Drawing.Point(4, 127);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(57, 23);
            this.label13.TabIndex = 12;
            this.label13.Text = "Celular";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Segoe Print", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.label12.Location = new System.Drawing.Point(4, 194);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(131, 23);
            this.label12.TabIndex = 11;
            this.label12.Text = "Correo electrónico";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Segoe Print", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.label11.Location = new System.Drawing.Point(3, 67);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(124, 23);
            this.label11.TabIndex = 10;
            this.label11.Text = "Apellido Materno";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Segoe Print", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.label10.Location = new System.Drawing.Point(3, 35);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(121, 23);
            this.label10.TabIndex = 9;
            this.label10.Text = "Apellido Paterno";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Segoe Print", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.label9.Location = new System.Drawing.Point(321, 168);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(184, 23);
            this.label9.TabIndex = 8;
            this.label9.Text = "Comprobante de domicilio";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Segoe Print", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.label8.Location = new System.Drawing.Point(321, 130);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(73, 23);
            this.label8.TabIndex = 7;
            this.label8.Text = "Municipio";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Segoe Print", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.label7.Location = new System.Drawing.Point(4, 229);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(42, 23);
            this.label7.TabIndex = 6;
            this.label7.Text = "Calle";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Segoe Print", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.label6.Location = new System.Drawing.Point(4, 292);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(97, 23);
            this.label6.TabIndex = 5;
            this.label6.Text = "Código postal";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe Print", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.label5.Location = new System.Drawing.Point(321, 6);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(58, 23);
            this.label5.TabIndex = 4;
            this.label5.Text = "Colonia";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe Print", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.label4.Location = new System.Drawing.Point(4, 93);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(148, 23);
            this.label4.TabIndex = 3;
            this.label4.Text = "Fecha de nacimiento";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe Print", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.label3.Location = new System.Drawing.Point(3, 255);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(118, 23);
            this.label3.TabIndex = 2;
            this.label3.Text = "Número de casa";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe Print", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.label2.Location = new System.Drawing.Point(4, 3);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 23);
            this.label2.TabIndex = 1;
            this.label2.Text = "Nombre";
            // 
            // openFileDialogImagenCargada
            // 
            this.openFileDialogImagenCargada.FileName = "openFileDialog1";
            // 
            // errorProviderForm
            // 
            this.errorProviderForm.ContainerControl = this;
            this.errorProviderForm.Icon = ((System.Drawing.Icon)(resources.GetObject("errorProviderForm.Icon")));
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            this.contextMenuStrip1.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip1_Opening);
            // 
            // FrmPersonasAlternativas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(171)))), ((int)(((byte)(39)))));
            this.ClientSize = new System.Drawing.Size(1366, 499);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panelDataGrid);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmPersonasAlternativas";
            this.Text = "FrmPersonasAlternativas";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FrmPersonasAlternativas_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panelDataGrid.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgPersonasAlternativas)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxComproCURP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxDomicilio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxComproINE)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderForm)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panelDataGrid;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.PictureBox pictureBoxComproINE;
        private System.Windows.Forms.DateTimePicker dtpFechaNacimiento;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtApellidoPaterno;
        private System.Windows.Forms.TextBox txtFraccionamiento;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.TextBox txtCURP;
        private System.Windows.Forms.TextBox txtNumCasa;
        private System.Windows.Forms.TextBox txtLocalidad;
        private System.Windows.Forms.TextBox txtMunicipio;
        private System.Windows.Forms.TextBox txtCalle;
        private System.Windows.Forms.TextBox txtColonia;
        private System.Windows.Forms.TextBox txtCelular;
        private System.Windows.Forms.TextBox txtTelefono;
        private System.Windows.Forms.TextBox txtCorreoElectronico;
        private System.Windows.Forms.TextBox txtCP;
        private System.Windows.Forms.TextBox txtApellidoMaterno;
        private System.Windows.Forms.Button btnComproCURPGuardada;
        private System.Windows.Forms.PictureBox pictureBoxComproCURP;
        private System.Windows.Forms.Button btnComproINEGuardada;
        private System.Windows.Forms.Button btnCargarImgDomicilio;
        private System.Windows.Forms.PictureBox pictureBoxDomicilio;
        private System.Windows.Forms.Button btnEditar;
        private System.Windows.Forms.Button btnEliminar;
        private System.Windows.Forms.Button btnLimpiar;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.OpenFileDialog openFileDialogImagenCargada;
        private System.Windows.Forms.Label lblImagendelComproGuardada;
        private System.Windows.Forms.Label lblImagenComproDom;
        private System.Windows.Forms.Label lblImagenComproINE;
        private System.Windows.Forms.Label lblImagenComproINEGuardada;
        private System.Windows.Forms.Label lblImagenComprobanteCURP;
        private System.Windows.Forms.Label lblImagendeComproCURPGuardada;
        private System.Windows.Forms.ErrorProvider errorProviderForm;
        private System.Windows.Forms.DataGridView dgPersonasAlternativas;
        private FontAwesome.Sharp.IconButton iconBtnToHome;
        private FontAwesome.Sharp.IconButton iconBtnCerrarForm;
        private FontAwesome.Sharp.IconButton iconBtnBackToUsers;
        private FontAwesome.Sharp.IconButton iconBtnToProducts;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.Label lblIDPersona;
        private System.Windows.Forms.Label lblIIDdelCliente;
        private System.Windows.Forms.ToolTip toolTipPerAlter;
    }
}