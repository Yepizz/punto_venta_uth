﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib_pdv_uth_v1;
using Lib_pdv_uth_v1.clientes;
using Lib_pdv_uth_v1.Productos;
using Lib_pdv_uth_v1.productos;
using ValidacionesRegEx;
using System.Text.RegularExpressions;
using System.IO;
using Lib_pdv_uth_v1.usuarios;

namespace pdv_uth_v1
{
    public partial class FrmCatalogoProductos : Form
    {
        //vars para acciones CRUD
        private bool nuevo = false;


        Producto borrar = new Producto();
        Producto prod = new Producto();
        int idProducto = 0;

        public FrmCatalogoProductos() 
        {
            InitializeComponent();
        }
        
        

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void FrmCatalogoProductos_Load(object sender, EventArgs e)
        {
            toolTipProductos.SetToolTip(btnAgregar, "Presiona este botón para agregar un producto nuevo.");
            toolTipProductos.SetToolTip(btnEditar, "Presiona este botón para editar este producto.");
            toolTipProductos.SetToolTip(btnEliminar, "Presiona este botón para eliminar permanente este producto.");
            toolTipProductos.SetToolTip(btnCerrar, "Presiona este botón para salir.");
            toolTipProductos.SetToolTip(btnLimpiar, "Presiona este botón para limpiar el formulario.");
            toolTipProductos.SetToolTip(iconBtnBack, "Regresar al anterior formulario");
            toolTipProductos.SetToolTip(btnCargarImagen, "Presiona este botón para cargar una imagén del producto.");
            toolTipProductos.SetToolTip(iconBtnCasa, "Presiona este botón para abrir el menú principal.");
            toolTipProductos.SetToolTip(iconBtnCerrar, "Presiona este botón para cerrar el programa.");
            CrearElDataGrid();
            comboUnidadDeMedida.SelectedItem = comboUnidadDeMedida.Items[0];
        }
        private void mostrarRegistrosEnDG()
        {
            dgProductos.DataSource = null;
            //  borrar todos los ren del DG
            dgProductos.Rows.Clear();
            //se crea lista de criterios de busqueda
            List<CriteriosBusqueda> criterios = new List<CriteriosBusqueda>();
            //se crea objeto de crioterio para busqueda
            CriteriosBusqueda criterio = new CriteriosBusqueda();
            // se asignan los datos del criterio del WHERE
            criterio.campo = " 1 ";
            criterio.operadorIntermedio = OperadorDeConsulta.IGUAL;
            criterio.valor = "1";
            criterio.operadorFinal = OperadorDeConsulta.NINGUNO;
            //se incluye el criterio en la lista de criterios
            criterios.Add(criterio);
            //se ejecuta la consulta y se asigna el resultado al DtaGrid
            dgProductos.DataSource = prod.consultar(criterios);
            // se refresca el dataGrid para mostrar los datos
            dgProductos.Refresh();
        }

      

        public void habilitarForm()
        {
            //habilitar todos los textbox
            checkPerecedero.Enabled = true;
            txtNombre.Enabled = true;
            txtPrecio.Enabled = true;
            txtCodigoBarras.Enabled = true;
            txtDescripcion.Enabled = true;
            // cambiar el color de fondo
            txtNombre.BackColor = Color.LightBlue;
            txtPrecio.BackColor = Color.LightBlue;
            txtCodigoBarras.BackColor = Color.LightBlue;
           
            txtDescripcion.BackColor = Color.LightBlue;
        }
        public void limpiarform()
        {
            //borramos
            txtNombre.Text = "";
            txtPrecio.Text = "";
            txtCodigoBarras.Text = "";
            picBoxCodBarras.Image = null;
            pictureBoxImagenProd.Image = null;
            picBoxCodBarras.Image = null;
            checkPerecedero.Checked = false;
            txtDescripcion.Text = "";


        }


        private void button1_Click(object sender, EventArgs e)
        {
            //GUARDAR
            Producto guardar = new Producto();
            guardar.Nombre = txtNombre.Text;
            guardar.Descripcion = txtDescripcion.Text;
            guardar.Precio = txtPrecio.Text;
            guardar.CodigoDeBarras = txtCodigoBarras.Text;
            guardar.Imagen = lblImagenGuardada.Text;
            guardar.EsPerecedero = checkPerecedero.Checked;
            guardar.UnidadDeMedida = (UnidadDeMedida)Enum.Parse(typeof(UnidadDeMedida), comboUnidadDeMedida.SelectedItem.ToString());

            mostrarRegistrosEnDG();
           
            if (guardar.alta())
            {

                // obtener el dir de la app
                string bin = System.IO.Path.GetDirectoryName(Application.StartupPath);
                //ir a un dir arriba
                string dir = System.IO.Path.GetDirectoryName(bin);
                //agregar el path para guardar la imagen
                dir += "\\Imagenes\\Productos\\";
                //guardar la imagen
                pictureBoxImagenProd.Image.Save(dir + lblImagenGuardada.Text);

                MessageBox.Show("EL producto <" + txtNombre.Text + "> se ha almacenado.", "Nuevo Producto", MessageBoxButtons.OK, MessageBoxIcon.Information);
                mostrarRegistrosEnDG();
                limpiarform();
              
            }
            else
            {
                MessageBox.Show("Error. No se pudo almacenar este producto, Intente de nuevo. " + Producto.msgError);
            }

        }

    
        
        private void btnEditar_Click(object sender, EventArgs e)
        {
            Producto paraModif = new Producto();
            List<DatosParaActualizar> datos = new List<DatosParaActualizar>();
            datos.Add(new DatosParaActualizar("nombre", txtNombre.Text));
            datos.Add(new DatosParaActualizar("descripcion", txtDescripcion.Text));
            datos.Add(new DatosParaActualizar("precio", txtPrecio.Text));
            datos.Add(new DatosParaActualizar("codigo_barras", txtCodigoBarras.Text));
            datos.Add(new DatosParaActualizar("imagen_producto", lblImagenGuardada.Text));
            datos.Add(new DatosParaActualizar("unidad_medida", comboUnidadDeMedida.Text));
            if (checkPerecedero.Checked) datos.Add(new DatosParaActualizar("es_perecedero", "1"));
            else datos.Add(new DatosParaActualizar("es_perecedero", "0"));
            if (paraModif.modificar(datos, idProducto))
            {
                MessageBox.Show("Producto modificado");
            }
            else MessageBox.Show("Error Producto no modificado");

            mostrarRegistrosEnDG();
            dgProductos.Refresh();
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            limpiarform();
        }

        private void btnCerrar_Click_1(object sender, EventArgs e)
        {
           
            //confirmamos boton cerrar
            if (MessageBox.Show("¿Desea Salir?", "Cerrar", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                //cerramos la aplicacion
                this.Close();

            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Realmente quieres eliminar este producto?", "Borrar producto", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                if (borrar.eliminar(idProducto))
                {
                    MessageBox.Show("producto eliminado ");
                    dgProductos.Refresh();
                }
                else MessageBox.Show("Error producto NO eliminado ");
            }
        }

       


        private void btnCargarImagen_Click(object sender, EventArgs e)
        {
            //Filtros para abrir imagen
            openFileDialogImagenACargar.Filter = "Image Files|*.png;";
            openFileDialogImagenACargar.Filter = "Gif Image (.gif)|*.gif|JPEG Image (.jpeg)|*.jpeg|Png Image (.png)|*.png|JPG Image (.jpg)|*.jpg";
            openFileDialogImagenACargar.Title = "Abrir imagen para el producto";
            openFileDialogImagenACargar.DefaultExt = ".png";
            
            if (openFileDialogImagenACargar.ShowDialog() == DialogResult.OK)
            {
                //Tomamos el nombre del archivo
                string fileName = openFileDialogImagenACargar.FileName;
                

                //Cargamos la imagen al PictureBox
                pictureBoxImagenProd.Image = Image.FromFile(openFileDialogImagenACargar.FileName);

                //Tomamos el nombre original de la imagen
                lblImagenProd.Text = openFileDialogImagenACargar.SafeFileName;

             //Se calcula la extencion de la imagen
                string extension = lblImagenProd.Text.Substring(lblImagenProd.Text.Length - 3);

                //si es '.jpeg' va a decir 'peg', asi que acompletamos.
                extension = extension.ToLower() == "peg" ? "jpeg" : extension.ToLower();

               //Creamo el nombre unico para la imagen con DateTime.
                lblImagenGuardada.Text = string.Format(@"prod_{0}." + extension, DateTime.Now.Ticks);
            }
        }

        private void txtCodigoBarras_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                Zen.Barcode.CodeEan13BarcodeDraw barcode = Zen.Barcode.BarcodeDrawFactory.CodeEan13WithChecksum;
                //le quitamos el último caracter al CODBARRAS EAN13( quedará 12)
                string codBarras = txtCodigoBarras.Text.Substring(0, txtCodigoBarras.Text.Length - 1);
                //con la cadena editada, "pintamos" el código de barras en el picBox
                picBoxCodBarras.Image = barcode.Draw(codBarras, 50);
            }
        }

        private void txtNombre_Leave(object sender, EventArgs e)
        {

            if (Validar.nombrePersonal(txtNombre.Text) == false)
            {

                errorProviderForm.SetError(txtNombre, "El nombre debe tener al menos 20 caracteres y no carateres especiales o numeros");

            }
            else
            {
                errorProviderForm.SetError(txtNombre, "");
            }
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void CrearElDataGrid()
        {
 /*           try
            {
                dgProductos.Columns.Add("ColumnId", "Id");
                dgProductos.Columns.Add("ColumnNombre", "Nombre");
                dgProductos.Columns.Add("ColumnDescripcion", "Descripcion");
                dgProductos.Columns.Add("ColumnPrecio", "Precio");
                dgProductos.Columns.Add("ColumnCodigoBarras", "Código de Barras");
                dgProductos.Columns.Add("ColumnImagenDelProducto", "Imagen");
                dgProductos.Columns.Add("ColumnUnidadDeMedida", "Unidad de medida");
                dgProductos.Columns.Add("ColumnPerecedero", "¿Es perecedero?");


                dgProductos.Columns[0].Visible = false;
                dgProductos.Columns[2].Width = 60;
                dgProductos.Columns[5].Width = 60;
                dgProductos.Columns[6].Width = 58;
                dgProductos.Columns[7].Width = 40;
                dgProductos.Columns[8].DefaultCellStyle.BackColor = Color.LawnGreen;

                DataGridViewCellStyle cellSt = new DataGridViewCellStyle();
                cellSt.Alignment = DataGridViewContentAlignment.TopCenter;
                dgProductos.ColumnHeadersDefaultCellStyle = cellSt;
                dgProductos.AllowUserToAddRows = false;
                dgProductos.AllowUserToResizeColumns = false;
                dgProductos.AllowUserToResizeRows = false;
                dgProductos.ReadOnly = false;
                dgProductos.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

            }
            catch (Exception)
            {

                MessageBox.Show("Error", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
    */    }

        private void dgProductos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                idProducto = int.Parse(dgProductos.Rows[e.RowIndex].Cells[0].Value.ToString());
                txtNombre.Text = dgProductos.Rows[e.RowIndex].Cells[1].Value.ToString();
                txtDescripcion.Text = dgProductos.Rows[e.RowIndex].Cells[2].Value.ToString();
                txtPrecio.Text = dgProductos.Rows[e.RowIndex].Cells[3].Value.ToString();
                txtCodigoBarras.Text = dgProductos.Rows[e.RowIndex].Cells[4].Value.ToString();
                lblImagenGuardada.Text = dgProductos.Rows[e.RowIndex].Cells[5].Value.ToString();
                comboUnidadDeMedida.SelectedItem = dgProductos.Rows[e.RowIndex].Cells[6].Value.ToString();
                checkPerecedero.Checked = bool.Parse(dgProductos.Rows[e.RowIndex].Cells[7].Value.ToString());
            }
        }

        private void btnGenerar_Click(object sender, EventArgs e)
        {

        }

        private void iconBtnClose_Click(object sender, EventArgs e)
        {
            this.Hide();
            MenuPrincipal men = new MenuPrincipal();

            men.ShowDialog();
        }

        private void iconBtnBack_Click(object sender, EventArgs e)
        {
            this.Close();

            FormCaja m = new FormCaja();

            m.ShowDialog();
        }

        private void iconBtnCerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgListaProductos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            //if (e.RowIndex > -1)
            //{
            //    idProducto = int.Parse(dgListaProductos.Rows[e.RowIndex].Cells[0].Value.ToString());
            //    txtCodigoBarras.Text = dgListaProductos.Rows[e.RowIndex].Cells[4].Value.ToString();
            //    txtNombre.Text = dgListaProductos.Rows[e.RowIndex].Cells[1].Value.ToString();
            //    txtPrecio.Text = dgListaProductos.Rows[e.RowIndex].Cells[3].Value.ToString();
            //    pictureBoxImagenProd.AccessibleName = dgListaProductos.Rows[e.RowIndex].Cells[5].Value.ToString();
            //    comboUnidadDeMedida.Text = dgListaProductos.Rows[e.RowIndex].Cells[6].Value.ToString();
            //    txtDescripcion.Text = dgListaProductos.Rows[e.RowIndex].Cells[2].Value.ToString();
            //    checkPerecedero.Checked = bool.Parse(dgListaProductos.Rows[e.RowIndex].Cells[7].Value.ToString());
            //}
        }

        private void dgProductos_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void panelCatalogoProductosForm_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}

       