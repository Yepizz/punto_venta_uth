﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AForge;
using AForge.Video.DirectShow;
using ZXing;
using AForge.Video;

namespace pdv_uth_v1
{
    public partial class FrmPruebas : Form
    {
        public FrmPruebas()
        {
            InitializeComponent();
        }
        VideoCaptureDevice videoCaptureDevice;
        FilterInfoCollection filterInfoCollection;
        BarcodeReader reader;

        private void FrmPruebas_Load(object sender, EventArgs e)
        {
            filterInfoCollection = new FilterInfoCollection(FilterCategory.VideoInputDevice);
            foreach (FilterInfo device in filterInfoCollection)
            {
                cboCamera.Items.Add(device.Name);
                cboCamera.SelectedIndex = 0;

            }
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            videoCaptureDevice = new VideoCaptureDevice(filterInfoCollection[cboCamera.SelectedIndex].MonikerString);
            videoCaptureDevice.NewFrame += VideoCaptureDevice_NewFrame;
            videoCaptureDevice.Start();
        }

        private void VideoCaptureDevice_NewFrame(object sender, NewFrameEventArgs eventArgs)
        {
           
            Bitmap bitmap = (Bitmap)eventArgs.Frame.Clone();
            BarcodeReader reader = new BarcodeReader();
          
            var result = reader.Decode(bitmap);
            if (result != null)
            {
                txtCodBarras.Invoke(new MethodInvoker(delegate ()
                {
                   
                    txtCodBarras.Text = result.ToString();
                }));
            }
         
            pictureBoxCam.Image = bitmap;
        }

        

        private void FrmPruebas_FormClosing(object sender, FormClosingEventArgs e)
        {
            if(videoCaptureDevice!=null)

            {
                if (videoCaptureDevice.IsRunning)
                    videoCaptureDevice.Stop();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            txtCodBarras.Text = "";
        }
    }
}
