﻿namespace pdv_uth_v1
{
    partial class FrmCrearTicket
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnPrintTicket = new System.Windows.Forms.Button();
            this.iconbtnAtras = new FontAwesome.Sharp.IconButton();
            this.SuspendLayout();
            // 
            // btnPrintTicket
            // 
            this.btnPrintTicket.Location = new System.Drawing.Point(622, 189);
            this.btnPrintTicket.Name = "btnPrintTicket";
            this.btnPrintTicket.Size = new System.Drawing.Size(139, 69);
            this.btnPrintTicket.TabIndex = 0;
            this.btnPrintTicket.Text = "IMPRIMIR TICKET";
            this.btnPrintTicket.UseVisualStyleBackColor = true;
            this.btnPrintTicket.Click += new System.EventHandler(this.btnPrintTicket_Click);
            // 
            // iconbtnAtras
            // 
            this.iconbtnAtras.FlatAppearance.BorderSize = 0;
            this.iconbtnAtras.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.iconbtnAtras.Flip = FontAwesome.Sharp.FlipOrientation.Horizontal;
            this.iconbtnAtras.IconChar = FontAwesome.Sharp.IconChar.Share;
            this.iconbtnAtras.IconColor = System.Drawing.Color.Red;
            this.iconbtnAtras.IconSize = 75;
            this.iconbtnAtras.Location = new System.Drawing.Point(525, 292);
            this.iconbtnAtras.Name = "iconbtnAtras";
            this.iconbtnAtras.Rotation = 0D;
            this.iconbtnAtras.Size = new System.Drawing.Size(72, 73);
            this.iconbtnAtras.TabIndex = 73;
            this.iconbtnAtras.UseVisualStyleBackColor = true;
            this.iconbtnAtras.Click += new System.EventHandler(this.iconbtnAtras_Click);
            // 
            // FrmCrearTicket
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.iconbtnAtras);
            this.Controls.Add(this.btnPrintTicket);
            this.Name = "FrmCrearTicket";
            this.Text = "FrmCrearTicket";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnPrintTicket;
        private FontAwesome.Sharp.IconButton iconbtnAtras;
    }
}