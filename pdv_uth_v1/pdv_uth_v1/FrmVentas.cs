﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace pdv_uth_v1
{
    public partial class FrmVentas : Form
    {
        private Timer time;

        private void reloj(object obj, EventArgs e)
        {
            DateTime hoy = DateTime.Now;
            lblHora.Text = hoy.ToString("hh:mm:ss tt");

        }
        public FrmVentas()
        {
            time = new Timer();
            time.Tick += new EventHandler(reloj);
            time.Enabled = true;
            
            InitializeComponent();
        }

        private void FrmVentas_Load(object sender, EventArgs e)
        {

        }

        private void iconBtnClose_Click(object sender, EventArgs e)
        {

            MessageBox.Show("¿Quieres devolverte a caja?", "Salir de ventas", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
            this.Hide();

            FormCaja frm = new FormCaja();

            frm.ShowDialog();
        }

        private void iconBtnBack_Click(object sender, EventArgs e)
        {
            this.Hide();

            FormCaja frmo  = new FormCaja();

            frmo.ShowDialog();
        }

        private void iconBtnHome_Click(object sender, EventArgs e)
        {
            this.Hide();

            MenuPrincipal i = new MenuPrincipal();

            i.ShowDialog();
        }
    }
}
