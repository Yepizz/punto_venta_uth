﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AForge;
using AForge.Video.DirectShow;
using ZXing;
using AForge.Video;
using Lib_pdv_uth_v1.cajas;
using Lib_pdv_uth_v1.Productos;
using Lib_pdv_uth_v1.productos;
using Lib_pdv_uth_v1.clientes;
using System.IO;
using System.Drawing.Printing;

namespace pdv_uth_v1
{
    public partial class FormCaja : Form
    {
        public static string msgError = "";
        private string id;
        private string nombre;
        private List<ProductosAVender> listaProductos;
        //instancia de objeto CAJAS
        Caja caja = new Caja();

        protected string Id { get => id; set => id = value; }
        protected string Nombre { get => nombre; set => nombre = value; }
        protected List<ProductosAVender> ListaProductos { get => listaProductos; set => listaProductos = value; }



        public FormCaja()
        {
            InitializeComponent();
        }

        public FormCaja(string id, string nombre, List<ProductosAVender> listaProductos)
        {
            this.Id = id;
            this.Nombre = nombre;
            this.ListaProductos = listaProductos;
        }

        VideoCaptureDevice videoCaptureDevice;
        FilterInfoCollection filterInfoCollection;
        BarcodeReader reader;

        private void iconBtnSalir_Click(object sender, EventArgs e)
        {
            if ((MessageBox.Show("¿Deseas salir de caja?", "¿Salir de caja?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes))
            {
                this.Hide();

                FrmLogin logi = new FrmLogin();

                logi.ShowDialog();
            }
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void iconButton11_Click(object sender, EventArgs e)
        {

        }

        private void iconButton12_Click(object sender, EventArgs e)
        {

        }

        private void FormCaja_Load(object sender, EventArgs e)
        {
            filterInfoCollection = new FilterInfoCollection(FilterCategory.VideoInputDevice);
            foreach (FilterInfo device in filterInfoCollection)
            {
                cboCamera.Items.Add(device.Name);
                cboCamera.SelectedIndex = 0;

            }
        }

        private void iconButtonStart_Click(object sender, EventArgs e)
        {
            videoCaptureDevice = new VideoCaptureDevice(filterInfoCollection[cboCamera.SelectedIndex].MonikerString);
            videoCaptureDevice.NewFrame += VideoCaptureDevice_NewFrame;
            videoCaptureDevice.Start();
        }

        private void VideoCaptureDevice_NewFrame(object sender, NewFrameEventArgs eventArgs)
        {

            Bitmap bitmap = (Bitmap)eventArgs.Frame.Clone();
            BarcodeReader reader = new BarcodeReader();

            var result = reader.Decode(bitmap);
            if (result != null)
            {
                txtCodBarras.Invoke(new MethodInvoker(delegate ()
                {

                    txtCodBarras.Text = result.ToString();
                }));
            }

            pictureBoxCam.Image = bitmap;
        }

        private void FormCaja_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (videoCaptureDevice != null)

            {
                if (videoCaptureDevice.IsRunning)
                    videoCaptureDevice.Stop();
            }
        }

        private void iconBtnInicio_Click(object sender, EventArgs e)
        {
            this.Hide();

            MenuPrincipal menuPpal = new MenuPrincipal();
            menuPpal.ShowDialog();

        }

        private void imprimirTicket(object sender, PrintPageEventArgs e)
        {
            //Obtener el DIR de la app.
            string bin = System.IO.Path.GetDirectoryName(Application.StartupPath);
            //ir a un dir arriba 
            string dir = System.IO.Path.GetDirectoryName(bin);

            int SPACE = 145;
            string title = dir + "\\Logonum3 (2).ico";

            Graphics g = e.Graphics;

            g.DrawRectangle(Pens.Black, 5, 5, 420, 450);

            g.DrawImage(Image.FromFile(title), 50, 7);

            Font fbody = new Font("Segoe Print", 15, FontStyle.Bold);
            Font fbody1 = new Font("Segoe Print", 15, FontStyle.Regular);
            Font rs = new Font("Stencil", 25, FontStyle.Bold);
            Font FTType = new Font("", 150, FontStyle.Bold);
            SolidBrush sb = new SolidBrush(Color.Black);

            g.DrawString("------------------------------------", fbody1, sb, 10, 120);

            g.DrawString("Fecha: ", fbody1, sb, 10, SPACE);
            g.DrawString(DateTime.Now.ToShortDateString(), fbody, sb, 90, SPACE);

            g.DrawString("Time :", fbody, sb, 10, SPACE + 30);
            g.DrawString(DateTime.Now.ToShortTimeString(), fbody1, sb, 90, SPACE + 30);

            g.DrawString("FolioNo.:", fbody, sb, 10, SPACE + 60);
            g.DrawString("00000000001", fbody1, sb, 120, SPACE + 60);

            g.DrawString("Caja:", fbody, sb, 10, SPACE + 90);
            g.DrawString("Caja #1", fbody1, sb, 100, SPACE + 90);

            g.DrawString("Atendió:", fbody, sb, 10, SPACE + 120);
            g.DrawString("ISRAEL YEPIZ VILLA", fbody1, sb, 153, SPACE + 120);

            g.DrawString("--------------------------------------", fbody1, sb, 10, 120);
        }

        public bool vender(int idUsuario, double efectivo, double totalDeVenta)
        {
            //  bool res = false;
            ////valore de venta (gral)
            //string valoresVenta = "(SELECT MAX(folio) + 1 FROM ventas folioVenta)," + this.id+","+idUsuario+",CURDATE(), " + totalDeVenta;
            //if (bd.insertar("ventas", "folio, caja_id, usuario_id, fecha_hora, total_venta", valoresVenta))
            //{
            //    //obtenemos el ID de la VENTA, con datos scalar
            //    int idDeVentaNueva = int.Parse(bd.consultarUnSoloDato("LAST_INSERT_ID()", "ventas", "1").ToString());
            //    //si se puede registral la venta, ahora se registran los productos
            //    foreach (ProductosAVender prod in this.listaProductos)
            //    {
            //        //sacamos los valores de cada ProductoAVender... de la lista
            //        string valoresDetalle = idDeVentaNueva + "," + prod.idProducto + "," + prod.cantidad;
            //        //por cada producto insertamos en ventas detalle
            //        if (bd.insertar("ventas_detalles", "venta_id, producto_id, cantidad", valoresDetalle) == false)
            //        {
            //            //hubo error al insertar
            //            res = false;
            //            msgError = "Error al insertar en 'ventas_detalles' <" + idDeVentaNueva + "," + prod.idProducto + "," + prod.cantidad + ">";
            //            break;//romper el forEach
            //        }//insertar detalles ventas
            //        else res = true;// si se insertan los detalles de venta yyyyyyy la venta
            //    }//por cada producto a vender de la venta registrando
            //}//insertar DATOS GNERALES DE  venta 
            //else
            //{
            //    res = false;
            //    msgError = "Error al registrar los DATOS GENERALES DE LA VENTA. "+msgError;
            //}
            //return res;
            return true;

        }

        private void iconBtnVentas_Click(object sender, EventArgs e)
        {
            this.Hide();

            FrmVentas frmventas = new FrmVentas();

            frmventas.ShowDialog();
        }

        private void iconBtnCreditos_Click(object sender, EventArgs e)
        {
            this.Hide();

            FrmCreditos frCreditos = new FrmCreditos();

            frCreditos.ShowDialog();
        }

        private void iconBtnProductos_Click(object sender, EventArgs e)
        {
            this.Hide();

            FrmCatalogoProductos pro = new FrmCatalogoProductos();

            pro.ShowDialog();
        }

        private void iconBtnPagar_Click(object sender, EventArgs e)
        {

            if (caja.vender(1, double.Parse(txtEfectivo.Text), double.Parse(txtTotal.Text)))
            {
                MessageBox.Show("¡La venta se ha registrado correctamente!, y tu cambio es de : " + txtCambio.Text);
                limpiarform();


            }
            else MessageBox.Show("Error en el registro de la venta. " + Caja.msgError);
        }

        public void limpiarform()
        {
            //borramos
            txtIva.Text = "";
            txtSubTotal.Text = "";
            txtTotal.Text = "";
            txtCambio.Text = "";
            txtEfectivo.Text = "";

        }
        private void iconBtnCancelarOperacion_Click(object sender, EventArgs e)
        {

        }

        private void iconBtnCancelarOperacion_Click_1(object sender, EventArgs e)
        {
            //salir ????
            if (MessageBox.Show("¿Desea cerrar la caja?",
                                "Salir",
                                MessageBoxButtons.YesNo,
                                MessageBoxIcon.Question) == DialogResult.Yes)
            {
                this.Close();
            }
        }

        private void dgProductos_Layout(object sender, LayoutEventArgs e)
        {
            if (dgProductos.RowCount > 0)
            {
                foreach (DataGridViewRow row in dgProductos.Rows)
                {
                    row.HeaderCell.Value = (row.Index + 1).ToString();
                }
            }
        }

        private void iconBtnAdd_Click(object sender, EventArgs e)
        {
            Producto prod = new Producto();
            prod = prod.consultarPorCodigoDeBarras(txtCodBarras.Text);

            //agregamos un renglon con la info del producto CAPTURADo al DG
            if (prod == null)
            {

                //producto NO EXISTE
                MessageBox.Show("El producto con el Código de Barras <" + txtCodBarras.Text + ">, no Existe. ", "No encontrado", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //cursor a CodBarras y select all
                txtCodBarras.Focus();
                txtCodBarras.SelectAll();

            }
            else
            {
                //pasar producto obj a obj[]
                //agregar el producto al DataGrid
                dgProductos.Rows.Add(new object[] { prod.Id, prod.CodigoDeBarras, prod.Nombre, prod.Descripcion, prod.Precio, numericCantidad.Value, (double.Parse(prod.Precio) * double.Parse(numericCantidad.Value.ToString())) });
                //agregar el produ a caja.ListaProductos
                caja.ListaProductos.Add(new ProductosAVender(prod.Id, int.Parse(numericCantidad.Value.ToString()), prod.CodigoDeBarras));
                //limpiamos los text
                txtCodBarras.Clear();
                numericCantidad.Value = 1;

            }
            double temp = 0;
            for (int i = 0; i < dgProductos.RowCount - 1; i++)
            {
                temp = temp +
                    double.Parse(dgProductos.Rows[i].Cells[dgProductos.ColumnCount - 1].Value.ToString());
            }
            txtTotal.Text = temp.ToString();
            txtSubTotal.Text = (temp * 0.84).ToString();
            txtIva.Text = (temp * 0.16).ToString();

        }

        private void dgProductos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                int.Parse(dgProductos.Rows[e.RowIndex].Cells[0].Value.ToString());
                txtCodBarras.Text = dgProductos.Rows[e.RowIndex].Cells[4].Value.ToString();
                dgProductos.Rows[e.RowIndex].Cells[1].Value.ToString();
                dgProductos.Rows[e.RowIndex].Cells[3].Value.ToString();
                dgProductos.Rows[e.RowIndex].Cells[5].Value.ToString();
                dgProductos.Rows[e.RowIndex].Cells[6].Value.ToString();
                dgProductos.Rows[e.RowIndex].Cells[2].Value.ToString();
                bool.Parse(dgProductos.Rows[e.RowIndex].Cells[7].Value.ToString());

                dgProductos.Refresh();
              
            }
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void txtEfectivo_MouseLeave(object sender, EventArgs e)
        {
            double cambio = double.Parse(txtEfectivo.Text.ToString()) - double.Parse(txtTotal.Text.ToString());
            txtCambio.Text = cambio.ToString();
        }

        private void iconbtnTicket_Click(object sender, EventArgs e)
        {
            PrintDocument pd = new PrintDocument();
            PaperSize ps = new PaperSize("", 475, 550);

            pd.PrintPage += new PrintPageEventHandler(imprimirTicket);

            pd.PrintController = new StandardPrintController();
            pd.DefaultPageSettings.Margins.Left = 0;
            pd.DefaultPageSettings.Margins.Right = 0;
            pd.DefaultPageSettings.Margins.Top = 0;
            pd.DefaultPageSettings.Margins.Bottom = 0;


            pd.DefaultPageSettings.PaperSize = ps;
            pd.Print();
        }

        private void label12_Click(object sender, EventArgs e)
        {

        }
    }
}
