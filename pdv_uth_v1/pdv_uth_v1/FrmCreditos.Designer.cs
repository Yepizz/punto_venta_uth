﻿namespace pdv_uth_v1
{
    partial class FrmCreditos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmCreditos));
            this.panel1 = new System.Windows.Forms.Panel();
            this.iconBtnClose = new FontAwesome.Sharp.IconButton();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dgCreditos = new System.Windows.Forms.DataGridView();
            this.iconBtnBack = new FontAwesome.Sharp.IconButton();
            this.iconBtnClock = new FontAwesome.Sharp.IconButton();
            this.lblHora = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.lblIDcliente = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.dtpFechaApertura = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.dtpFechaSaldado = new System.Windows.Forms.DateTimePicker();
            this.iconBtnRegresar = new FontAwesome.Sharp.IconButton();
            this.iconButton1 = new FontAwesome.Sharp.IconButton();
            this.iconButton2 = new FontAwesome.Sharp.IconButton();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgCreditos)).BeginInit();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(176)))), ((int)(((byte)(105)))));
            this.panel1.Controls.Add(this.iconBtnClose);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1043, 63);
            this.panel1.TabIndex = 0;
            // 
            // iconBtnClose
            // 
            this.iconBtnClose.Dock = System.Windows.Forms.DockStyle.Right;
            this.iconBtnClose.FlatAppearance.BorderSize = 0;
            this.iconBtnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.iconBtnClose.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.iconBtnClose.IconChar = FontAwesome.Sharp.IconChar.PowerOff;
            this.iconBtnClose.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.iconBtnClose.IconSize = 65;
            this.iconBtnClose.ImageAlign = System.Drawing.ContentAlignment.TopRight;
            this.iconBtnClose.Location = new System.Drawing.Point(943, 0);
            this.iconBtnClose.Name = "iconBtnClose";
            this.iconBtnClose.Rotation = 0D;
            this.iconBtnClose.Size = new System.Drawing.Size(100, 63);
            this.iconBtnClose.TabIndex = 1;
            this.iconBtnClose.UseVisualStyleBackColor = true;
            this.iconBtnClose.Click += new System.EventHandler(this.iconBtnClose_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe Print", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.label1.Location = new System.Drawing.Point(3, 2);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(715, 51);
            this.label1.TabIndex = 0;
            this.label1.Text = "INFORMACIÓN DE CRÉDITOS PARA CLIENTES";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.dgCreditos);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(0, 63);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(521, 491);
            this.panel2.TabIndex = 1;
            // 
            // dgCreditos
            // 
            this.dgCreditos.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(189)))), ((int)(((byte)(84)))));
            this.dgCreditos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgCreditos.Dock = System.Windows.Forms.DockStyle.Left;
            this.dgCreditos.Location = new System.Drawing.Point(0, 0);
            this.dgCreditos.Name = "dgCreditos";
            this.dgCreditos.Size = new System.Drawing.Size(521, 491);
            this.dgCreditos.TabIndex = 0;
            // 
            // iconBtnBack
            // 
            this.iconBtnBack.Dock = System.Windows.Forms.DockStyle.Right;
            this.iconBtnBack.FlatAppearance.BorderSize = 0;
            this.iconBtnBack.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.iconBtnBack.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.iconBtnBack.IconChar = FontAwesome.Sharp.IconChar.ArrowCircleLeft;
            this.iconBtnBack.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(141)))), ((int)(((byte)(84)))));
            this.iconBtnBack.IconSize = 75;
            this.iconBtnBack.Location = new System.Drawing.Point(451, 0);
            this.iconBtnBack.Name = "iconBtnBack";
            this.iconBtnBack.Rotation = 0D;
            this.iconBtnBack.Size = new System.Drawing.Size(71, 91);
            this.iconBtnBack.TabIndex = 2;
            this.iconBtnBack.UseVisualStyleBackColor = true;
            this.iconBtnBack.Click += new System.EventHandler(this.iconBtnBack_Click);
            // 
            // iconBtnClock
            // 
            this.iconBtnClock.Dock = System.Windows.Forms.DockStyle.Left;
            this.iconBtnClock.FlatAppearance.BorderSize = 0;
            this.iconBtnClock.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.iconBtnClock.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.iconBtnClock.IconChar = FontAwesome.Sharp.IconChar.Clock;
            this.iconBtnClock.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(141)))), ((int)(((byte)(84)))));
            this.iconBtnClock.IconSize = 75;
            this.iconBtnClock.Location = new System.Drawing.Point(0, 0);
            this.iconBtnClock.Name = "iconBtnClock";
            this.iconBtnClock.Rotation = 0D;
            this.iconBtnClock.Size = new System.Drawing.Size(71, 91);
            this.iconBtnClock.TabIndex = 3;
            this.iconBtnClock.UseVisualStyleBackColor = true;
            // 
            // lblHora
            // 
            this.lblHora.AutoSize = true;
            this.lblHora.Font = new System.Drawing.Font("Segoe Print", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHora.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(156)))), ((int)(((byte)(0)))));
            this.lblHora.Location = new System.Drawing.Point(89, 23);
            this.lblHora.Name = "lblHora";
            this.lblHora.Size = new System.Drawing.Size(0, 37);
            this.lblHora.TabIndex = 4;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(207)))), ((int)(((byte)(167)))));
            this.panel3.Controls.Add(this.iconBtnClock);
            this.panel3.Controls.Add(this.iconBtnBack);
            this.panel3.Controls.Add(this.lblHora);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(521, 463);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(522, 91);
            this.panel3.TabIndex = 5;
            // 
            // lblIDcliente
            // 
            this.lblIDcliente.AutoSize = true;
            this.lblIDcliente.Font = new System.Drawing.Font("Segoe Print", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIDcliente.Location = new System.Drawing.Point(527, 67);
            this.lblIDcliente.Name = "lblIDcliente";
            this.lblIDcliente.Size = new System.Drawing.Size(148, 28);
            this.lblIDcliente.TabIndex = 6;
            this.lblIDcliente.Text = "ID DEL CLIENTE";
            this.lblIDcliente.Click += new System.EventHandler(this.lblIDcliente_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(604, 131);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 20);
            this.textBox1.TabIndex = 7;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe Print", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(583, 95);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(144, 28);
            this.label2.TabIndex = 8;
            this.label2.Text = "Saldo del cliente";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe Print", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(583, 174);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(159, 28);
            this.label3.TabIndex = 9;
            this.label3.Text = "Fecha de Apertura";
            // 
            // dtpFechaApertura
            // 
            this.dtpFechaApertura.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpFechaApertura.Location = new System.Drawing.Point(604, 217);
            this.dtpFechaApertura.Name = "dtpFechaApertura";
            this.dtpFechaApertura.Size = new System.Drawing.Size(100, 20);
            this.dtpFechaApertura.TabIndex = 10;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe Print", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(594, 271);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(123, 28);
            this.label5.TabIndex = 13;
            this.label5.Text = "Fecha saldado";
            // 
            // dtpFechaSaldado
            // 
            this.dtpFechaSaldado.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpFechaSaldado.Location = new System.Drawing.Point(599, 322);
            this.dtpFechaSaldado.Name = "dtpFechaSaldado";
            this.dtpFechaSaldado.Size = new System.Drawing.Size(100, 20);
            this.dtpFechaSaldado.TabIndex = 14;
            // 
            // iconBtnRegresar
            // 
            this.iconBtnRegresar.FlatAppearance.BorderSize = 0;
            this.iconBtnRegresar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.iconBtnRegresar.Flip = FontAwesome.Sharp.FlipOrientation.Horizontal;
            this.iconBtnRegresar.IconChar = FontAwesome.Sharp.IconChar.Share;
            this.iconBtnRegresar.IconColor = System.Drawing.Color.Red;
            this.iconBtnRegresar.IconSize = 75;
            this.iconBtnRegresar.Location = new System.Drawing.Point(877, 365);
            this.iconBtnRegresar.Name = "iconBtnRegresar";
            this.iconBtnRegresar.Rotation = 0D;
            this.iconBtnRegresar.Size = new System.Drawing.Size(72, 73);
            this.iconBtnRegresar.TabIndex = 73;
            this.iconBtnRegresar.UseVisualStyleBackColor = true;
            this.iconBtnRegresar.Click += new System.EventHandler(this.iconBtnRegresar_Click);
            // 
            // iconButton1
            // 
            this.iconButton1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.iconButton1.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.iconButton1.IconChar = FontAwesome.Sharp.IconChar.Edit;
            this.iconButton1.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.iconButton1.IconSize = 55;
            this.iconButton1.Location = new System.Drawing.Point(678, 407);
            this.iconButton1.Name = "iconButton1";
            this.iconButton1.Rotation = 0D;
            this.iconButton1.Size = new System.Drawing.Size(64, 57);
            this.iconButton1.TabIndex = 74;
            this.iconButton1.UseVisualStyleBackColor = true;
            // 
            // iconButton2
            // 
            this.iconButton2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.iconButton2.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.iconButton2.IconChar = FontAwesome.Sharp.IconChar.PlusCircle;
            this.iconButton2.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(207)))), ((int)(((byte)(167)))));
            this.iconButton2.IconSize = 55;
            this.iconButton2.Location = new System.Drawing.Point(562, 405);
            this.iconButton2.Name = "iconButton2";
            this.iconButton2.Rotation = 0D;
            this.iconButton2.Size = new System.Drawing.Size(66, 59);
            this.iconButton2.TabIndex = 75;
            this.iconButton2.UseVisualStyleBackColor = true;
            // 
            // FrmCreditos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(171)))), ((int)(((byte)(39)))));
            this.ClientSize = new System.Drawing.Size(1043, 554);
            this.Controls.Add(this.iconButton2);
            this.Controls.Add(this.iconButton1);
            this.Controls.Add(this.iconBtnRegresar);
            this.Controls.Add(this.dtpFechaSaldado);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.dtpFechaApertura);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.lblIDcliente);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmCreditos";
            this.Text = "FrmCreditos";
            this.Load += new System.EventHandler(this.FrmCreditos_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgCreditos)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private FontAwesome.Sharp.IconButton iconBtnClose;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridView dgCreditos;
        private FontAwesome.Sharp.IconButton iconBtnBack;
        private FontAwesome.Sharp.IconButton iconBtnClock;
        private System.Windows.Forms.Label lblHora;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label lblIDcliente;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker dtpFechaApertura;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker dtpFechaSaldado;
        private FontAwesome.Sharp.IconButton iconBtnRegresar;
        private FontAwesome.Sharp.IconButton iconButton1;
        private FontAwesome.Sharp.IconButton iconButton2;
    }
}