﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace pdv_uth_v1
{
    public partial class FrmCreditos : Form
    {
        private Timer time;

        private void reloj(object obj, EventArgs e)
        {
            DateTime hoy = DateTime.Now;
            lblHora.Text = hoy.ToString("hh:mm:ss tt");

        }
        public FrmCreditos()
        {
            time = new Timer();
            time.Tick+= new EventHandler(reloj);
            time.Enabled = true;
            InitializeComponent();
        }

        private void FrmCreditos_Load(object sender, EventArgs e)
        {

        }

        private void iconBtnClose_Click(object sender, EventArgs e)
        {
            if((MessageBox.Show("¿Deseas salir de caja?", "¿Cerrar caja?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes))
            {
                this.Hide();

                FormCaja cat = new FormCaja();

                cat.ShowDialog();
            }
        }

        private void iconBtnBack_Click(object sender, EventArgs e)
        {
            this.Hide();

            this.Hide();

            FormCaja caj = new FormCaja();

            caj.ShowDialog();

        }

        private void lblIDcliente_Click(object sender, EventArgs e)
        {
            lblIDcliente.Text = FrmCatalogoCliente.idParaPersona.ToString();
        }

        private void iconBtnRegresar_Click(object sender, EventArgs e)
        {
            this.Hide();

            FrmCatalogoCliente cll = new FrmCatalogoCliente();

            cll.ShowDialog();
        }
    }
}
