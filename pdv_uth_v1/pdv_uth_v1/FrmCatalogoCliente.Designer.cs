﻿namespace pdv_uth_v1
{
    partial class FrmCatalogoCliente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmCatalogoCliente));
            this.panelFormu = new System.Windows.Forms.Panel();
            this.iconBtnParaID = new FontAwesome.Sharp.IconButton();
            this.lblUsuario = new System.Windows.Forms.Label();
            this.iconBtnBackToHome = new FontAwesome.Sharp.IconButton();
            this.iconBtnIngresarAUsuarios = new FontAwesome.Sharp.IconButton();
            this.btnComproCURPGuardada = new System.Windows.Forms.Button();
            this.pictureBoxComproCURP = new System.Windows.Forms.PictureBox();
            this.lblImagendeComproCURPGuardada = new System.Windows.Forms.Label();
            this.lblImagenComprobanteCURP = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.btnComproINEGuardada = new System.Windows.Forms.Button();
            this.btnCargarImgDomicilio = new System.Windows.Forms.Button();
            this.pictureBoxComproINE = new System.Windows.Forms.PictureBox();
            this.lblImagenComproINEGuardada = new System.Windows.Forms.Label();
            this.lblImagenComproINE = new System.Windows.Forms.Label();
            this.pictureBoxDomicilio = new System.Windows.Forms.PictureBox();
            this.lblImagendelComproGuardada = new System.Windows.Forms.Label();
            this.lblImagenComproDom = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnCerrarTodo = new System.Windows.Forms.Button();
            this.txtFraccionamiento = new System.Windows.Forms.TextBox();
            this.lblFraccionamiento = new System.Windows.Forms.Label();
            this.txtTelefono = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtCurp = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.btnModificar = new System.Windows.Forms.Button();
            this.btnBorrar = new System.Windows.Forms.Button();
            this.btnLimpiar = new System.Windows.Forms.Button();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.dtpFechaNacimiento = new System.Windows.Forms.DateTimePicker();
            this.label15 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtLocalidad = new System.Windows.Forms.TextBox();
            this.txtMunicipio = new System.Windows.Forms.TextBox();
            this.txtCalle = new System.Windows.Forms.TextBox();
            this.txtCP = new System.Windows.Forms.TextBox();
            this.txtCorreo = new System.Windows.Forms.TextBox();
            this.txtNumCalle = new System.Windows.Forms.TextBox();
            this.txtColonia = new System.Windows.Forms.TextBox();
            this.txtApMat = new System.Windows.Forms.TextBox();
            this.txtApPat = new System.Windows.Forms.TextBox();
            this.txtCelular = new System.Windows.Forms.TextBox();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panelAcciones = new System.Windows.Forms.Panel();
            this.panelDataGrid = new System.Windows.Forms.Panel();
            this.dgCliente = new System.Windows.Forms.DataGridView();
            this.saveFDComprobantes = new System.Windows.Forms.SaveFileDialog();
            this.errorProviderForm = new System.Windows.Forms.ErrorProvider(this.components);
            this.openFileDialogImagenACargar = new System.Windows.Forms.OpenFileDialog();
            this.toolTipClientes = new System.Windows.Forms.ToolTip(this.components);
            this.panelFormu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxComproCURP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxComproINE)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxDomicilio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panelDataGrid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgCliente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderForm)).BeginInit();
            this.SuspendLayout();
            // 
            // panelFormu
            // 
            this.panelFormu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(176)))), ((int)(((byte)(105)))));
            this.panelFormu.Controls.Add(this.iconBtnParaID);
            this.panelFormu.Controls.Add(this.lblUsuario);
            this.panelFormu.Controls.Add(this.iconBtnBackToHome);
            this.panelFormu.Controls.Add(this.iconBtnIngresarAUsuarios);
            this.panelFormu.Controls.Add(this.btnComproCURPGuardada);
            this.panelFormu.Controls.Add(this.pictureBoxComproCURP);
            this.panelFormu.Controls.Add(this.lblImagendeComproCURPGuardada);
            this.panelFormu.Controls.Add(this.lblImagenComprobanteCURP);
            this.panelFormu.Controls.Add(this.label17);
            this.panelFormu.Controls.Add(this.btnComproINEGuardada);
            this.panelFormu.Controls.Add(this.btnCargarImgDomicilio);
            this.panelFormu.Controls.Add(this.pictureBoxComproINE);
            this.panelFormu.Controls.Add(this.lblImagenComproINEGuardada);
            this.panelFormu.Controls.Add(this.lblImagenComproINE);
            this.panelFormu.Controls.Add(this.pictureBoxDomicilio);
            this.panelFormu.Controls.Add(this.lblImagendelComproGuardada);
            this.panelFormu.Controls.Add(this.lblImagenComproDom);
            this.panelFormu.Controls.Add(this.pictureBox1);
            this.panelFormu.Controls.Add(this.btnCerrarTodo);
            this.panelFormu.Controls.Add(this.txtFraccionamiento);
            this.panelFormu.Controls.Add(this.lblFraccionamiento);
            this.panelFormu.Controls.Add(this.txtTelefono);
            this.panelFormu.Controls.Add(this.label16);
            this.panelFormu.Controls.Add(this.txtCurp);
            this.panelFormu.Controls.Add(this.label14);
            this.panelFormu.Controls.Add(this.btnModificar);
            this.panelFormu.Controls.Add(this.btnBorrar);
            this.panelFormu.Controls.Add(this.btnLimpiar);
            this.panelFormu.Controls.Add(this.btnGuardar);
            this.panelFormu.Controls.Add(this.label11);
            this.panelFormu.Controls.Add(this.dtpFechaNacimiento);
            this.panelFormu.Controls.Add(this.label15);
            this.panelFormu.Controls.Add(this.label13);
            this.panelFormu.Controls.Add(this.label12);
            this.panelFormu.Controls.Add(this.label10);
            this.panelFormu.Controls.Add(this.txtLocalidad);
            this.panelFormu.Controls.Add(this.txtMunicipio);
            this.panelFormu.Controls.Add(this.txtCalle);
            this.panelFormu.Controls.Add(this.txtCP);
            this.panelFormu.Controls.Add(this.txtCorreo);
            this.panelFormu.Controls.Add(this.txtNumCalle);
            this.panelFormu.Controls.Add(this.txtColonia);
            this.panelFormu.Controls.Add(this.txtApMat);
            this.panelFormu.Controls.Add(this.txtApPat);
            this.panelFormu.Controls.Add(this.txtCelular);
            this.panelFormu.Controls.Add(this.txtNombre);
            this.panelFormu.Controls.Add(this.label9);
            this.panelFormu.Controls.Add(this.label8);
            this.panelFormu.Controls.Add(this.label7);
            this.panelFormu.Controls.Add(this.label6);
            this.panelFormu.Controls.Add(this.label5);
            this.panelFormu.Controls.Add(this.label4);
            this.panelFormu.Controls.Add(this.label3);
            this.panelFormu.Controls.Add(this.label2);
            this.panelFormu.Controls.Add(this.label1);
            this.panelFormu.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelFormu.Font = new System.Drawing.Font("Segoe Print", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panelFormu.Location = new System.Drawing.Point(0, 0);
            this.panelFormu.Name = "panelFormu";
            this.panelFormu.Size = new System.Drawing.Size(1366, 353);
            this.panelFormu.TabIndex = 0;
            // 
            // iconBtnParaID
            // 
            this.iconBtnParaID.FlatAppearance.BorderSize = 0;
            this.iconBtnParaID.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.iconBtnParaID.Flip = FontAwesome.Sharp.FlipOrientation.Vertical;
            this.iconBtnParaID.IconChar = FontAwesome.Sharp.IconChar.Share;
            this.iconBtnParaID.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(156)))), ((int)(((byte)(0)))));
            this.iconBtnParaID.IconSize = 48;
            this.iconBtnParaID.Location = new System.Drawing.Point(1282, 289);
            this.iconBtnParaID.Name = "iconBtnParaID";
            this.iconBtnParaID.Rotation = 0D;
            this.iconBtnParaID.Size = new System.Drawing.Size(54, 41);
            this.iconBtnParaID.TabIndex = 74;
            this.iconBtnParaID.UseVisualStyleBackColor = true;
            this.iconBtnParaID.Click += new System.EventHandler(this.iconBtnParaID_Click);
            // 
            // lblUsuario
            // 
            this.lblUsuario.AutoSize = true;
            this.lblUsuario.Location = new System.Drawing.Point(340, 75);
            this.lblUsuario.Name = "lblUsuario";
            this.lblUsuario.Size = new System.Drawing.Size(117, 19);
            this.lblUsuario.TabIndex = 73;
            this.lblUsuario.Text = "Nombre del usuario";
            // 
            // iconBtnBackToHome
            // 
            this.iconBtnBackToHome.FlatAppearance.BorderSize = 0;
            this.iconBtnBackToHome.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.iconBtnBackToHome.Flip = FontAwesome.Sharp.FlipOrientation.Horizontal;
            this.iconBtnBackToHome.IconChar = FontAwesome.Sharp.IconChar.Share;
            this.iconBtnBackToHome.IconColor = System.Drawing.Color.Red;
            this.iconBtnBackToHome.IconSize = 75;
            this.iconBtnBackToHome.Location = new System.Drawing.Point(942, 198);
            this.iconBtnBackToHome.Name = "iconBtnBackToHome";
            this.iconBtnBackToHome.Rotation = 0D;
            this.iconBtnBackToHome.Size = new System.Drawing.Size(72, 73);
            this.iconBtnBackToHome.TabIndex = 72;
            this.iconBtnBackToHome.UseVisualStyleBackColor = true;
            this.iconBtnBackToHome.Click += new System.EventHandler(this.iconBtnBackToHome_Click);
            // 
            // iconBtnIngresarAUsuarios
            // 
            this.iconBtnIngresarAUsuarios.FlatAppearance.BorderSize = 0;
            this.iconBtnIngresarAUsuarios.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.iconBtnIngresarAUsuarios.Flip = FontAwesome.Sharp.FlipOrientation.Vertical;
            this.iconBtnIngresarAUsuarios.IconChar = FontAwesome.Sharp.IconChar.Share;
            this.iconBtnIngresarAUsuarios.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(156)))), ((int)(((byte)(0)))));
            this.iconBtnIngresarAUsuarios.IconSize = 75;
            this.iconBtnIngresarAUsuarios.Location = new System.Drawing.Point(1281, 198);
            this.iconBtnIngresarAUsuarios.Name = "iconBtnIngresarAUsuarios";
            this.iconBtnIngresarAUsuarios.Rotation = 0D;
            this.iconBtnIngresarAUsuarios.Size = new System.Drawing.Size(72, 73);
            this.iconBtnIngresarAUsuarios.TabIndex = 71;
            this.iconBtnIngresarAUsuarios.UseVisualStyleBackColor = true;
            this.iconBtnIngresarAUsuarios.Click += new System.EventHandler(this.iconBtnIngresarAUsuarios_Click);
            // 
            // btnComproCURPGuardada
            // 
            this.btnComproCURPGuardada.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(189)))), ((int)(((byte)(84)))));
            this.btnComproCURPGuardada.FlatAppearance.BorderSize = 30;
            this.btnComproCURPGuardada.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnComproCURPGuardada.Image = global::pdv_uth_v1.Properties.Resources.upload_information_16316__1_;
            this.btnComproCURPGuardada.Location = new System.Drawing.Point(772, 238);
            this.btnComproCURPGuardada.Name = "btnComproCURPGuardada";
            this.btnComproCURPGuardada.Size = new System.Drawing.Size(50, 49);
            this.btnComproCURPGuardada.TabIndex = 70;
            this.btnComproCURPGuardada.UseVisualStyleBackColor = false;
            this.btnComproCURPGuardada.Click += new System.EventHandler(this.btnComproCURPGuardada_Click);
            // 
            // pictureBoxComproCURP
            // 
            this.pictureBoxComproCURP.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(171)))), ((int)(((byte)(39)))));
            this.pictureBoxComproCURP.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBoxComproCURP.Location = new System.Drawing.Point(562, 204);
            this.pictureBoxComproCURP.Name = "pictureBoxComproCURP";
            this.pictureBoxComproCURP.Size = new System.Drawing.Size(204, 88);
            this.pictureBoxComproCURP.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxComproCURP.TabIndex = 69;
            this.pictureBoxComproCURP.TabStop = false;
            // 
            // lblImagendeComproCURPGuardada
            // 
            this.lblImagendeComproCURPGuardada.AutoSize = true;
            this.lblImagendeComproCURPGuardada.Font = new System.Drawing.Font("Segoe Print", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblImagendeComproCURPGuardada.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(13)))), ((int)(((byte)(121)))));
            this.lblImagendeComproCURPGuardada.Location = new System.Drawing.Point(351, 198);
            this.lblImagendeComproCURPGuardada.Name = "lblImagendeComproCURPGuardada";
            this.lblImagendeComproCURPGuardada.Size = new System.Drawing.Size(97, 12);
            this.lblImagendeComproCURPGuardada.TabIndex = 68;
            this.lblImagendeComproCURPGuardada.Text = "Imagen del Comprobante";
            this.lblImagendeComproCURPGuardada.Visible = false;
            // 
            // lblImagenComprobanteCURP
            // 
            this.lblImagenComprobanteCURP.AutoSize = true;
            this.lblImagenComprobanteCURP.Font = new System.Drawing.Font("Segoe Print", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblImagenComprobanteCURP.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(13)))), ((int)(((byte)(121)))));
            this.lblImagenComprobanteCURP.Location = new System.Drawing.Point(351, 210);
            this.lblImagenComprobanteCURP.Name = "lblImagenComprobanteCURP";
            this.lblImagenComprobanteCURP.Size = new System.Drawing.Size(97, 12);
            this.lblImagenComprobanteCURP.TabIndex = 67;
            this.lblImagenComprobanteCURP.Text = "Imagen del Comprobante";
            this.lblImagenComprobanteCURP.Visible = false;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Segoe Print", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.label17.Location = new System.Drawing.Point(328, 198);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(197, 28);
            this.label17.TabIndex = 66;
            this.label17.Text = "Comprobante de CURP";
            // 
            // btnComproINEGuardada
            // 
            this.btnComproINEGuardada.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(176)))), ((int)(((byte)(105)))));
            this.btnComproINEGuardada.FlatAppearance.BorderSize = 30;
            this.btnComproINEGuardada.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnComproINEGuardada.Image = global::pdv_uth_v1.Properties.Resources._1486505253_folder_folder_up_folder_upload_update_folder_upload_81427__1_;
            this.btnComproINEGuardada.Location = new System.Drawing.Point(772, 145);
            this.btnComproINEGuardada.Name = "btnComproINEGuardada";
            this.btnComproINEGuardada.Size = new System.Drawing.Size(50, 47);
            this.btnComproINEGuardada.TabIndex = 65;
            this.btnComproINEGuardada.UseVisualStyleBackColor = false;
            this.btnComproINEGuardada.Click += new System.EventHandler(this.btnComproINEGuardada_Click);
            // 
            // btnCargarImgDomicilio
            // 
            this.btnCargarImgDomicilio.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(189)))), ((int)(((byte)(84)))));
            this.btnCargarImgDomicilio.FlatAppearance.BorderSize = 30;
            this.btnCargarImgDomicilio.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnCargarImgDomicilio.Image = global::pdv_uth_v1.Properties.Resources._1486505253_folder_folder_up_folder_upload_update_folder_upload_81427__2_;
            this.btnCargarImgDomicilio.Location = new System.Drawing.Point(769, 43);
            this.btnCargarImgDomicilio.Name = "btnCargarImgDomicilio";
            this.btnCargarImgDomicilio.Size = new System.Drawing.Size(53, 52);
            this.btnCargarImgDomicilio.TabIndex = 64;
            this.btnCargarImgDomicilio.UseVisualStyleBackColor = false;
            this.btnCargarImgDomicilio.Click += new System.EventHandler(this.btnCargarImgDomicilio_Click);
            // 
            // pictureBoxComproINE
            // 
            this.pictureBoxComproINE.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(141)))), ((int)(((byte)(84)))));
            this.pictureBoxComproINE.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBoxComproINE.Location = new System.Drawing.Point(562, 104);
            this.pictureBoxComproINE.Name = "pictureBoxComproINE";
            this.pictureBoxComproINE.Size = new System.Drawing.Size(204, 88);
            this.pictureBoxComproINE.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxComproINE.TabIndex = 63;
            this.pictureBoxComproINE.TabStop = false;
            // 
            // lblImagenComproINEGuardada
            // 
            this.lblImagenComproINEGuardada.AutoSize = true;
            this.lblImagenComproINEGuardada.Font = new System.Drawing.Font("Segoe Print", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblImagenComproINEGuardada.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(13)))), ((int)(((byte)(121)))));
            this.lblImagenComproINEGuardada.Location = new System.Drawing.Point(351, 109);
            this.lblImagenComproINEGuardada.Name = "lblImagenComproINEGuardada";
            this.lblImagenComproINEGuardada.Size = new System.Drawing.Size(97, 12);
            this.lblImagenComproINEGuardada.TabIndex = 62;
            this.lblImagenComproINEGuardada.Text = "Imagen del Comprobante";
            this.lblImagenComproINEGuardada.Visible = false;
            // 
            // lblImagenComproINE
            // 
            this.lblImagenComproINE.AutoSize = true;
            this.lblImagenComproINE.Font = new System.Drawing.Font("Segoe Print", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblImagenComproINE.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(13)))), ((int)(((byte)(121)))));
            this.lblImagenComproINE.Location = new System.Drawing.Point(351, 121);
            this.lblImagenComproINE.Name = "lblImagenComproINE";
            this.lblImagenComproINE.Size = new System.Drawing.Size(97, 12);
            this.lblImagenComproINE.TabIndex = 61;
            this.lblImagenComproINE.Text = "Imagen del Comprobante";
            this.lblImagenComproINE.Visible = false;
            // 
            // pictureBoxDomicilio
            // 
            this.pictureBoxDomicilio.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(156)))), ((int)(((byte)(0)))));
            this.pictureBoxDomicilio.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBoxDomicilio.Location = new System.Drawing.Point(562, 9);
            this.pictureBoxDomicilio.Name = "pictureBoxDomicilio";
            this.pictureBoxDomicilio.Size = new System.Drawing.Size(204, 88);
            this.pictureBoxDomicilio.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxDomicilio.TabIndex = 60;
            this.pictureBoxDomicilio.TabStop = false;
            // 
            // lblImagendelComproGuardada
            // 
            this.lblImagendelComproGuardada.AutoSize = true;
            this.lblImagendelComproGuardada.Font = new System.Drawing.Font("Segoe Print", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblImagendelComproGuardada.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(13)))), ((int)(((byte)(121)))));
            this.lblImagendelComproGuardada.Location = new System.Drawing.Point(342, 20);
            this.lblImagendelComproGuardada.Name = "lblImagendelComproGuardada";
            this.lblImagendelComproGuardada.Size = new System.Drawing.Size(97, 12);
            this.lblImagendelComproGuardada.TabIndex = 59;
            this.lblImagendelComproGuardada.Text = "Imagen del Comprobante";
            this.lblImagendelComproGuardada.Visible = false;
            // 
            // lblImagenComproDom
            // 
            this.lblImagenComproDom.AutoSize = true;
            this.lblImagenComproDom.Font = new System.Drawing.Font("Segoe Print", 5.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblImagenComproDom.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(13)))), ((int)(((byte)(121)))));
            this.lblImagenComproDom.Location = new System.Drawing.Point(342, 32);
            this.lblImagenComproDom.Name = "lblImagenComproDom";
            this.lblImagenComproDom.Size = new System.Drawing.Size(97, 12);
            this.lblImagenComproDom.TabIndex = 58;
            this.lblImagenComproDom.Text = "Imagen del Comprobante";
            this.lblImagenComproDom.Visible = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::pdv_uth_v1.Properties.Resources.Logo;
            this.pictureBox1.Location = new System.Drawing.Point(1020, 130);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(255, 203);
            this.pictureBox1.TabIndex = 45;
            this.pictureBox1.TabStop = false;
            // 
            // btnCerrarTodo
            // 
            this.btnCerrarTodo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(176)))), ((int)(((byte)(105)))));
            this.btnCerrarTodo.FlatAppearance.BorderSize = 0;
            this.btnCerrarTodo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCerrarTodo.Image = global::pdv_uth_v1.Properties.Resources.Windows_Close_Program_22531__1_;
            this.btnCerrarTodo.Location = new System.Drawing.Point(1282, -2);
            this.btnCerrarTodo.Name = "btnCerrarTodo";
            this.btnCerrarTodo.Size = new System.Drawing.Size(84, 68);
            this.btnCerrarTodo.TabIndex = 22;
            this.btnCerrarTodo.UseVisualStyleBackColor = false;
            this.btnCerrarTodo.Click += new System.EventHandler(this.btnCerrarTodo_Click);
            // 
            // txtFraccionamiento
            // 
            this.txtFraccionamiento.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(189)))), ((int)(((byte)(84)))));
            this.txtFraccionamiento.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtFraccionamiento.Font = new System.Drawing.Font("Segoe Print", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFraccionamiento.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(141)))), ((int)(((byte)(84)))));
            this.txtFraccionamiento.Location = new System.Drawing.Point(562, 303);
            this.txtFraccionamiento.Name = "txtFraccionamiento";
            this.txtFraccionamiento.Size = new System.Drawing.Size(204, 20);
            this.txtFraccionamiento.TabIndex = 17;
            // 
            // lblFraccionamiento
            // 
            this.lblFraccionamiento.AutoSize = true;
            this.lblFraccionamiento.Font = new System.Drawing.Font("Segoe Print", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFraccionamiento.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.lblFraccionamiento.Location = new System.Drawing.Point(339, 295);
            this.lblFraccionamiento.Name = "lblFraccionamiento";
            this.lblFraccionamiento.Size = new System.Drawing.Size(145, 28);
            this.lblFraccionamiento.TabIndex = 42;
            this.lblFraccionamiento.Text = "Fraccionamiento";
            // 
            // txtTelefono
            // 
            this.txtTelefono.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(189)))), ((int)(((byte)(84)))));
            this.txtTelefono.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtTelefono.Font = new System.Drawing.Font("Segoe Print", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTelefono.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(141)))), ((int)(((byte)(84)))));
            this.txtTelefono.Location = new System.Drawing.Point(1020, 104);
            this.txtTelefono.Name = "txtTelefono";
            this.txtTelefono.Size = new System.Drawing.Size(200, 20);
            this.txtTelefono.TabIndex = 16;
            this.txtTelefono.TextChanged += new System.EventHandler(this.txtTelefono_TextChanged);
            this.txtTelefono.Leave += new System.EventHandler(this.txtTelefono_Leave);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Segoe Print", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.label16.Location = new System.Drawing.Point(837, 93);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(79, 28);
            this.label16.TabIndex = 40;
            this.label16.Text = "Teléfono";
            // 
            // txtCurp
            // 
            this.txtCurp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(189)))), ((int)(((byte)(84)))));
            this.txtCurp.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtCurp.Font = new System.Drawing.Font("Segoe Print", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCurp.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(141)))), ((int)(((byte)(84)))));
            this.txtCurp.Location = new System.Drawing.Point(1020, 27);
            this.txtCurp.Name = "txtCurp";
            this.txtCurp.Size = new System.Drawing.Size(183, 20);
            this.txtCurp.TabIndex = 14;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Segoe Print", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.label14.Location = new System.Drawing.Point(837, 20);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(51, 28);
            this.label14.TabIndex = 38;
            this.label14.Text = "Curp";
            // 
            // btnModificar
            // 
            this.btnModificar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(156)))), ((int)(((byte)(0)))));
            this.btnModificar.FlatAppearance.BorderSize = 30;
            this.btnModificar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnModificar.Font = new System.Drawing.Font("Segoe Print", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnModificar.Image = global::pdv_uth_v1.Properties.Resources.businessapplication_edit_male_user_thepencil_theclient_negocio_23211;
            this.btnModificar.Location = new System.Drawing.Point(919, 296);
            this.btnModificar.Name = "btnModificar";
            this.btnModificar.Size = new System.Drawing.Size(44, 44);
            this.btnModificar.TabIndex = 19;
            this.btnModificar.UseVisualStyleBackColor = false;
            this.btnModificar.Click += new System.EventHandler(this.btnModificar_Click);
            // 
            // btnBorrar
            // 
            this.btnBorrar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(176)))), ((int)(((byte)(105)))));
            this.btnBorrar.FlatAppearance.BorderSize = 30;
            this.btnBorrar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnBorrar.Font = new System.Drawing.Font("Segoe Print", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBorrar.Image = global::pdv_uth_v1.Properties.Resources.delete_delete_deleteusers_delete_male_user_maleclient_23481;
            this.btnBorrar.Location = new System.Drawing.Point(857, 295);
            this.btnBorrar.Name = "btnBorrar";
            this.btnBorrar.Size = new System.Drawing.Size(56, 45);
            this.btnBorrar.TabIndex = 20;
            this.btnBorrar.UseVisualStyleBackColor = false;
            this.btnBorrar.Click += new System.EventHandler(this.btnBorrar_Click);
            // 
            // btnLimpiar
            // 
            this.btnLimpiar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(176)))), ((int)(((byte)(105)))));
            this.btnLimpiar.FlatAppearance.BorderSize = 30;
            this.btnLimpiar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnLimpiar.Font = new System.Drawing.Font("Segoe Print", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLimpiar.Image = global::pdv_uth_v1.Properties.Resources._3792081_broom_halloween_magic_witch_1090491;
            this.btnLimpiar.Location = new System.Drawing.Point(969, 295);
            this.btnLimpiar.Name = "btnLimpiar";
            this.btnLimpiar.Size = new System.Drawing.Size(45, 45);
            this.btnLimpiar.TabIndex = 21;
            this.btnLimpiar.UseVisualStyleBackColor = false;
            this.btnLimpiar.Click += new System.EventHandler(this.btnLimpiar_Click);
            // 
            // btnGuardar
            // 
            this.btnGuardar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(156)))), ((int)(((byte)(0)))));
            this.btnGuardar.FlatAppearance.BorderSize = 30;
            this.btnGuardar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnGuardar.Font = new System.Drawing.Font("Segoe Print", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGuardar.Image = global::pdv_uth_v1.Properties.Resources.business_application_addmale_useradd_insert_add_user_client_23121;
            this.btnGuardar.Location = new System.Drawing.Point(793, 295);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(58, 46);
            this.btnGuardar.TabIndex = 18;
            this.btnGuardar.UseVisualStyleBackColor = false;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Segoe Print", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.label11.Location = new System.Drawing.Point(837, 54);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(177, 28);
            this.label11.TabIndex = 32;
            this.label11.Text = "Fecha de nacimiento";
            // 
            // dtpFechaNacimiento
            // 
            this.dtpFechaNacimiento.CalendarFont = new System.Drawing.Font("Segoe Print", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpFechaNacimiento.CalendarForeColor = System.Drawing.Color.Red;
            this.dtpFechaNacimiento.CalendarMonthBackground = System.Drawing.Color.Red;
            this.dtpFechaNacimiento.CalendarTitleBackColor = System.Drawing.Color.Red;
            this.dtpFechaNacimiento.CalendarTitleForeColor = System.Drawing.Color.Red;
            this.dtpFechaNacimiento.CalendarTrailingForeColor = System.Drawing.Color.Red;
            this.dtpFechaNacimiento.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpFechaNacimiento.Location = new System.Drawing.Point(1020, 55);
            this.dtpFechaNacimiento.Name = "dtpFechaNacimiento";
            this.dtpFechaNacimiento.Size = new System.Drawing.Size(200, 27);
            this.dtpFechaNacimiento.TabIndex = 15;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Segoe Print", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.label15.Location = new System.Drawing.Point(328, 109);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(180, 28);
            this.label15.TabIndex = 30;
            this.label15.Text = "Comprobante de INE";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Segoe Print", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.label13.Location = new System.Drawing.Point(3, 289);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(89, 28);
            this.label13.TabIndex = 28;
            this.label13.Text = "Localidad";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Segoe Print", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.label12.Location = new System.Drawing.Point(3, 317);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(90, 28);
            this.label12.TabIndex = 27;
            this.label12.Text = "Municipio";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Segoe Print", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.label10.Location = new System.Drawing.Point(328, 17);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(228, 28);
            this.label10.TabIndex = 25;
            this.label10.Text = "Comprobante De Domicilio";
            // 
            // txtLocalidad
            // 
            this.txtLocalidad.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(189)))), ((int)(((byte)(84)))));
            this.txtLocalidad.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtLocalidad.Font = new System.Drawing.Font("Segoe Print", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLocalidad.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(141)))), ((int)(((byte)(84)))));
            this.txtLocalidad.Location = new System.Drawing.Point(178, 295);
            this.txtLocalidad.Name = "txtLocalidad";
            this.txtLocalidad.Size = new System.Drawing.Size(100, 20);
            this.txtLocalidad.TabIndex = 10;
            // 
            // txtMunicipio
            // 
            this.txtMunicipio.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(189)))), ((int)(((byte)(84)))));
            this.txtMunicipio.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtMunicipio.Font = new System.Drawing.Font("Segoe Print", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMunicipio.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(141)))), ((int)(((byte)(84)))));
            this.txtMunicipio.Location = new System.Drawing.Point(178, 323);
            this.txtMunicipio.Name = "txtMunicipio";
            this.txtMunicipio.Size = new System.Drawing.Size(100, 20);
            this.txtMunicipio.TabIndex = 11;
            // 
            // txtCalle
            // 
            this.txtCalle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(189)))), ((int)(((byte)(84)))));
            this.txtCalle.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtCalle.Font = new System.Drawing.Font("Segoe Print", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCalle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(141)))), ((int)(((byte)(84)))));
            this.txtCalle.Location = new System.Drawing.Point(178, 176);
            this.txtCalle.Name = "txtCalle";
            this.txtCalle.Size = new System.Drawing.Size(100, 20);
            this.txtCalle.TabIndex = 6;
            this.txtCalle.TextChanged += new System.EventHandler(this.txtCalle_TextChanged);
            // 
            // txtCP
            // 
            this.txtCP.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(189)))), ((int)(((byte)(84)))));
            this.txtCP.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtCP.Font = new System.Drawing.Font("Segoe Print", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCP.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(141)))), ((int)(((byte)(84)))));
            this.txtCP.Location = new System.Drawing.Point(178, 267);
            this.txtCP.Name = "txtCP";
            this.txtCP.Size = new System.Drawing.Size(100, 20);
            this.txtCP.TabIndex = 9;
            this.txtCP.Leave += new System.EventHandler(this.txtCP_Leave);
            // 
            // txtCorreo
            // 
            this.txtCorreo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(189)))), ((int)(((byte)(84)))));
            this.txtCorreo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtCorreo.Font = new System.Drawing.Font("Segoe Print", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCorreo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(141)))), ((int)(((byte)(84)))));
            this.txtCorreo.Location = new System.Drawing.Point(178, 145);
            this.txtCorreo.Name = "txtCorreo";
            this.txtCorreo.Size = new System.Drawing.Size(100, 20);
            this.txtCorreo.TabIndex = 5;
            this.txtCorreo.TextChanged += new System.EventHandler(this.txtCorreo_TextChanged);
            this.txtCorreo.Leave += new System.EventHandler(this.txtCorreo_Leave);
            // 
            // txtNumCalle
            // 
            this.txtNumCalle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(189)))), ((int)(((byte)(84)))));
            this.txtNumCalle.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtNumCalle.Font = new System.Drawing.Font("Segoe Print", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNumCalle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(141)))), ((int)(((byte)(84)))));
            this.txtNumCalle.Location = new System.Drawing.Point(178, 204);
            this.txtNumCalle.Name = "txtNumCalle";
            this.txtNumCalle.Size = new System.Drawing.Size(100, 20);
            this.txtNumCalle.TabIndex = 7;
            this.txtNumCalle.TextChanged += new System.EventHandler(this.txtNumCalle_TextChanged);
            // 
            // txtColonia
            // 
            this.txtColonia.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(189)))), ((int)(((byte)(84)))));
            this.txtColonia.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtColonia.Font = new System.Drawing.Font("Segoe Print", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtColonia.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(141)))), ((int)(((byte)(84)))));
            this.txtColonia.Location = new System.Drawing.Point(178, 234);
            this.txtColonia.Name = "txtColonia";
            this.txtColonia.Size = new System.Drawing.Size(100, 20);
            this.txtColonia.TabIndex = 8;
            // 
            // txtApMat
            // 
            this.txtApMat.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(189)))), ((int)(((byte)(84)))));
            this.txtApMat.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtApMat.Font = new System.Drawing.Font("Segoe Print", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtApMat.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(141)))), ((int)(((byte)(84)))));
            this.txtApMat.Location = new System.Drawing.Point(178, 83);
            this.txtApMat.Name = "txtApMat";
            this.txtApMat.Size = new System.Drawing.Size(100, 20);
            this.txtApMat.TabIndex = 3;
            this.txtApMat.TextChanged += new System.EventHandler(this.txtApMat_TextChanged);
            this.txtApMat.Leave += new System.EventHandler(this.txtApMat_Leave);
            // 
            // txtApPat
            // 
            this.txtApPat.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(189)))), ((int)(((byte)(84)))));
            this.txtApPat.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtApPat.Font = new System.Drawing.Font("Segoe Print", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtApPat.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(141)))), ((int)(((byte)(84)))));
            this.txtApPat.Location = new System.Drawing.Point(178, 51);
            this.txtApPat.Name = "txtApPat";
            this.txtApPat.Size = new System.Drawing.Size(100, 20);
            this.txtApPat.TabIndex = 2;
            this.txtApPat.TextChanged += new System.EventHandler(this.txtApPat_TextChanged);
            this.txtApPat.Leave += new System.EventHandler(this.txtApPat_Leave);
            // 
            // txtCelular
            // 
            this.txtCelular.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(189)))), ((int)(((byte)(84)))));
            this.txtCelular.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtCelular.Font = new System.Drawing.Font("Segoe Print", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCelular.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(141)))), ((int)(((byte)(84)))));
            this.txtCelular.Location = new System.Drawing.Point(178, 115);
            this.txtCelular.Name = "txtCelular";
            this.txtCelular.Size = new System.Drawing.Size(100, 20);
            this.txtCelular.TabIndex = 4;
            this.txtCelular.TextChanged += new System.EventHandler(this.txtCelular_TextChanged);
            this.txtCelular.Leave += new System.EventHandler(this.txtCelular_Leave);
            // 
            // txtNombre
            // 
            this.txtNombre.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(189)))), ((int)(((byte)(84)))));
            this.txtNombre.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtNombre.Font = new System.Drawing.Font("Segoe Print", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNombre.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(141)))), ((int)(((byte)(84)))));
            this.txtNombre.Location = new System.Drawing.Point(178, 17);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(100, 20);
            this.txtNombre.TabIndex = 1;
            this.txtNombre.TextChanged += new System.EventHandler(this.txtNombre_TextChanged);
            this.txtNombre.Leave += new System.EventHandler(this.txtNombre_Leave);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Segoe Print", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.label9.Location = new System.Drawing.Point(3, 228);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(71, 28);
            this.label9.TabIndex = 8;
            this.label9.Text = "Colonia";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Segoe Print", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.label8.Location = new System.Drawing.Point(3, 261);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(38, 28);
            this.label8.TabIndex = 7;
            this.label8.Text = "C.P";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Segoe Print", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.label7.Location = new System.Drawing.Point(3, 109);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(68, 28);
            this.label7.TabIndex = 6;
            this.label7.Text = "Celular";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Segoe Print", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.label6.Location = new System.Drawing.Point(3, 137);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 28);
            this.label6.TabIndex = 5;
            this.label6.Text = "Correo";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe Print", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.label5.Location = new System.Drawing.Point(3, 198);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(147, 28);
            this.label5.TabIndex = 4;
            this.label5.Text = "Número De Calle";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe Print", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.label4.Location = new System.Drawing.Point(3, 43);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(147, 28);
            this.label4.TabIndex = 3;
            this.label4.Text = "Apellido Paterno";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe Print", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.label3.Location = new System.Drawing.Point(3, 75);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(151, 28);
            this.label3.TabIndex = 2;
            this.label3.Text = "Apellido Materno";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe Print", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.label2.Location = new System.Drawing.Point(3, 170);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 28);
            this.label2.TabIndex = 1;
            this.label2.Text = "Calle";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe Print", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(11)))), ((int)(((byte)(143)))));
            this.label1.Location = new System.Drawing.Point(7, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 28);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nombre";
            // 
            // panelAcciones
            // 
            this.panelAcciones.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(156)))), ((int)(((byte)(0)))));
            this.panelAcciones.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelAcciones.Location = new System.Drawing.Point(0, 522);
            this.panelAcciones.Name = "panelAcciones";
            this.panelAcciones.Size = new System.Drawing.Size(1366, 10);
            this.panelAcciones.TabIndex = 1;
            // 
            // panelDataGrid
            // 
            this.panelDataGrid.BackColor = System.Drawing.SystemColors.ControlDark;
            this.panelDataGrid.Controls.Add(this.dgCliente);
            this.panelDataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelDataGrid.Location = new System.Drawing.Point(0, 353);
            this.panelDataGrid.Name = "panelDataGrid";
            this.panelDataGrid.Size = new System.Drawing.Size(1366, 179);
            this.panelDataGrid.TabIndex = 1;
            // 
            // dgCliente
            // 
            this.dgCliente.AllowUserToOrderColumns = true;
            this.dgCliente.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgCliente.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(141)))), ((int)(((byte)(84)))));
            this.dgCliente.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgCliente.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(189)))), ((int)(((byte)(84)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe Print", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(141)))), ((int)(((byte)(84)))));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(208)))), ((int)(((byte)(135)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(156)))), ((int)(((byte)(0)))));
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgCliente.DefaultCellStyle = dataGridViewCellStyle1;
            this.dgCliente.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgCliente.Location = new System.Drawing.Point(0, 0);
            this.dgCliente.Name = "dgCliente";
            this.dgCliente.Size = new System.Drawing.Size(1366, 179);
            this.dgCliente.TabIndex = 0;
            this.dgCliente.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgCliente_CellContentClick);
            this.dgCliente.RowDefaultCellStyleChanged += new System.Windows.Forms.DataGridViewRowEventHandler(this.dgCliente_RowDefaultCellStyleChanged);
            // 
            // errorProviderForm
            // 
            this.errorProviderForm.ContainerControl = this;
            this.errorProviderForm.Icon = ((System.Drawing.Icon)(resources.GetObject("errorProviderForm.Icon")));
            // 
            // openFileDialogImagenACargar
            // 
            this.openFileDialogImagenACargar.FileName = "openFileDialog1";
            // 
            // FrmCatalogoCliente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Coral;
            this.ClientSize = new System.Drawing.Size(1366, 532);
            this.Controls.Add(this.panelAcciones);
            this.Controls.Add(this.panelDataGrid);
            this.Controls.Add(this.panelFormu);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmCatalogoCliente";
            this.Text = "FrmCatalogoCliente";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FrmCatalogoCliente_Load);
            this.panelFormu.ResumeLayout(false);
            this.panelFormu.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxComproCURP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxComproINE)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxDomicilio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panelDataGrid.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgCliente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderForm)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelFormu;
        private System.Windows.Forms.Button btnModificar;
        private System.Windows.Forms.Button btnBorrar;
        private System.Windows.Forms.Button btnLimpiar;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.DateTimePicker dtpFechaNacimiento;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtLocalidad;
        private System.Windows.Forms.TextBox txtMunicipio;
        private System.Windows.Forms.TextBox txtCalle;
        private System.Windows.Forms.TextBox txtCP;
        private System.Windows.Forms.TextBox txtCorreo;
        private System.Windows.Forms.TextBox txtNumCalle;
        private System.Windows.Forms.TextBox txtColonia;
        private System.Windows.Forms.TextBox txtApMat;
        private System.Windows.Forms.TextBox txtApPat;
        private System.Windows.Forms.TextBox txtCelular;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panelAcciones;
        private System.Windows.Forms.Panel panelDataGrid;
        private System.Windows.Forms.SaveFileDialog saveFDComprobantes;
        private System.Windows.Forms.TextBox txtCurp;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtTelefono;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ErrorProvider errorProviderForm;
        private System.Windows.Forms.TextBox txtFraccionamiento;
        private System.Windows.Forms.Label lblFraccionamiento;
        private System.Windows.Forms.Button btnCerrarTodo;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.DataGridView dgCliente;
        private System.Windows.Forms.Label lblImagendelComproGuardada;
        private System.Windows.Forms.Label lblImagenComproDom;
        private System.Windows.Forms.PictureBox pictureBoxDomicilio;
        private System.Windows.Forms.Label lblImagenComproINEGuardada;
        private System.Windows.Forms.Label lblImagenComproINE;
        private System.Windows.Forms.PictureBox pictureBoxComproINE;
        private System.Windows.Forms.Button btnCargarImgDomicilio;
        private System.Windows.Forms.Button btnComproINEGuardada;
        private System.Windows.Forms.OpenFileDialog openFileDialogImagenACargar;
        private System.Windows.Forms.Label lblImagendeComproCURPGuardada;
        private System.Windows.Forms.Label lblImagenComprobanteCURP;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.PictureBox pictureBoxComproCURP;
        private System.Windows.Forms.Button btnComproCURPGuardada;
        private FontAwesome.Sharp.IconButton iconBtnIngresarAUsuarios;
        private FontAwesome.Sharp.IconButton iconBtnBackToHome;
        private System.Windows.Forms.Label lblUsuario;
        private System.Windows.Forms.ToolTip toolTipClientes;
        private FontAwesome.Sharp.IconButton iconBtnParaID;
    }
}