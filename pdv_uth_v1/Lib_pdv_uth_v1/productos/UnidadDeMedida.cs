﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib_pdv_uth_v1.productos
{
    public enum UnidadDeMedida
    {
        UNIDAD,
        LITRO,
        KILO,
        CAJA,
        PAQUETE
    }
}
