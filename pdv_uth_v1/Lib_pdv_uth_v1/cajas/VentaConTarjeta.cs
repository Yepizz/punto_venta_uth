﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib_pdv_uth_v1.cajas
{
    class VentaConTarjeta
    {
        protected string numTarjeta;
        protected string vigenciaMes;
        protected string vigenciaAño;
        protected string numAutorizacion;
        protected DateTime fechaHora;

        public string NumTarjeta { get => numTarjeta; set => numTarjeta = value; }
        public string VigenciaMes { get => vigenciaMes; set => vigenciaMes = value; }
        public string VigenciaAño { get => vigenciaAño; set => vigenciaAño = value; }
        public string NumAutorizacion { get => numAutorizacion; set => numAutorizacion = value; }
        public DateTime FechaHora { get => fechaHora; set => fechaHora = value; }

        public VentaConTarjeta(string numTarjeta, string vigenciaMes, string vigenciaAño, string numAutorizacion, DateTime fechaHora)
        {
            this.NumTarjeta = numTarjeta;
            this.VigenciaMes = vigenciaMes;
            this.VigenciaAño = vigenciaAño;
            this.NumAutorizacion = numAutorizacion;
            this.FechaHora = fechaHora;
        }
    }
}
