﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using LibBD;

using Lib_pdv_uth_v1;


namespace Lib_pdv_uth_v1.usuarios 
{
    public class Usuario : DatosUsuarios, ICrud
    {
        //VARIABLE DE BASE DE DATOS
        LibMySql bd;
        //var de errores
        public static string msgError = "";
      
        //CONSTRUCTOR DE USUARIO
        public Usuario(): base(1,"","","",new DateTime(),"","","","","","","","","","","","","","","","",new TipoUsuario(),"")
        {
            bd = new LibMySql("127.0.0.1","root","123123", "punto_venta_uth");
        }
        public Usuario(int id, string nombre, string apellidoPaterno, string apellidoMaterno, DateTime fechaNacimiento, string celular, string telefono, string correo, string calle, 
            string numeroCasa, string codigoPostal, string colonia, string fraccionamiento, string localidad,
            string municipio, string comproDom, string curp, string comproINE, string actaNacimiento, string certificadoEstudios,
            string numeroSeguroSocial, TipoUsuario tipoUsuario, string contraseña) : base(id, nombre, apellidoPaterno, apellidoMaterno,fechaNacimiento,celular,
                telefono,correo,calle,numeroCasa,codigoPostal, colonia,fraccionamiento, localidad,municipio,comproDom, curp,comproINE,actaNacimiento,
                certificadoEstudios,numeroSeguroSocial,tipoUsuario, contraseña)
        {
            bd = new LibMySql("127.0.0.1", "root", "123123", "punto_venta_uth");
        }

        

       
        public bool alta()
        {
            string fechaCorrecta = this.FechaNacimiento.Year + "-" + this.FechaNacimiento.Month + "-" + this.FechaNacimiento.Day;
           
            return insertar(this.Nombre, this.ApellidoPaterno , this.ApellidoMaterno , this.Celular, this.Telefono , this.Correo, fechaCorrecta, this.Calle, this.NumeroCasa, this.Colonia, this.Fraccionamiento,
                 this.CodigoPostal, this.Localidad, this.Municipio, this.ComproDom,
                 this.ComproINE,  this.Curp, this.ComproCurp,this.ActaNacimiento, this.CertificadoEstudios,
                 this.NumeroSeguroSocial,  this.TipoUsuario.ToString(),this.Contraseña);
           
        }

        public bool insertar(string nom, string apPatern, string apMatern, string cel, string telef, string correo, string fechaNacimiento,
                              string calle, string numeroCasa, string colonia, string fraccionamiento, string codigoPostal,
                              string localidad, string municipio, string comprobanteDomicilio,
                              string comprobanINE, string curp, string comproCurp, string actaNacimiento, string certifDeEstudios, string numeroSeguroSoc,
                              string tipoDeUsuario, string pdw)
        {

            bool res = false;
            string valores = "'" + nom + "','" + apPatern + "','" + apMatern + "','" + fechaNacimiento + "','" + cel + "','" + telef + "','" + correo + "','" + calle + "','" + numeroCasa + "','" + codigoPostal + "','" + colonia + "','" + fraccionamiento + "','" + localidad + "','" + municipio + "','"
                + comprobanteDomicilio + "','" + comprobanINE + "','" + curp + "','" + comproCurp + "','" + actaNacimiento + "','" + certifDeEstudios +
                "','" + numeroSeguroSoc + "','" + tipoDeUsuario + "','" + pdw + "'";
            if (bd.insertar("usuarios",
                            "nombre, apellido_paterno, apellido_materno, fecha_de_nacimiento, celular, telefono, correo," +
                            "calle, numero_casa, codigo_postal, colonia, fraccionamiento, localidad, municipio, img_comprobante_domicilio, ine_comprobante, " +
                            "curp, curp_comprobante, acta_nacimiento, certificado_estudios, numero_seguro_social, tipo_usuario, pwd ",
                            valores))
            {
                res = true;
            }
            else
            {
                msgError = "Error al dar de alta nuevo Usuario. " + LibMySql.msgError;
            }//Devolvemos resultado
            return res;
        }
        public TipoUsuario login(string us, string contraseña)
        {
            //hacemos la consulta de un registro de usuario con correo y una  contraseña
            object usLogueado = bd.consultarUnRegistro("*", "usuarios", " correo='" + us + "' AND pwd='" + contraseña + "'");
            object[] user = (object[])usLogueado;
            string tipo = user[22].ToString();
            //verificamos si existe un resultado
            if (usLogueado != null)
            {
                //el campo 22 es el tipo
                if (tipo == "ADMINISTRADOR")
                    return TipoUsuario.ADMINISTRADOR;
                else if (tipo == "CAJERO")
                    return TipoUsuario.CAJERO;
                else return TipoUsuario.ERROR;
            }
            else
            {
                msgError = "Usuario NO REGISTRADO. Consulte a Administrador. ";
                return TipoUsuario.ERROR;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="datos"></param>
        /// <param name="id"></param>
        /// <returns></returns>


        public bool modificar(List<ModificarInformacionUsuarios> datos, int idUsuario)
        {
            //crear la losta de datos
            string camposValores = "";
            for (int i = 0; i < datos.Count; i++)
            {
                camposValores += " " + datos[i].Campo + " = " + "'" + datos[i].Valor + "'";
                if (i < datos.Count - 1) camposValores += ",";
            }
            //ejecuta el actualizar de BD con los datos
            return bd.actualizar("usuarios", camposValores, "id=" + idUsuario);

        }

        //public TipoUsuario LoginUs(string us, string contraseña)
        //{
        //    //Se hace la consulta de nuestro registro de correo y contraseña
        //    object UsLogueado = bd.consultarUnRegistro("*", "usuarios", "correo'" + us + "' AND pwd='" + contraseña + "'");
        //    //Sacamos el arreglo de objects
        //    object[] user = (object[])UsLogueado;
        //    //Creamos un domicilio
        //    Domicilio dom = new Domicilio();
        //    //Se valida si hay un registro de resultado
        //    if (user != null)
        //    {
        //        Usuario temp = new Usuario();
        //        {
        //            //Agregamos todos los valores en el objeto de usuario temp
        //            Id = int.Parse(user[0].ToString());
        //            Nombre = user[1].ToString();
        //            apellidoPaterno = user[2].ToString();
        //            TipoUsuario = user[22].ToString() == "CAJERO" ? TipoUsuario.CAJERO : TipoUsuario.ADMINISTRADOR;
        //        }
        //        temp.ApellidoMaterno = user[3].ToString();
        //        temp.FechaDeNacimiento = DateTime.Parse(user[4].ToString());
        //        temp.Celular = user[5].ToString();
        //        temp.Telefono = user[6].ToString();
        //        temp.Correo = user[7].ToString();
        //        temp.Domicilio = new Domicilio(user[8].ToString(), user[9].ToString(), user[10].ToString(),
        //            user[11].ToString(), user[12].ToString(), user[13].ToString(), user[14].ToString(),
        //            user[15].ToString());
        //        temp.ComprobanteINE = user[16].ToString();
        //        temp.Curp = user[17].ToString();
        //        temp.CurpCompro = user[18].ToString();


        //    }

        //}


        public List<object> consultar(List<CriteriosBusqueda> criteriosBusqueda)
        {
            //Creamos una instancia de objects para resultados
            List<object> resultado = new List<object>();
            //Creamos una variable where
            string where = "";
            for (int i = 0; i < criteriosBusqueda.Count; i++)
            {
                string operIntermedio = "";
                switch (criteriosBusqueda[i].operadorIntermedio)
                {
                    case OperadorDeConsulta.IGUAL: operIntermedio = "="; break;
                    case OperadorDeConsulta.LIKE: operIntermedio = "LIKE"; break;
                    case OperadorDeConsulta.DIFERENTE: operIntermedio = "<>"; break;
                    case OperadorDeConsulta.NO_IGUAL: operIntermedio = "!="; break;
                    default: operIntermedio = ""; break;
                }
                string op = "";
                switch (criteriosBusqueda[i].operadorIntermedio)
                {
                    case OperadorDeConsulta.AND: op = "AND"; break;
                    case OperadorDeConsulta.OR: op = "OR"; break;

                    default: op = ""; break;
                }
                where += " " + criteriosBusqueda[i].campo + " " + operIntermedio + " " + criteriosBusqueda[i].valor + " " + op + " ";
            }
            //Hacemos la consulta de una lista de Objects
            List<object> tempo = bd.consultar("*", "usuarios", where);
            //Hacemos un mapeo de cada objeto de Usuarios
            foreach (object[] UsuarioTemp in tempo)
            {
                var usuarioDatos = new
                {
                    Id = int.Parse(UsuarioTemp[0].ToString()),
                    Nombre = UsuarioTemp[1].ToString(),
                    ApellidoPaterno = UsuarioTemp[2].ToString(),
                    ApellidoMaterno = UsuarioTemp[3].ToString(),
                    FechaNacimiento = DateTime.Parse(UsuarioTemp[4].ToString()),
                    Celular = UsuarioTemp[5].ToString(),
                    Telefono = UsuarioTemp[6].ToString(),
                    Correo = UsuarioTemp[7].ToString(),
                    Calle = UsuarioTemp[8].ToString(),
                    NumeroCasa = UsuarioTemp[9].ToString(),
                    CodigoPostal = UsuarioTemp[10].ToString(),
                    Colonia = UsuarioTemp[11].ToString(),
                    Fraccionamiento = UsuarioTemp[12].ToString(),
                    Localidad = UsuarioTemp[13].ToString(),
                    Municipio = UsuarioTemp[14].ToString(),
                    ComproDom = UsuarioTemp[15].ToString(),
                    ComproINE = UsuarioTemp[16].ToString(),
                    Curp = UsuarioTemp[17].ToString(),
                    ComproCurp = UsuarioTemp[18].ToString(),
                    ActaNacimiento = UsuarioTemp[19].ToString(),
                    CertificadoEstudios = UsuarioTemp[20].ToString(),
                    NumeroSeguroSocial = UsuarioTemp[21].ToString(),
                    TipoUsuario = UsuarioTemp[22].ToString(),
                    Contraseña = UsuarioTemp[23].ToString()


                };
                resultado.Add(usuarioDatos);
            }
            //regresamos la lista de los datos del cliente
            return resultado;
        }
    
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool borrar(int id)
        {
            return bd.eliminar("usuarios", " id=" + id);
        }

        /// <summary>
        /// @param criteriosBusqueda 
        /// @return
        /// </summary>
        public List<object> consulta(List<CriteriosBusqueda> criteriosBusqueda)
        {
            throw new NotImplementedException();

        }

        /// <summary>
        /// @param us 
        /// @param contraseña 
        /// @return
        /// </summary>

        public bool editar(List<DatosParaActualizar> datos, int id)
        {
            //crear la losta de datos
            string camposValores = "";
            for (int i = 0; i < datos.Count; i++)
            {
                camposValores += " " + datos[i].Campo + " = " + datos[i].Valor;
                if (i < datos.Count - 1) camposValores += ",";
            }
            //ejecuta el actualizar de BD con los datos
            return bd.actualizar("usuarios", camposValores, "id=" + id);
           
        }
        bool ICrud.alta()
        {
            throw new NotImplementedException();
        }

        bool ICrud.modificar(List<DatosParaActualizar> datos, int id)
        {
            throw new NotImplementedException();
        }

        bool ICrud.eliminar(int id)
        {
            throw new NotImplementedException();
        }

        List<object> ICrud.consultar(List<CriteriosBusqueda> criteriosBusqueda)
        {
            throw new NotImplementedException();
        }
    }
}
