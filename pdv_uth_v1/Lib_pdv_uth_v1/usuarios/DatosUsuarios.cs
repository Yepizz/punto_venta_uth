﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib_pdv_uth_v1.usuarios
{
   public class DatosUsuarios
    {
        private int id;
        private string nombre;
        private string apellidoPaterno;
        private string apellidoMaterno;
        private DateTime fechaNacimiento;
        private string celular;
        private string telefono;
        private string correo;
        private string calle;
        private string numeroCasa;
        private string codigoPostal;
        private string colonia;
        private string fraccionamiento;
        private string localidad;
        private string municipio;
        private string comproDom;
        private string curp;
        private string comproINE;
        private string comproCurp;
        private string actaNacimiento;
        private string certificadoEstudios;
        private string numeroSeguroSocial;
        private TipoUsuario tipoUsuario;
        private string contraseña;

        //Getters y setter de Usuario
        public int Id { get => id; set => id = value; }
        public string Nombre { get => nombre; set => nombre = value; }
        public string ApellidoPaterno { get => apellidoPaterno; set => apellidoPaterno = value; }
        public string ApellidoMaterno { get => apellidoMaterno; set => apellidoMaterno = value; }
        public DateTime FechaNacimiento { get => fechaNacimiento; set => fechaNacimiento = value; }
        public string Celular { get => celular; set => celular = value; }
        public string Telefono { get => telefono; set => telefono = value; }
        public string Correo { get => correo; set => correo = value; }
        public string Calle { get => calle; set => calle = value; }
        public string NumeroCasa { get => numeroCasa; set => numeroCasa = value; }
        public string CodigoPostal { get => codigoPostal; set => codigoPostal = value; }
        public string Colonia { get => colonia; set => colonia = value; }
        public string Fraccionamiento { get => fraccionamiento; set => fraccionamiento = value; }
        public string Localidad { get => localidad; set => localidad = value; }
        public string Municipio { get => municipio; set => municipio = value; }
        public string ComproDom { get => comproDom; set => comproDom = value; }
        public string Curp { get => curp; set => curp = value; }
        public string ComproINE { get => comproINE; set => comproINE = value; }
        public string ActaNacimiento { get => actaNacimiento; set => actaNacimiento = value; }
        public string CertificadoEstudios { get => certificadoEstudios; set => certificadoEstudios = value; }
        public string NumeroSeguroSocial { get => numeroSeguroSocial; set => numeroSeguroSocial = value; }
        public TipoUsuario TipoUsuario { get => tipoUsuario; set => tipoUsuario = value; }
        public string Contraseña { get => contraseña; set => contraseña = value; }
        public string ComproCurp { get => comproCurp; set => comproCurp = value; }
        public DatosUsuarios(int id, string nombre, string apellidoPaterno, string apellidoMaterno, DateTime fechaNacimiento, string celular, string telefono, string correo, string calle, string numeroCasa, string codigoPostal, string colonia, string fraccionamiento, string localidad, string municipio, string comproDom, string curp, string comproINE, string actaNacimiento, string certificadoEstudios, string numeroSeguroSocial, TipoUsuario tipoUsuario, string contraseña)
        {
            this.Id = id;
            this.Nombre = nombre;
            this.ApellidoPaterno = apellidoPaterno;
            this.ApellidoMaterno = apellidoMaterno;
           this.FechaNacimiento = fechaNacimiento;
            this.Celular = celular;
            this.Telefono = telefono;
            this.Correo = correo;
            this.Calle = calle;
            this.NumeroCasa = numeroCasa;
            this.CodigoPostal = codigoPostal;
            this.Colonia = colonia;
            this.Fraccionamiento = fraccionamiento;
            this.Localidad = localidad;
            this.Municipio = municipio;
            this.ComproDom = comproDom;
            this.Curp = curp;
            this.ComproINE = comproINE;
            this.ComproCurp = comproCurp;
            this.ActaNacimiento = actaNacimiento;
            this.CertificadoEstudios = certificadoEstudios;
            this.NumeroSeguroSocial = numeroSeguroSocial;
            this.TipoUsuario = tipoUsuario;
            this.Contraseña = contraseña;
        }
    }
}
