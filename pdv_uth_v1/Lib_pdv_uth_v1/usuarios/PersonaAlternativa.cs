﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LibBD;

using Lib_pdv_uth_v1;

namespace Lib_pdv_uth_v1.usuarios
{
    public class PersonaAlternativa: DatosPersonaAlternativa
    {
        //Variable de base de datos
        LibMySql bd;
        //Variable de mensaje de eroores
        public static string msgError = "";
        public int idCliente;

        //Constructor de usuario
        public PersonaAlternativa(): base(1,"","","",new DateTime(),"","","","","","","","","","","","","","",1)
        {
            bd = new LibMySql("127.0.0.1", "root", "123123", "punto_venta_uth");
        }
        //Constructor de usuario
        public PersonaAlternativa(int id, string nombre,string apellidoPaterno, string apellidoMaterno, DateTime fechaNacimiento, string celular, string telefono, string correo, string calle, string numCasa,
            string codigoPostal,string colonia, string fraccionamiento, string localidad, string municipio, string comproDom, string comproIne, string curp, string comproCurp,
            int clienteId):base(id,nombre,apellidoPaterno,apellidoMaterno,fechaNacimiento,celular, telefono, correo, calle,numCasa,codigoPostal,colonia,fraccionamiento,localidad,municipio,
                comproDom, comproIne, curp, comproCurp, clienteId)
        {
            bd = new LibMySql("127.0.0.1", "root", "123123", "punto_venta_uth");
        }

        public bool Alta()
        {
            string fechaCorrecta = this.FechaNacimiento.Year + "-" + this.FechaNacimiento.Month + "-" + this.FechaNacimiento.Day;

            return insertar(this.Nombre, this.ApellidoPaterno, this.ApellidoMaterno, fechaCorrecta, this.Celular, this.Telefono, this.Correo, this.Calle, this.NumCasa, this.CodigoPostal,
                this.Colonia, this.Fraccionamiento, this.Localidad, this.Municipio, this.ComproDomicilio, this.ComprobanteINE, this.Curp, this.ComproCURP);
        }

        public bool insertar(string nom, string apPatern, string apMatern, string fechaNacimiento, string cel, string telef, string correo, string calle, string numeroCasa, string codigoPostal, string colonia, string fraccionamiento, string localidad, string municipio, string comprobanteDomicilio, string comprobanINE, string curp, string comproCurp)
        {
            bool res = false;
            string valores = "'" + nom + "','" + apPatern + "','" + apMatern + "','" + fechaNacimiento + "','" + cel + "','" + telef + "','" + correo + "','" + calle + "','" + numeroCasa + "','" + codigoPostal + "','" + colonia + "','" + fraccionamiento + "','" + localidad + "','" + municipio + "','"
                + comprobanteDomicilio + "','" + comprobanINE + "','" + curp + "','" + comproCurp + "','" + idCliente + "'";

            if (bd.insertar("persona_alternativas",
                            "nombre, apellido_paterno, apellido_materno, fecha_de_nacimiento, celular, telefono, correo," +
                            "calle, numero_casa, codigo_postal, colonia, fraccionamiento, localidad, municipio, img_comprobante_domicilio, ine_comprobante, " +
                            "curp, curp_comprobante, cliente_id ",
                            valores))
            {
                
                res = true;
            }
            else
            {
                msgError = "Error al dar de alta a Persona alternativa. " + LibMySql.msgError;
            }//Ahora devolvemos el resultado
            return res;
        }

        public bool modificar(List<DatosActualizarUsuario> datos, int idPersonaAlternativa)
        {
            //crear la losta de datos
            string camposValores = "";
            for (int i = 0; i < datos.Count; i++)
            {
                camposValores += " " + datos[i].Campo + " = " + "'" + datos[i].Valor + "'";
                if (i < datos.Count - 1) camposValores += ",";
            }
            //ejecuta el actualizar de BD con los datos
            return bd.actualizar("persona_alternativas", camposValores, "id=" + idPersonaAlternativa);
        }



        public List<object> consultar(List<CriteriosBusqueda> criteriosBusqueda)
        {
            //Creamos una instancia de objects para resultados
            List<object> resultado = new List<object>();
            //Creamos una variable where
            string where = "";
            for (int i = 0; i < criteriosBusqueda.Count; i++)
            {
                string operIntermedio = "";
                switch (criteriosBusqueda[i].operadorIntermedio)
                {
                    case OperadorDeConsulta.IGUAL: operIntermedio = "="; break;
                    case OperadorDeConsulta.LIKE: operIntermedio = "LIKE"; break;
                    case OperadorDeConsulta.DIFERENTE: operIntermedio = "<>"; break;
                    case OperadorDeConsulta.NO_IGUAL: operIntermedio = "!="; break;
                    default: operIntermedio = ""; break;
                }
                string op = "";
                switch (criteriosBusqueda[i].operadorIntermedio)
                {
                    case OperadorDeConsulta.AND: op = "AND"; break;
                    case OperadorDeConsulta.OR: op = "OR"; break;

                    default: op = ""; break;
                }
                where += " " + criteriosBusqueda[i].campo + " " + operIntermedio + " " + criteriosBusqueda[i].valor + " " + op + " ";
            }
            //Hacemos la consulta de una lista de Objects
            List<object> tempo = bd.consultar("*", "persona_alternativas", where);
            //Hacemos un mapeo de cada objeto de Usuarios
            foreach (object[] Persona_AlterTemp in tempo)
            {
                var Persona_AlterDatos = new
                {
                    Id = int.Parse(Persona_AlterTemp[0].ToString()),
                    Nombre = Persona_AlterTemp[1].ToString(),
                    ApellidoPaterno = Persona_AlterTemp[2].ToString(),
                    ApellidoMaterno = Persona_AlterTemp[3].ToString(),
                    FechaNacimiento = DateTime.Parse(Persona_AlterTemp[4].ToString()),
                    Celular = Persona_AlterTemp[5].ToString(),
                    Telefono = Persona_AlterTemp[6].ToString(),
                    Correo = Persona_AlterTemp[7].ToString(),
                    Calle = Persona_AlterTemp[8].ToString(),
                    NumeroCasa = Persona_AlterTemp[9].ToString(),
                    CodigoPostal = Persona_AlterTemp[10].ToString(),
                    Colonia = Persona_AlterTemp[11].ToString(),
                    Fraccionamiento = Persona_AlterTemp[12].ToString(),
                    Localidad = Persona_AlterTemp[13].ToString(),
                    Municipio = Persona_AlterTemp[14].ToString(),
                    ComproDom = Persona_AlterTemp[15].ToString(),
                    ComproINE = Persona_AlterTemp[16].ToString(),
                    Curp = Persona_AlterTemp[17].ToString(),
                    ComproCurp = Persona_AlterTemp[18].ToString(),
                    ClienteID = Persona_AlterTemp[19].ToString()
                    


                };
                resultado.Add(Persona_AlterDatos);
            }
            //regresamos la lista de los datos del cliente
            return resultado;

        }
        public bool borrar(int id)
        {
            return bd.eliminar("persona_alternativas", " id=" + id);
        }

        public bool editar(List<DatosParaActualizar> datos, int id)
        {
            //crear la lista de datos
            string camposValores = "";
            for (int i = 0; i < datos.Count; i++)
            {
                camposValores += " " + datos[i].Campo + " = " + datos[i].Valor;
                if (i < datos.Count - 1) camposValores += ",";
            }
            //ejecuta el actualizar de BD con los datos
            return bd.actualizar("persona_alternativas", camposValores, "id=" + id);

        }

        public List<PersonaAlternativa> Consultar(string nombre, string apellido, string calle)
        {
            string where = "nombre LIKE '%" + nombre + "%' AND ( apellido_paterno LIKE '%" + apellido + "%' OR  apellido_materno LIKE '%" + apellido + "%') AND calle LIKE '%" + calle + "%'";
            List<object> listaTemp = bd.consultar("*", "persona_alternativas", where);
            List<PersonaAlternativa> listaPersonaAlt = new List<PersonaAlternativa>();
            foreach (var registro in listaTemp)
            {
                for (int i = 0; i < listaTemp.Count; i++)
                {
                    object[] arreglo = (object[])registro;
                    //creamos un objeto Cliente
                    PersonaAlternativa tempo = new PersonaAlternativa
                    {
                        //poner todos los valores en el objeto cliente temp
                        Id = int.Parse(arreglo[0].ToString()),
                        Nombre = arreglo[1].ToString(),
                        ApellidoPaterno = arreglo[2].ToString()
                    };
                    tempo.ApellidoMaterno = arreglo[3].ToString();
                    tempo.FechaNacimiento = DateTime.Parse(arreglo[4].ToString());
                    tempo.Celular = arreglo[5].ToString();
                    tempo.Telefono = arreglo[6].ToString();
                    tempo.Correo = arreglo[7].ToString();
                    tempo.Calle = arreglo[8].ToString();
                    tempo.NumCasa = arreglo[9].ToString();
                    tempo.CodigoPostal = arreglo[10].ToString();
                    tempo.Colonia = arreglo[11].ToString();
                    tempo.Fraccionamiento = arreglo[12].ToString();
                    tempo.Localidad = arreglo[13].ToString();
                    tempo.Municipio = arreglo[14].ToString();
                    tempo.ComproDomicilio = arreglo[15].ToString();
                    tempo.ComprobanteINE = arreglo[16].ToString();
                    tempo.Curp = arreglo[17].ToString();
                    tempo.ComproCURP = arreglo[18].ToString();
                    tempo.idCliente = int.Parse(arreglo[19].ToString());
                    //ponemos obj temp  en listacliente
                    listaPersonaAlt.Add(tempo);
                }
            }
            return listaPersonaAlt;
        }

        public bool Actualizar(string nom, string apP, string apM, string cel, string correo, string fechaNac,
                             string calle, string numCasa, string colonia, string cp,
                             string localidad, string municipio, string comprobanteDom, string comproINE, string id)
        {
            bool res = false;
            if (bd.actualizar("",
                              "nombre='" + nom + "',apellido_paterno='" + apP + "',apellido_materno='" + apM + "',celular='" + cel + "'," +
                              " correo='" + correo + "',fecha_de_nacimiento = '" + fechaNac + "',calle = '" + calle + "',numero_casa = '" + numCasa + "'" +
                              ",colonia = '" + colonia + "',codigo_postal = '" + cp + "',localidad = '" + localidad + "',municipio = '" + municipio + "'" +
                              ",img_comprobante_domicilio = '" + comprobanteDom + "',ine_comprobante = '"
                              + comproINE + "'", " cliente_id=" + id
                            )
                )
            { res = true; }
            else
            {
                msgError = "Error al actualizar la persona Alternativa. " + LibMySql.msgError;
            }
            return res;
        }

    }
}
