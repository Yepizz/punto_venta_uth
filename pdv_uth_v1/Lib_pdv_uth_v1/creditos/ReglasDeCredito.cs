﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib_pdv_uth_v1.creditos
{
    class ReglasDeCredito
    {
        private int maxFiadoPorSemana;
        private double minCubrirPorQuincena;

        public int MaxFiadoPorSemana { get => maxFiadoPorSemana; set => maxFiadoPorSemana = value; }
        public  double MinCubrirPorQuincena { get => minCubrirPorQuincena; set => minCubrirPorQuincena = value; }

        public ReglasDeCredito(int maxFiadoPorSemana, double minCubrirPorQuincena)
        {
            this.MaxFiadoPorSemana = maxFiadoPorSemana;
            this.MinCubrirPorQuincena = minCubrirPorQuincena;
        }
    }
}
