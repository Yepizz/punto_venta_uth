﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lib_pdv_uth_v1.clientes;
using LibBD;


namespace Lib_pdv_uth_v1.creditos
{
   public  class Credito: DatosCreditos
    {
        //prop de BD
        LibMySql bd;
        //var de errores
        public static string msgError = "";
        public int id;
        public Credito(): base(1,1,new DateTime(),1,new EstadoCredito(),new DateTime())
        {
            bd = new LibMySql("127.0.0.1", "root", "123123", "punto_venta_uth");
        }

        //Constructor de Crédito.
        public Credito(int id, double saldo, DateTime fecha_apertura, int cliente_id,
            EstadoCredito estado, DateTime fecha_saldado ): base(id, saldo, fecha_apertura, cliente_id,estado,fecha_saldado)
        {
            bd = new LibMySql("127.0.0.1", "root", "123123", "punto_venta_uth");
        }

        public bool Alta()
        {
            
            return insertar(this.Saldo, this.Fecha_apertura, this.Cliente_id, this.Estado, this.Fecha_saldado);
        }

        public bool insertar(double saldo, DateTime fecha_apertura, int cliente_id, EstadoCredito estado, DateTime fecha_saldado)
        {
            bool res = false;
            string valores = "'" + saldo + "','" + fecha_apertura + "','" + cliente_id + "','" + estado + "','" + fecha_saldado + "'";

            if (bd.insertar("creditos",
                          "saldo, fecha_apertura, cliente_id, estado, fecha_saldado",
                          valores))
            {

                res = true;
            }
            else
            {
                msgError = "Error al dar de alta el crédito al cliente. " + LibMySql.msgError;
            }//Devolver Res....
            return res;
        }

        public bool borrar(int id)
        {
            return bd.eliminar("creditos", " id=" + id);
        }

        public bool editar(List<DatosParaActualizar> datos, int id)
        {
            //crear la lista de datos
            string camposValores = "";
            for (int i = 0; i < datos.Count; i++)
            {
                camposValores += " " + datos[i].Campo + " = " + datos[i].Valor;
                if (i < datos.Count - 1) camposValores += ",";
            }
            //ejecuta el actualizar de BD con los datos
            return bd.actualizar("creditos", camposValores, "id=" + id);

        }


    }
}
