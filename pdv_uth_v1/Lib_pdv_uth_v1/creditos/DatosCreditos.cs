﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lib_pdv_uth_v1.clientes;

namespace Lib_pdv_uth_v1.creditos
{
   public  class DatosCreditos
    {
        private int id;
        private double saldo;
        private DateTime fecha_apertura;
        private int cliente_id;
        private EstadoCredito estado;
        private DateTime fecha_saldado;

        public int Id { get => id; set => id = value; }
        public double Saldo { get => saldo; set => saldo = value; }
        public DateTime Fecha_apertura { get => fecha_apertura; set => fecha_apertura = value; }
        public int Cliente_id { get => cliente_id; set => cliente_id = value; }
        public EstadoCredito Estado { get => estado; set => estado = value; }
        public DateTime Fecha_saldado { get => fecha_saldado; set => fecha_saldado = value; }

        public DatosCreditos(int id, double saldo, DateTime fecha_apertura, int cliente_id, EstadoCredito estado, DateTime fecha_saldado)
        {
            this.Id = id;
            this.Saldo = saldo;
            this.Fecha_apertura = fecha_apertura;
            this.Cliente_id = cliente_id;
            this.Estado = estado;
            this.Fecha_saldado = fecha_saldado;
        }
    }
}

